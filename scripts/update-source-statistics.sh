#!/bin/bash
#
# Name              : update-source-statistics.sh
# Short Description : updates all statistics of sources stored in the repository 
# Documentation     : N/A
# Author            : MH, IZBI Leipzig
# Arguments         : (1) gomma configuration file
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 12.03.2009
#-------------------------------------------

LIB_DIR=/u/homes/hartung/gomma/onex/updater/lib

LOG_FILE=/u/homes/hartung/gomma/onex/updater/logs/updateSourceStatistics.log

# include all necessary external libraries
CP=${LIB_DIR}/mysql-connector-java-5.1.7-bin.jar

# include all module and kernel libraries
CP=${CP}:${LIB_DIR}/gomma-kernel.jar
CP=${CP}:${LIB_DIR}/gomma-tools.jar
CP=${CP}:${LIB_DIR}/commons-pool-1.4.jar
CP=${CP}:${LIB_DIR}/commons-dbcp-1.2.2.jar

/usr/local/java/bin/java -Xmx2048m -classpath ${CP} org.gomma.utils.SourceStatisticsUpdater $1 > ${LOG_FILE}
