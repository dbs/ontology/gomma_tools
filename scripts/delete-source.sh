#!/bin/bash
#
# Name              : delete-source.sh
# Short Description : deletes source data into the Gomma repository
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 04.07.2008
#-------------------------------------------

LOG_FILE=/home/tkirsten/Development/java/GOMMA-Kernel/upload.log

USER=tkirsten
PASSWORD=tori@cagiva

mysql -u${USER} -p${PASSWORD} gomma -e'delete from gomma_lds_structure where lds_id_fk=$1' > ${LOG_FILE}
mysql -u${USER} -p${PASSWORD} gomma -e'delete from gomma_obj_atts where obj_id_fk in (select obj_id from gomma_obj_versions where lds_id_fk = $1)' >> ${LOG_FILE}
mysql -u${USER} -p${PASSWORD} gomma -e'delete from gomma_obj_versions where lds_id_fk= $1' >> ${LOG_FILE}
mysql -u${USER} -p${PASSWORD} gomma -e'delete from gomma_lds_versions where lds_id_fk=$1' >> ${LOG_FILE}
