#!/bin/bash
#
# Name              : import-source.sh
# Short Description : imports source data into the Gomma repository
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 09.06.2008
#-------------------------------------------

# set the environment
source ./load-config.sh

#set the log file name
LOG_FILE=${LOG}/upload.log

# include tool libraries
CP=${CP}:${JAR}/gomma-tools.jar

CP=${CP}:${JAR}/app-patient-cleaning.jar

${JAVA_HOME}/java -Xmx512m -classpath ${CP} org.gomma.io.importer.ExternalSourceImporter $1 $2 1> ${LOG_FILE} 2>> ${LOG_FILE}

