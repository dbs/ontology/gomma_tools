#!/bin/bash
#
# Name              : update-source-statistics.sh
# Short Description : updates all statistics of sources stored in the repository 
# Documentation     : N/A
# Author            : MH, IZBI Leipzig
# Arguments         : (1) gomma configuration file
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 12.03.2009
#-------------------------------------------

# set the environment
source ./load-config.sh

#set the log file name
LOG_FILE=${LOG}/update-source-stats.log

# include all necessary external libraries

# include all module and kernel libraries
CP=${CP}:${JAR}/gomma-tools.jar

$JAVA_HOME/java -Xmx2048m -classpath ${CP} org.gomma.utils.StatisticsGenerator $1 $2 > ${LOG_FILE}
