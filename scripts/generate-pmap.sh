#!/bin/bash
#
# Name              : generate-pmap.sh
# Short Description : genrated a perfect mapping in XML format
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 09.06.2008
#-------------------------------------------

LIB_DIR=/home/tkirsten/Development/java/GOMMA-Kernel

LOG_FILE=/home/tkirsten/Development/java/GOMMA-Kernel/gomma.log

# include all necessary external libraries
CP=${LIB_DIR}/lib/mysql-connector-java-5.1.7-bin.jar

# include all module and kernel libraries
CP=${CP}:${LIB_DIR}/jar/gomma-kernel.jar

java -Xmx512 -classpath ${CP} org.gomma.tools.PerfectMappingTransformerXML -i=$1 -o=$2 -dCol=$3 -dType=$4 -dSource=$5 -rCol=$6 -rType=$7 -rSource=$8 > ${LOG_FILE}
