#!/bin/bash
#
# Name              : update-import-scripts.sh
# Short Description : updates all import scripts referenced in the master file
# Documentation     : N/A
# Author            : MH, IZBI Leipzig
# Arguments         : (1) location of master file, (2) folder where the import scripts are located
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 12.03.2009
#-------------------------------------------

LIB_DIR=/u/homes/gomma/onex/updater/lib

LOG_FILE=/u/homes/gomma/onex/updater/logs/updateImportScripts.log

# include all necessary external libraries
CP=${LIB_DIR}/mysql-connector-java-5.1.7-bin.jar

# include all module and kernel libraries
CP=${CP}:${LIB_DIR}/gomma-kernel.jar
CP=${CP}:${LIB_DIR}/gomma-tools.jar
CP=${CP}:${LIB_DIR}/commons-pool-1.4.jar
CP=${CP}:${LIB_DIR}/commons-dbcp-1.2.2.jar

/usr/local/bin/java -classpath ${CP} org.gomma.io.importer.UpdateImportFiles $1 $2 > ${LOG_FILE}
