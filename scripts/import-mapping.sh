#!/bin/bash
#
# Name              : import-source.sh
# Short Description : imports source data into the Gomma repository
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 09.06.2008
#-------------------------------------------

LIB_DIR=/home/tkirsten/Development/workspace/GOMMA

LOG_FILE=/home/tkirsten/Development/workspace/GOMMA/upload.log

# include all necessary external libraries
CP=${LIB_DIR}/kernel/trunk/lib/mysql-connector-java-5.1.7-bin.jar:
CP=${CP}:${LIB_DIR}/kernel/trunk/lib/simmetrics_jar_v1_6_2_d07_02_07.jar

# include all module and kernel libraries
CP=${CP}:${LIB_DIR}/bin

java -Xmx512 -classpath ${CP} org.gomma.io.importer.MappingImporter $1 -i=$2 > ${LOG_FILE}
