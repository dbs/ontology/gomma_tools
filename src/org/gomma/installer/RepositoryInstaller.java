/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the OMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/

package org.gomma.installer;

import java.rmi.RemoteException;

import org.gomma.GommaImpl;
import org.gomma.exceptions.GommaException;
import org.gomma.rmi.Server;

/**
 * The class RepositoryInstaller can be used to create and drop the repository schema.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   1.4
 */
public class RepositoryInstaller {

	private Server gomma;
	
	public RepositoryInstaller(String confFileNameLoc) throws GommaException, RemoteException {
		this.gomma = new GommaImpl();
		this.gomma.initialize(confFileNameLoc);
	}
	
	public void installRepository() throws GommaException, RemoteException {
		((GommaImpl)this.gomma).installRepository();
	}
	
	public void deinstallRepository() throws GommaException, RemoteException {
		((GommaImpl)this.gomma).deinstallRepository();
	}
	
	public void finish() throws GommaException, RemoteException {
		this.gomma.shutdown();
	}
	
	public static String usage() {
		return "\nusage: RepositoryInstaller <configFileNameLocation> [-install|-deinstall]\n";
	}
	
	public static void main(String[] args) {
		RepositoryInstaller installer;
		if (args != null && args.length==1) {
			try {
				installer = new RepositoryInstaller(args[0]);
				installer.installRepository();
				installer.finish();
				return;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else
		if (args != null && args.length==2) {
			try {
				String cfg = args[0].startsWith("-")?args[1]:args[0];
				String opt = args[1].startsWith("-")?args[1]:args[0]; 
				installer = new RepositoryInstaller(cfg);
				if (opt.equalsIgnoreCase("-install")) {
					installer.installRepository();
					installer.finish();
					return;
				} else 
				if (opt.equalsIgnoreCase("-deinstall")) {
					installer.deinstallRepository();
					installer.finish();
					return;
				} else throw new GommaException("Could not resolve one of the options -install|-deinstall");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else System.out.println(RepositoryInstaller.usage());
	}
}
