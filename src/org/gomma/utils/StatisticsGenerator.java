package org.gomma.utils;

import org.gomma.GommaImpl;

public class StatisticsGenerator {
	
	public static String usage() {
		return "usage: StatisticsGenerator -s|-m <gomma.ini>";
	}
	
	public static void main(String[] args) throws Exception {
		if (args==null||!(args.length==2)) {
			System.out.println("Error: Wrong number of arguments.\n"+StatisticsGenerator.usage());
		} else {
			
			GommaImpl server = new GommaImpl();
			server.initialize(args[1]);
			
			System.out.print("Calculating and storing statistics ... ");
			
			
			if (args[0].equalsIgnoreCase("-s")) {
				server.getStatisticsManager().storeSourceVersionStatistics();
				server.getStatisticsManager().storeSourceStatistics();
			} else if (args[0].equalsIgnoreCase("-m")) {
				server.getStatisticsManager().storeMappingStatistics();
			} else {
				System.out.println("Could not resolve the option '"+args[0]+"'.");
				StatisticsGenerator.usage();
			}
			System.out.println("done");
			
			server.shutdown();
		}
	}
}
