package org.gomma.utils;

import org.gomma.GommaImpl;

public class SourceStatisticsUpdaterStatic {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		GommaImpl rep = new GommaImpl();
		rep.initialize("./gomma-conf.ini");
		rep.getEvolutionStatisticsManager().storeSourceVersionEvolution();
		//rep.getEvolutionStatisticsManager().storeLDSVersionStats("FlyAnatomyOntology","AnatomicalEntity");
		//rep.getEvolutionStatisticsManager().storeEvolutionStats("FlyAnatomyOntology","AnatomicalEntity");
		//rep.getEvolutionStatisticsManager().storeLDSVersionStats("ProteinProteinInteractionOntology","Experiment");
		//rep.getEvolutionStatisticsManager().storeEvolutionStats("ProteinProteinInteractionOntology","Experiment");
		//rep.getEvolutionStatisticsManager().storeLDSVersionStats("ProteinModificationOntology","Protein");
		//rep.getEvolutionStatisticsManager().storeEvolutionStats("ProteinModificationOntology","Protein");
		//rep.getEvolutionStatisticsManager().storeLDSVersionStats("PathwayOntology","Process");
		//rep.getEvolutionStatisticsManager().storeEvolutionStats("PathwayOntology","Process");
		//rep.getEvolutionStatisticsManager().storeLDSVersionStats("NCIThesaurus","HealthEntity");
//		rep.getEvolutionStatisticsManager().storeEvolutionStats("NCIThesaurus","HealthEntity");
		rep.shutdown();
	}

}
