package org.gomma.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class PerfectMappingTransformerXML {

	private static final String NL = "\n";
	
	public PerfectMappingTransformerXML(String inFileName, String outFileName, 
			int domainCol, String domainType, String domainSource, 
			int rangeCol, String rangeType, String rangeSource) throws Exception {
		writeFile(outFileName,readInFile(inFileName,domainCol,rangeCol),domainType,domainSource,rangeType,rangeSource);
	}
	
	private ArrayList<Correspondence> readInFile(String fileName, int dCol, int rCol) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String line;
		String[] columns;
		ArrayList<Correspondence> list = new ArrayList<Correspondence>();
		
		while ((line=reader.readLine())!=null) {
			columns = line.split("\t");
			
			if (columns.length<=dCol) throw new Exception("The number of columns is lower than the specified domain column.");
			if (columns.length<=rCol) throw new Exception("The number of columns is lower than the specified range column.");
			
			list.add(new Correspondence(columns[dCol],columns[rCol]));
		}
		reader.close();
		return list;
	}
	
	private void writeFile(String fileName, ArrayList<Correspondence> list, String domainType, String domainSource, String rangeType, String rangeSource) throws Exception {
		FileWriter writer = new FileWriter(fileName);
		StringBuffer buf = new StringBuffer();
		
		buf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(NL)
		   .append("<mapping name=\"???\" is_instance_map=\"???\" mapping_class=\"???\" mapping_type=\"???\">").append(NL)
		   .append("  <metadata mapping_method=\"???\" mapping_tool=\"???\" minConfidence=\"???\" minSupport=\"???\">").append(NL)
		   .append("    <domain_sources>").append(NL)
		   .append("      <source objecttype=\"").append(domainType).append("\" name=\"").append(domainSource).append("\" timestamp=\"???\" version=\"???\" is_ontology=\"???\" url=\"???\"/>").append(NL)
		   .append("    </domain_sources>").append(NL)
		   .append("    <range_sources>").append(NL)
		   .append("      <source objecttype=\"").append(rangeType).append("\" name=\"").append(rangeSource).append("\" timestamp=\"???\" version=\"???\" is_ontology=\"???\" url=\"???\"/>").append(NL)
		   .append("    </range_sources>").append(NL)
		   .append("  </metadata>").append(NL)
		   .append("  <correspondences>").append(NL);
		
		writer.write(buf.toString());
		buf = new StringBuffer();
		
		for (Correspondence c : list) {
			buf.append("    <correspondence support=\"1\" confidence=\"1.0\" user_checked=\"1\" corr_type=\"N/A\">").append(NL)
			   .append("      <domain_objects><object accession=\"").append(c.getDomainElement())
			   		.append("\" objecttype=\"").append(domainType).append("\" source_name=\"").append(domainSource).append("\" /></domain_objects>").append(NL)
			   .append("      <range_objects><object accession=\"").append(c.getRangeElement())
			   		.append("\" objecttype=\"").append(rangeType).append("\" source_name=\"").append(rangeSource).append("\" /></range_objects>").append(NL)
			   .append("    </correspondence>").append(NL);

		}
		buf.append("  </correspondences>").append(NL)
		   .append("</mapping>");

		writer.write(buf.toString());
		writer.flush();
		writer.close();
	}
	
	public static String usage() {
		return PerfectMappingTransformerXML.class.getName()+" -i=<inFile> -o=<outFile> "+
					"-dCol=<domainIDColumn> -dType=<domainObjectType> -dSource=<domainPDS> "+
					"-rCol=<rangeIDColumn> -rType=<rangeObjectType> -rSource=<rangePDS>";
	}
	
	public static void main(String[] args) {
		String inFileName=null, outFileName=null, dType=null, dSource=null, rType=null, rSource=null;
		int dCol=0, rCol=1;
		
		if (args!=null && (args.length==6 || args.length==8)) {
			for (String arg :args) {
				if (arg.startsWith("-i=")) inFileName=arg.substring(3).trim();
				if (arg.startsWith("-o=")) outFileName=arg.substring(3).trim();
				if (arg.startsWith("-dCol=")) dCol = Integer.parseInt(arg.substring(6).trim());
				if (arg.startsWith("-dType=")) dType = arg.substring(7).trim();
				if (arg.startsWith("-dSource=")) dSource = arg.substring(9).trim();
				if (arg.startsWith("-rCol=")) rCol = Integer.parseInt(arg.substring(6).trim());
				if (arg.startsWith("-rType=")) rType = arg.substring(7).trim();
				if (arg.startsWith("-rSource=")) rSource = arg.substring(9).trim();
			}
			if (inFileName!=null && outFileName!=null) {
				try {
					new PerfectMappingTransformerXML(inFileName,outFileName,dCol,dType,dSource,rCol,rType,rSource);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(usage());
	}
	
	private class Correspondence {
		private String domain;
		private String range;
		
		public Correspondence(String domainElement, String rangeElement) {
			this.domain=domainElement;
			this.range =rangeElement;
		}
		
		public String getDomainElement() {
			return this.domain;
		}
		
		public String getRangeElement() {
			return this.range;
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof Correspondence)) return false;
			Correspondence o1 = (Correspondence)o;
			return this.getDomainElement().equals(o1.getDomainElement()) &&
					this.getRangeElement().equals(o1.getRangeElement());
		}
	}
}
