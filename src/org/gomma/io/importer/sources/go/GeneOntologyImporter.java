package org.gomma.io.importer.sources.go;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.gomma.api.util.DatabaseConnectionData;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;

public class GeneOntologyImporter extends PreSourceImporter {
	public void loadSourceData() throws ImportException {
		String[] repParameters = this.getRepositoryParameters();
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from web ...");
		String ls = System.getProperty("line.separator");
		try {
			RandomAccessFile f = new RandomAccessFile("importScript","rw");
			f.writeBytes("#!/bin/bash"+ls);
			this.getMainImporter().getLocation();
			f.writeBytes("wget "+this.getMainImporter().getLocation()+ls);
			f.writeBytes("tar xvzf go_*.tar.gz 1> /dev/null"+ls);
			f.writeBytes("cd go_*"+ls);
			f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term.sql"+ls);
			f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term2term.sql"+ls);
			f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term_definition.sql"+ls);
			f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term_synonym.sql"+ls);
			f.writeBytes("mysqlimport -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" -d -L "+repParameters[2]+" term.txt term2term.txt term_definition.txt term_synonym.txt"+ls);
			f.writeBytes("cd .."+ls);
			f.writeBytes("rm -rf go_*");
			f.close();
			Runtime r = Runtime.getRuntime();
			Process p = r.exec(new String[]{"chmod","755","importScript"});
			p.waitFor();
			p = r.exec(new String[]{"./importScript"});
			p.waitFor();
			p = r.exec(new String[]{"rm","-f","importScript"});
			p.waitFor();
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		} catch (InterruptedException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		String objectType = this.getMainImporter().getObjectType();
		String objectTypeForQuery = "";
		if (objectType.equals("Function")) {
			objectTypeForQuery = "function";
		} else if (objectType.equals("Process")) {
			objectTypeForQuery = "process";
		} else if (objectType.equals("Component")) {
			objectTypeForQuery = "component";
		}
		String[] statements = new String[9]; 
		try {
			//Objects
			statements[0] = "insert into tmp_objects (accession) select acc from term where term_type like '%"+objectTypeForQuery+"%';";
			//Relationships
			statements[1] = "insert into tmp_structure (parent_acc,child_acc,rel_type) "+
									"select parent.acc, child.acc, type.name "+
									"from term parent, term child, term type, term2term "+
									"where child.id = term2_id and parent.id = term1_id and type.id = relationship_type_id "+
									"and child.term_type like '%"+objectTypeForQuery+"%' and parent.term_type like '%"+objectTypeForQuery+"%';";
			//Names
			statements[2] = "insert into tmp_obj_attribute (accession,attribute,value) "+
									"select acc,'name',name from term where term_type like '%"+objectTypeForQuery+"%';";
			//Obsolete Status
			statements[3] = "insert into tmp_obj_attribute (accession,attribute, value) "+
									"select acc,'isObsolete',CASE WHEN is_obsolete = '1' THEN 'true' ELSE 'false' END "+
									"from term where term_type like '%"+objectTypeForQuery+"%';";
			//Definition
			statements[4] = "insert into tmp_obj_attribute (accession,attribute,value) "+
									"select acc,'definition',term_definition "+
									"from term t, term_definition def "+
									"where t.term_type like '%"+objectTypeForQuery+"%' and t.id = def.term_id;";
			//Synonyms
			try {
				String synonymsWithScope = "insert into tmp_obj_attribute (accession,attribute,scope,value) "+
											"select t.acc,'synonym', synScope.name,syn.term_synonym from term t, term_synonym syn, term synScope "+
											"where t.term_type like '%"+objectTypeForQuery+"%' and t.id = syn.term_id and syn.synonym_type_id = synScope.id;";
				super.getImportAPI().insertIntoTmpTables(new String[]{synonymsWithScope});
			} catch (RepositoryException e) {
				String synonymsWithoutScope = "insert into tmp_obj_attribute (accession,attribute,scope,value) "+
												"select acc,'synonym','N/A',term_synonym from term t, term_synonym syn "+
												"where t.term_type like '%"+objectTypeForQuery+"%' and t.id = syn.term_id;";
				super.getImportAPI().insertIntoTmpTables(new String[]{synonymsWithoutScope});
			}
		
			statements[5] = "drop table if exists term";
			statements[6] = "drop table if exists term2term";
			statements[7] = "drop table if exists term_synonym";
			statements[8] = "drop table if exists term_definition";
			super.getImportAPI().insertIntoTmpTables(statements);
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void removeDownloadedSourceData() throws ImportException {
		// Nothing to do here, since GO data is removed directly after insert into repository	
	}
	private String[] getRepositoryParameters() {
		DatabaseConnectionData dbc = DatabaseHandler.getInstance().getDatabaseConnectionData();
		String jdbcUrl = dbc.getDatabaseUrl();
		int index1 = jdbcUrl.indexOf("//");
		int index2 = jdbcUrl.indexOf("?");
		String temp = jdbcUrl.substring(index1+2,index2);
		int index3 = temp.indexOf(":");
		int index4 = temp.indexOf("/");
		String host = temp.substring(0,index3);
		String port = temp.substring(index3+1,index4);
		String dbName = temp.substring(index4+1);
		return new String[]{host,port,dbName,dbc.getDatabaseUser(),dbc.getDatabasePw()};
	}
}
