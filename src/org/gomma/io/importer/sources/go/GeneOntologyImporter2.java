package org.gomma.io.importer.sources.go;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.gomma.api.util.DatabaseConnectionData;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;

public class GeneOntologyImporter2 extends PreSourceImporter {
	public static final String CREATE_TERM_TABLE = "DROP TABLE IF EXISTS 'term';"+
													"CREATE TABLE 'term' ("+
													"'id' int(11) NOT NULL AUTO_INCREMENT,"+
													"'name' varchar(255) NOT NULL DEFAULT '',"+
													"'term_type' varchar(55) NOT NULL,"+
													"'acc' varchar(255) NOT NULL,"+
													"'is_obsolete' int(11) NOT NULL DEFAULT '0',"+
													"'is_root' int(11) NOT NULL DEFAULT '0',"+
													"'is_relation' int(11) NOT NULL DEFAULT '0',"+
													"PRIMARY KEY ('id'),"+
													"UNIQUE KEY 'acc' ('acc'),"+
													"UNIQUE KEY 't0' ('id'),"+
													"KEY 't1' ('name'),"+
													"KEY 't2' ('term_type'),"+
													"KEY 't3' ('acc'),"+
													"KEY 't4' ('id','acc'),"+
													"KEY 't5' ('id','name'),"+
													"KEY 't6' ('id','term_type'),"+
													"KEY 't7' ('id','acc','name','term_type')"+
													") TYPE=MyISAM AUTO_INCREMENT=35250 DEFAULT CHARSET=latin1;";
	public static final String CREATE_TERM2TERM_TABLE = "DROP TABLE IF EXISTS 'term2term';"+
														"CREATE TABLE 'term2term' ("+
														  "'id' int(11) NOT NULL AUTO_INCREMENT,"+
														  "'relationship_type_id' int(11) NOT NULL,"+
														  "'term1_id' int(11) NOT NULL,"+
														  "'term2_id' int(11) NOT NULL,"+
														  "'complete' int(11) NOT NULL DEFAULT '0',"+
														  "PRIMARY KEY ('id'),"+
														  "UNIQUE KEY 'term1_id' ('term1_id','term2_id','relationship_type_id'),"+
														  "KEY 'tt1' ('term1_id'),"+
														  "KEY 'tt2' ('term2_id'),"+
														  "KEY 'tt3' ('term1_id','term2_id'),"+
														  "KEY 'tt4' ('relationship_type_id')"+
														") TYPE=MyISAM AUTO_INCREMENT=64100 DEFAULT CHARSET=latin1;";
	
	public static final String CREATE_TERM_DEFINITION_TABLE = "DROP TABLE IF EXISTS 'term_definition';"+
																"CREATE TABLE 'term_definition' ("+
																  "'term_id' int(11) NOT NULL,"+
																  "'term_definition' text NOT NULL,"+
																  "'dbxref_id' int(11) DEFAULT NULL,"+
																  "'term_comment' mediumtext,"+
																  "'reference' varchar(255) DEFAULT NULL,"+
																  "UNIQUE KEY 'term_id' ('term_id'),"+
																  "KEY 'dbxref_id' ('dbxref_id'),"+
																  "KEY 'td1' ('term_id')"+
																") TYPE=MyISAM DEFAULT CHARSET=latin1;";
	
	public static final String CREATE_TERM_SYNONYMS_TABLE = "DROP TABLE IF EXISTS 'term_synonym';"+
															"CREATE TABLE 'term_synonym' ("+
															  "'term_id' int(11) NOT NULL,"+
															  "'term_synonym' varchar(996) DEFAULT NULL,"+
															  "'acc_synonym' varchar(255) DEFAULT NULL,"+
															  "'synonym_type_id' int(11) NOT NULL,"+
															  "'synonym_category_id' int(11) DEFAULT NULL,"+
															  "UNIQUE KEY 'term_id' ('term_id','term_synonym'),"+
															  "KEY 'synonym_type_id' ('synonym_type_id'),"+
															  "KEY 'synonym_category_id' ('synonym_category_id'),"+
															  "KEY 'ts1' ('term_id'),"+
															  "KEY 'ts2' ('term_synonym'),"+
															  "KEY 'ts3' ('term_id','term_synonym')"+
															") TYPE=MyISAM DEFAULT CHARSET=latin1;";
	
	
	public void loadSourceData() throws ImportException {
		String[] repParameters = this.getRepositoryParameters();
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from web ...");
		String ls = System.getProperty("line.separator");
		try {
			String[] statements = new String[4];
			statements[0] = CREATE_TERM_TABLE;
			statements[1] = CREATE_TERM2TERM_TABLE;
			statements[2] = CREATE_TERM_DEFINITION_TABLE;
			statements[3] = CREATE_TERM_SYNONYMS_TABLE;
			super.getImportAPI().insertIntoTmpTables(statements);
			
			RandomAccessFile f = new RandomAccessFile("importScript","rw");
			f.writeBytes("#!/bin/bash"+ls);
			this.getMainImporter().getLocation();
			f.writeBytes("wget "+this.getMainImporter().getLocation()+ls);
			f.writeBytes("tar xvzf go_*.tar.gz 1> /dev/null"+ls);
			f.writeBytes("cd go_*"+ls);
			//f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term.sql"+ls);
			//f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term2term.sql"+ls);
			//f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term_definition.sql"+ls);
			//f.writeBytes("mysql -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" "+repParameters[2]+" < term_synonym.sql"+ls);
			f.writeBytes("mysqlimport -u "+repParameters[3]+" -h "+repParameters[0]+" -P "+repParameters[1]+" --password="+repParameters[4]+" -d -L "+repParameters[2]+" term.txt term2term.txt term_definition.txt term_synonym.txt"+ls);
			f.writeBytes("cd .."+ls);
			f.writeBytes("rm -rf go_*");
			f.close();
			Runtime r = Runtime.getRuntime();
			Process p = r.exec(new String[]{"chmod","755","importScript"});
			p.waitFor();
			p = r.exec(new String[]{"./importScript"});
			p.waitFor();
			p = r.exec(new String[]{"rm","-f","importScript"});
			p.waitFor();
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		} catch (InterruptedException e) {
			throw new ImportException(e.getMessage());
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		String objectType = this.getMainImporter().getObjectType();
		String objectTypeForQuery = "";
		if (objectType.equals("Function")) {
			objectTypeForQuery = "function";
		} else if (objectType.equals("Process")) {
			objectTypeForQuery = "process";
		} else if (objectType.equals("Component")) {
			objectTypeForQuery = "component";
		}
		String[] statements = new String[9]; 
		try {
			//Objects
			statements[0] = "insert into tmp_objects (accession) select acc from term where term_type like '%"+objectTypeForQuery+"%';";
			//Relationships
			statements[1] = "insert into tmp_structure (parent_acc,child_acc,rel_type) "+
									"select parent.acc, child.acc, type.name "+
									"from term parent, term child, term type, term2term "+
									"where child.id = term2_id and parent.id = term1_id and type.id = relationship_type_id "+
									"and child.term_type like '%"+objectTypeForQuery+"%' and parent.term_type like '%"+objectTypeForQuery+"%';";
			//Names
			statements[2] = "insert into tmp_obj_attribute (accession,attribute,value) "+
									"select acc,'name',name from term where term_type like '%"+objectTypeForQuery+"%';";
			//Obsolete Status
			statements[3] = "insert into tmp_obj_attribute (accession,attribute, value) "+
									"select acc,'isObsolete',CASE WHEN is_obsolete = '1' THEN 'true' ELSE 'false' END "+
									"from term where term_type like '%"+objectTypeForQuery+"%';";
			//Definition
			statements[4] = "insert into tmp_obj_attribute (accession,attribute,value) "+
									"select acc,'definition',term_definition "+
									"from term t, term_definition def "+
									"where t.term_type like '%"+objectTypeForQuery+"%' and t.id = def.term_id;";
			//Synonyms
			try {
				String synonymsWithScope = "insert into tmp_obj_attribute (accession,attribute,scope,value) "+
											"select t.acc,'synonym', synScope.name,syn.term_synonym from term t, term_synonym syn, term synScope "+
											"where t.term_type like '%"+objectTypeForQuery+"%' and t.id = syn.term_id and syn.synonym_type_id = synScope.id;";
				super.getImportAPI().insertIntoTmpTables(new String[]{synonymsWithScope});
			} catch (RepositoryException e) {
				String synonymsWithoutScope = "insert into tmp_obj_attribute (accession,attribute,scope,value) "+
												"select acc,'synonym','N/A',term_synonym from term t, term_synonym syn "+
												"where t.term_type like '%"+objectTypeForQuery+"%' and t.id = syn.term_id;";
				super.getImportAPI().insertIntoTmpTables(new String[]{synonymsWithoutScope});
			}
		
			statements[5] = "drop table if exists term";
			statements[6] = "drop table if exists term2term";
			statements[7] = "drop table if exists term_synonym";
			statements[8] = "drop table if exists term_definition";
			super.getImportAPI().insertIntoTmpTables(statements);
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void removeDownloadedSourceData() throws ImportException {
		// Nothing to do here, since GO data is removed directly after insert into repository	
	}
	private String[] getRepositoryParameters() {
		DatabaseConnectionData dbc = DatabaseHandler.getInstance().getDatabaseConnectionData();
		String jdbcUrl = dbc.getDatabaseUrl();
		int index1 = jdbcUrl.indexOf("//");
		int index2 = jdbcUrl.indexOf("?");
		String temp = jdbcUrl.substring(index1+2,index2);
		int index3 = temp.indexOf(":");
		int index4 = temp.indexOf("/");
		String host = temp.substring(0,index3);
		String port = temp.substring(index3+1,index4);
		String dbName = temp.substring(index4+1);
		return new String[]{host,port,dbName,dbc.getDatabaseUser(),dbc.getDatabasePw()};
	}
}
