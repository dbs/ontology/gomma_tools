package org.gomma.io.importer.sources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;

public class SimpleRDFImporter extends PreSourceImporter {

	private List<String[]> accessions;
	private List<String[]> attributesImport;
	
	public void loadSourceData() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from file system ...");
		if (accessions==null) {
			accessions = new Vector<String[]>();
		}
		if (attributesImport==null) {
			attributesImport = new Vector<String[]>();
		}
		try {
			String location = this.getMainImporter().getLocation();
			RandomAccessFile csvFile = new RandomAccessFile(location,"r");
			String currentLine;
			HashSet<String> allSubjects = new HashSet<String>();
			while ((currentLine=csvFile.readLine())!=null) {
				String[] values = currentLine.split("\t");
				attributesImport.add(values);
				allSubjects.add(values[0]);
			}
			for (String subject : allSubjects) {
				accessions.add(new String[]{subject});
			}
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		
		try {
			SourceVersionImportAPI api = super.getImportAPI();
			api.insertIntoTmpTables(accessions,"insert into tmp_objects (accession) values (?)");
			api.insertIntoTmpTables(attributesImport,"insert into tmp_obj_attribute (accession,attribute,scope,value) values (?,?,'N/A',?)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
			
		duration = (System.currentTimeMillis()-start);
		System.out.println("  Done ! ("+duration+" ms)");
	}
	
	public void removeDownloadedSourceData() throws ImportException {
	}

}
