package org.gomma.io.importer.sources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Vector;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.sources.OLD.OBOImporter;

public class OBOImporterLocal_Lee extends OBOImporter {
	public void loadSourceData() throws ImportException {
		
	}
	
	public void importIntoTmpTables() throws ImportException {
		List<String> relationshipTypeArray = new Vector<String>();
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		try {
			SourceVersionImportAPI api = super.getImportAPI();
			String location = this.getMainImporter().getLocation();
			
			String currentDir = new File(".").getAbsolutePath().replace("\\", "/");
			//Remove "." at last position in current path
			currentDir = currentDir.substring(0,currentDir.length()-1);	
			
			String fileLocation = (currentDir+location).replace("//", "/").replace(" ", "%20");
			
			BufferedReader oboFile= new BufferedReader(new InputStreamReader(new FileInputStream(fileLocation),"UTF8"));
			//RandomAccessFile oboFile = new RandomAccessFile((currentDir+location).replace("//", "/").replace(" ", "%20"),"r");
			
			String line;
			List<String[]> objectsImport = new Vector<String[]>();
			List<String[]> attributesImport = new Vector<String[]>();
			List<String[]> relationshipsImport = new Vector<String[]>();
			List<String[]> tmpRelationshipsImport = new Vector<String[]>();
			List<String[]> tmpAttributesImport = new Vector<String[]>();
			
			int parsed = 0;
			boolean inTerm = false;
			String id = null;
			String is_Obsolete = "false";
			
			while ((line=oboFile.readLine())!=null) {
				if (line.startsWith("[Term]")) {
					//Term entdeckt
					inTerm = true;
				}
				if (line.equals("")&&inTerm) {
					//aktueller Term wurde beendet
					inTerm = false;
					attributesImport.add(new String[]{id,"isObsolete","N/A",is_Obsolete});
					objectsImport.add(new String[]{id});
					relationshipsImport.addAll(tmpRelationshipsImport);
					attributesImport.addAll(tmpAttributesImport);
					parsed++;
					if (parsed>2000) {
						api.insertIntoTmpTables(objectsImport,"insert into tmp_objects (accession) values (?)");
						api.insertIntoTmpTables(attributesImport,"insert into tmp_obj_attribute (accession,attribute,scope,value) values (?,?,?,?)");
						api.insertIntoTmpTables(relationshipsImport,"insert into tmp_structure (child_acc,parent_acc,rel_type) values (?,?,?)");
						objectsImport.clear();
						attributesImport.clear();
						relationshipsImport.clear();
						parsed = 0;
					}
					id = null;
					is_Obsolete = "false";
					tmpRelationshipsImport.clear();
					tmpAttributesImport.clear();
				}
				//Basis-Attribute
				if (line.startsWith("id: ")&&inTerm) {
					id = line.split("id: ")[1].trim();
				}
				if (line.startsWith("name: ")&&inTerm) {
					String name = line.split("name: ")[1].trim();
					attributesImport.add(new String[]{id,"name","N/A",name});
				}
				if (line.startsWith("is_obsolete: ")&&inTerm) {
					is_Obsolete = line.split("is_obsolete: ")[1].trim();
				}
				
				//Externe Referenzen
				if (line.startsWith("xref: ")&&inTerm) {					
					String object = line.split("xref: ")[1].trim();
					object = object.split(" ")[0].trim();		
					//int index1 = object.indexOf("\"");
					//int index2 = object.lastIndexOf("\"");
					String xref = object.trim();//object.substring(index1+1,index2).trim();
					xref = xref.replaceAll("\\\\.",".");
					xref = xref.replaceAll("\\\\,",",");
					//System.out.println(xref);
					attributesImport.add(new String[]{id,"xref","N/A",xref});
				}
				//Definitionen
				if (line.startsWith("def: ")&&inTerm) {
					String object = line.split("def: \"")[1].trim();
					object = object.split("\" \\[")[0].trim();
					//int index1 = object.indexOf("\"");
					//int index2 = object.lastIndexOf("\"");
					String definition = object.trim();//object.substring(index1+1,index2).trim();
					definition = definition.replaceAll("\\\\.",".");
					definition = definition.replaceAll("\\\\,",",");
					attributesImport.add(new String[]{id,"definition","N/A",definition});
				}
				
				//Synonyme
				if (line.startsWith("alt_id: ")&&inTerm) {
					String synonymValue = line.split("alt_id: ")[1].trim();
					addAttributeToTmpList(new String[]{id,"synonym","alt_id",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("exact_synonym: ")&&inTerm) {
					String object = line.split("exact_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","exact",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("broad_synonym: ")&&inTerm) {
					String object = line.split("broad_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","broad",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("related_synonym: ")&&inTerm) {
					String object = line.split("related_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","related",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("narrow_synonym: ")&&inTerm) {
					String object = line.split("narrow_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","narrow",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("synonym: ")&&inTerm) {
					String scope = "N/A";
					if (line.indexOf("RELATED")>0) {
						scope = "related";
					}
					if (line.indexOf("BROAD")>0) {
						scope = "broad";
					}
					if (line.indexOf("NARROW")>0) {
						scope = "narrow";
					}
					if (line.indexOf("EXACT")>0) {
						scope = "exact";
					}
					if (line.indexOf("PREFNAME")>0) {
						scope = "prefname";
					}
					if (line.indexOf("NON-ENGL-EQUIV")>0) {
						scope = "non-engl-equiv";
					}
					String object = line.split("synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym",scope,synonymValue},tmpAttributesImport);
				}
				
				//Relationen
				if (line.startsWith("is_a: ")&&inTerm) {
					String[] relationship = new String[3];
					if (id!=null) {
						relationship[0] = id;
						String object = line.split("is_a: ")[1].trim();
						object = object.split(" ")[0].trim();
						relationship[1] = object;
						//System.out.println("is_a - "+ object);
						relationship[2] = "is_a";
						boolean doubleRelationship = false;
						for (int i=0;i<tmpRelationshipsImport.size();i++) {
							String[] tmp = tmpRelationshipsImport.get(i);
							if (tmp[0].equalsIgnoreCase(relationship[0])&&tmp[1].equalsIgnoreCase(relationship[1])&&tmp[2].equalsIgnoreCase(relationship[2])) {
								doubleRelationship = true;
								break;
							}
						}
						if (!doubleRelationship) {
							tmpRelationshipsImport.add(relationship);
						}
					}
				}
				if (line.startsWith("relationship: ")&&inTerm) {
					String[] relationship = new String[3];
					if (id!=null) {
						relationship[0] = id;
						String objectAndRelationship = line.split("relationship: ")[1].trim();
						String relation = objectAndRelationship.split(" ")[0].trim();
						if (relation.equalsIgnoreCase("is_part_of")) {
							relation = "part_of";
						}					
						String object = objectAndRelationship.split(" ")[1].trim();
						//System.out.println(id+ " - "+relation + " - "+ object);
						
						relationship[1] = object;
						relationship[2] = relation;
						
						if(!relationshipTypeArray.contains(relation)){
							relationshipTypeArray.add(relation);
						}
						boolean doubleRelationship = false;
						for (int i=0;i<tmpRelationshipsImport.size();i++) {
							String[] tmp = tmpRelationshipsImport.get(i);
							if (tmp[0].equalsIgnoreCase(relationship[0])&&tmp[1].equalsIgnoreCase(relationship[1])&&tmp[2].equalsIgnoreCase(relationship[2])) {
								doubleRelationship = true;
								break;
							}
						}
						if (!doubleRelationship) {
							tmpRelationshipsImport.add(relationship);
						}
					}
				}
				
			}
			if (parsed>0) {
				api.insertIntoTmpTables(objectsImport,"insert into tmp_objects (accession) values (?)");
				api.insertIntoTmpTables(attributesImport,"insert into tmp_obj_attribute (accession,attribute,scope,value) values (?,?,?,?)");
				api.insertIntoTmpTables(relationshipsImport,"insert into tmp_structure (child_acc,parent_acc,rel_type) values (?,?,?)");
				objectsImport.clear();
				attributesImport.clear();
				relationshipsImport.clear();
			}
			/*System.out.println("\nINSERT INTO gomma_rela_types(name,descr) VALUES ");
			for(String rel:relationshipTypeArray){
				System.out.println("(\""+rel+"\",\"N/A\"),");
			}*/
			
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void removeDownloadedSourceData() throws ImportException {

	}
}
