package org.gomma.io.importer.sources.ensembl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;

public class EnsemblImporter extends PreSourceImporter {
	private List<String[]> accessions;
	
	public void loadSourceData() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from web ...");
		accessions = new Vector<String[]>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			Connection c = DriverManager.getConnection(this.getMainImporter().getLocation());
			Statement s = c.createStatement();
			ResultSet rs = s.executeQuery("select stable_id from translation_stable_id");
			while (rs.next()) {
				accessions.add(new String[]{rs.getString(1)});
			}
			rs.close();
			s.close();
			c.close();
		} catch (InstantiationException e) {
			throw new ImportException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new ImportException(e.getMessage());
		} catch (ClassNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (SQLException e) {
			throw new ImportException(e.getMessage());
		}
		duration = (System.currentTimeMillis()-start);
		System.out.println("  Done ! ("+duration+" ms)");
	}
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		try {
			SourceVersionImportAPI api = super.getImportAPI();
			api.insertIntoTmpTables(accessions,"insert into tmp_objects (accession) values (?)");
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void removeDownloadedSourceData() throws ImportException {
	}
}
