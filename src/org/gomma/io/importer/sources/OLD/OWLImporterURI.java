package org.gomma.io.importer.sources.OLD;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.model.DataTypes;
import org.semanticweb.owl.apibinding.OWLManager;
import org.semanticweb.owl.model.OWLAnnotationAxiom;
import org.semanticweb.owl.model.OWLAxiom;
import org.semanticweb.owl.model.OWLClass;
import org.semanticweb.owl.model.OWLDescription;
import org.semanticweb.owl.model.OWLEntityAnnotationAxiom;
import org.semanticweb.owl.model.OWLObjectAllRestriction;
import org.semanticweb.owl.model.OWLObjectSomeRestriction;
import org.semanticweb.owl.model.OWLOntology;
import org.semanticweb.owl.model.OWLOntologyCreationException;
import org.semanticweb.owl.model.OWLOntologyManager;
import org.semanticweb.owl.model.OWLTypedConstant;

import uk.ac.manchester.cs.owl.OWLTypedConstantImpl;

public class OWLImporterURI extends PreSourceImporter {

	public OWLImporterURI() {
		super();
	}	
	
	public void loadSourceData() throws ImportException {}
	
	public void importIntoTmpTables() throws ImportException {
	
		try {				
				OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
				// load an ontology from a physical URI
				String location = super.getMainImporter().getLocation();
				
				URI physicalURI = URI.create(location);
				OWLOntology ontology = manager.loadOntologyFromPhysicalURI(physicalURI);

				HashMap<String, List<String>> genidMap = new HashMap<String, List<String>>();
				HashMap<String, ImportObj>	objHashMap	= new HashMap<String, ImportObj>();				
				ArrayList<ImportObj>		objList		= new ArrayList<ImportObj>();
				ImportSourceStructure		objRelSet	= new ImportSourceStructure();

				for (OWLClass owlClass : ontology.getReferencedClasses()) {	
					//Obj hinzufuegen
					String currentAcc	= owlClass.getURI().toString();

					ImportObj o;
					if(!objHashMap.containsKey(currentAcc)){
						o = new ImportObj(currentAcc);
						objHashMap.put(currentAcc, o);
					}else{
						o = objHashMap.get(currentAcc);
					}
					
					String name = owlClass.toString();
					o.addAttribute("name", "N/A", DataTypes.STRING, name);
					
					for (OWLAnnotationAxiom axiom: owlClass.getAnnotationAxioms(ontology)){
						
						if(axiom.getAnnotation().getAnnotationValue() instanceof OWLTypedConstantImpl){
							
							String synValueString = ((OWLTypedConstantImpl)axiom.getAnnotation().getAnnotationValue()).getLiteral();//
							
							if (synValueString.contains("@")) {	//hinter @ steht languagekuerzel, z.B. en, lat, ...									
								String [] attSplitted = synValueString.split("@");
								synValueString = attSplitted[0];															
							}
							
							if(synValueString.contains("genid")){
								if(genidMap.containsKey(o.getAccessionNumber())){
									List<String> genidList = genidMap.get(o.getAccessionNumber());
									genidList.add(synValueString.split("#")[1]);
								}else{
									List<String> genidList = new Vector<String>();
									genidList.add(synValueString.split("#")[1]);
									genidMap.put(o.getAccessionNumber(), genidList);
								}
							}else{
								o.addAttribute("synonym", "N/A", DataTypes.STRING, synValueString.replace("&#39;", "'"));
							}
						}else{
							String synValueString = axiom.getAnnotation().getAnnotationValue().toString();//.replace("&#39;", "'")
							if (synValueString.contains("@")) {	//hinter @ steht languagekuerzel, z.B. en, lat, ...									
								String [] attSplitted = synValueString.split("@");
								synValueString = attSplitted[0];															
							}//System.out.println(synValueString);
							if(synValueString.contains("genid")){
								if(genidMap.containsKey(o.getAccessionNumber())){
									List<String> genidList = genidMap.get(o.getAccessionNumber());
									genidList.add(synValueString);
								}else{
									List<String> genidList = new Vector<String>();
									genidList.add(synValueString);
									genidMap.put(o.getAccessionNumber(), genidList);
								}
							}else{
								o.addAttribute("synonym", "N/A", DataTypes.STRING, synValueString.replace("&#39;", "'"));
							}
						}
					}
					objList.add(o);
							
					//alle Rel hinzufuegen

					// get relationships
					for(OWLDescription relationship: owlClass.getSuperClasses(ontology)){				
						
						if(relationship instanceof OWLClass){
							OWLClass rel				= (OWLClass) relationship;
							String type					= "is_a";
							String fromAcc				= rel.getURI().toString();					
							objRelSet.addRelationship(fromAcc, currentAcc, type);
						
							
						}else if(relationship instanceof OWLObjectSomeRestriction){
							OWLObjectSomeRestriction rel= (OWLObjectSomeRestriction) relationship;
							String type 				= rel.getProperty().toString();
							//String toAcc				= rel.getFiller().asOWLClass().getURI().toString();
							OWLDescription f = rel.getFiller();					
							if(!f.isAnonymous()){
								String fromAcc=f.asOWLClass().getURI().toString();
								objRelSet.addRelationship(fromAcc, currentAcc, type);
							}else{
							}
							
						}else if(relationship instanceof OWLObjectAllRestriction){
							
							OWLObjectAllRestriction rel = (OWLObjectAllRestriction) relationship;
							String type 				= rel.getProperty().toString();
							//String toAcc				= rel.getFiller().asOWLClass().getURI().toString();
							OWLDescription f = rel.getFiller();					
							if(!f.isAnonymous()){
								String fromAcc=f.asOWLClass().getURI().toString();
								objRelSet.addRelationship(fromAcc, currentAcc, type);
							}else{
								//????
								//als Attribute?
								//System.out.println(rel.toString());
							}
							
						}else{
							// andere Properties z.B. DataValue, DataMinCardinality
							//System.out.println(relationship.toString());
						}							
					}
				}
				
				if (genidMap.size()>0) {
					HashMap<String, String> genid2AttValue	= new HashMap<String, String>();
					for (OWLAxiom axiom : ontology.getAxioms()) {
						if (axiom instanceof OWLEntityAnnotationAxiom) {
							OWLEntityAnnotationAxiom axiomCast = (OWLEntityAnnotationAxiom)axiom;
							if (axiomCast.getSubject().toString().contains("genid")) {
								//System.out.println(axiomCast.getSubject()+" - "+((OWLTypedConstant)axiomCast.getAnnotation().getAnnotationValue()).getLiteral());
								String genid = axiomCast.getSubject().toString();
								String genidValue = ((OWLTypedConstant)axiomCast.getAnnotation().getAnnotationValue()).getLiteral();
								genid2AttValue.put(genid, genidValue);
							}
						}
					}
					for(String objKey : genidMap.keySet()) {
						ImportObj o = objHashMap.get(objKey);
						//System.out.println();
						for(String genidFromMap:genidMap.get(objKey)){
							String synValueString = genid2AttValue.get(genidFromMap);
							o.addAttribute("synonym", "N/A", DataTypes.STRING, synValueString);
						}		
					}
				}
				
				//System.out.println(objHashMap.size());
				//System.out.println(objList.size());
				
				
				super.importIntoTmpTables(objList,objRelSet);
	
		} catch (OWLOntologyCreationException e) {
			System.out.println("The ontology could not be created: " + e.getMessage());
		}
	}

	@Override
	protected void removeDownloadedSourceData() throws ImportException {
	}
}
