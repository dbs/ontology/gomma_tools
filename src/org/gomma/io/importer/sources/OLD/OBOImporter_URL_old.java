package org.gomma.io.importer.sources.OLD;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Vector;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;
@Deprecated
public class OBOImporter_URL_old extends PreSourceImporter {
	
	public void loadSourceData() throws ImportException {}
	
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		try {

			// FUER ONEX IMPORT - ALTES TOOLS PROJEKT MIT ALTEM KERNEL 

			//SourceVersionImportAPI api = this.getMainImporter().getSourceVersionImportAPI();
			SourceVersionImportAPI api = super.getImportAPI();
			
			String location = super.getMainImporter().getLocation();
			URL physicalURL = new URL(location);
	        URLConnection conn = physicalURL.openConnection();
	        BufferedReader oboFile = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			List<String[]> objectsImport = new Vector<String[]>();
			List<String[]> attributesImport = new Vector<String[]>();
			List<String[]> relationshipsImport = new Vector<String[]>();
			List<String[]> tmpRelationshipsImport = new Vector<String[]>();
			List<String[]> tmpAttributesImport = new Vector<String[]>();
			
			int parsed = 0;
			boolean inTerm = false;
			String id = null;
			String is_Obsolete = "false";
			
			while ((line=oboFile.readLine())!=null) {
				if (line.startsWith("[Term]")) {
					//Term entdeckt
					inTerm = true;
				}
				if (line.equals("")&&inTerm) {
					//aktueller Term wurde beendet
					inTerm = false;
					attributesImport.add(new String[]{id,"isObsolete","N/A",is_Obsolete});
					objectsImport.add(new String[]{id});
					relationshipsImport.addAll(tmpRelationshipsImport);
					attributesImport.addAll(tmpAttributesImport);
					parsed++;
					
					if (parsed>2000) {
						api.insertIntoTmpTables(objectsImport,"insert into tmp_objects (accession) values (?)");
						api.insertIntoTmpTables(attributesImport,"insert into tmp_obj_attribute (accession,attribute,scope,value) values (?,?,?,?)");
						api.insertIntoTmpTables(relationshipsImport,"insert into tmp_structure (child_acc,parent_acc,rel_type) values (?,?,?)");
						objectsImport.clear();
						attributesImport.clear();
						relationshipsImport.clear();
						parsed = 0;
					}
					id = null;
					is_Obsolete = "false";
					tmpRelationshipsImport.clear();
					tmpAttributesImport.clear();
				}
				//Basis-Attribute
				if (line.startsWith("id: ")&&inTerm) {
					id = line.split("id: ")[1].trim();
				}
				if (line.startsWith("name: ")&&inTerm) {
					String name = line.split("name: ")[1].trim();
					attributesImport.add(new String[]{id,"name","N/A",name});
				}
				if (line.startsWith("is_obsolete: ")&&inTerm) {
					is_Obsolete = line.split("is_obsolete: ")[1].trim();
				}
				
				//Definitionen
				if (line.startsWith("def: ")&&inTerm) {
					String object = line.split("def: \"")[1].trim();
					object = object.split("\" \\[")[0].trim();
					//int index1 = object.indexOf("\"");
					//int index2 = object.lastIndexOf("\"");
					String definition = object.trim();//object.substring(index1+1,index2).trim();
					definition = definition.replaceAll("\\\\.",".");
					definition = definition.replaceAll("\\\\,",",");
					attributesImport.add(new String[]{id,"definition","N/A",definition});
				}
				
				//Synonyme
				if (line.startsWith("alt_id: ")&&inTerm) {
					String synonymValue = line.split("alt_id: ")[1].trim();
					addAttributeToTmpList(new String[]{id,"synonym","alt_id",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("exact_synonym: ")&&inTerm) {
					String object = line.split("exact_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","exact",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("broad_synonym: ")&&inTerm) {
					String object = line.split("broad_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","broad",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("related_synonym: ")&&inTerm) {
					String object = line.split("related_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","related",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("narrow_synonym: ")&&inTerm) {
					String object = line.split("narrow_synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym","narrow",synonymValue},tmpAttributesImport);
				}
				if (line.startsWith("synonym: ")&&inTerm) {
					String scope = "N/A";
					if (line.indexOf("RELATED")>0) {
						scope = "related";
					}
					if (line.indexOf("BROAD")>0) {
						scope = "broad";
					}
					if (line.indexOf("NARROW")>0) {
						scope = "narrow";
					}
					if (line.indexOf("EXACT")>0) {
						scope = "exact";
					}
					String object = line.split("synonym: ")[1].trim();
					int index1 = object.indexOf("\"");
					int index2 = object.lastIndexOf("\"");
					String synonymValue = object.substring(index1+1,index2).trim();
					addAttributeToTmpList(new String[]{id,"synonym",scope,synonymValue},tmpAttributesImport);
				}
				
				//Relationen
				if (line.startsWith("is_a: ")&&inTerm) {
					String[] relationship = new String[3];
					if (id!=null) {
						relationship[0] = id;
						String object = line.split("is_a: ")[1].trim();
						object = object.split("!")[0].trim();
						relationship[1] = object;
						relationship[2] = "is_a";
						boolean doubleRelationship = false;
						for (int i=0;i<tmpRelationshipsImport.size();i++) {
							String[] tmp = tmpRelationshipsImport.get(i);
							if (tmp[0].equalsIgnoreCase(relationship[0])&&tmp[1].equalsIgnoreCase(relationship[1])&&tmp[2].equalsIgnoreCase(relationship[2])) {
								doubleRelationship = true;
								break;
							}
						}
						if (!doubleRelationship) {
							tmpRelationshipsImport.add(relationship);
						}
					}
				}
				if (line.startsWith("relationship: ")&&inTerm) {
					String[] relationship = new String[3];
					if (id!=null) {
						relationship[0] = id;
						String objectAndRelationship = line.split("relationship: ")[1].trim();
						objectAndRelationship = objectAndRelationship.split("!")[0].trim();
						String relation = objectAndRelationship.split(" ")[0].trim();
						if (relation.equalsIgnoreCase("is_part_of")) {
							relation = "part_of";
						}
						String object = objectAndRelationship.split(" ")[1].trim();
						relationship[1] = object;
						relationship[2] = relation;
						boolean doubleRelationship = false;
						for (int i=0;i<tmpRelationshipsImport.size();i++) {
							String[] tmp = tmpRelationshipsImport.get(i);
							if (tmp[0].equalsIgnoreCase(relationship[0])&&tmp[1].equalsIgnoreCase(relationship[1])&&tmp[2].equalsIgnoreCase(relationship[2])) {
								doubleRelationship = true;
								break;
							}
						}
						if (!doubleRelationship) {
							tmpRelationshipsImport.add(relationship);
						}
					}
				}
				
			}
			if (parsed>0) {
				api.insertIntoTmpTables(objectsImport,"insert into tmp_objects (accession) values (?)");
				api.insertIntoTmpTables(attributesImport,"insert into tmp_obj_attribute (accession,attribute,scope,value) values (?,?,?,?)");
				api.insertIntoTmpTables(relationshipsImport,"insert into tmp_structure (child_acc,parent_acc,rel_type) values (?,?,?)");
				objectsImport.clear();
				attributesImport.clear();
				relationshipsImport.clear();
			}
			
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	private void addAttributeToTmpList(String[] newAttribute, List<String[]> attributesToImport) {
		boolean doubleAttribute = false;
		for (int i=0;i<attributesToImport.size();i++) {
			String[] tmp = attributesToImport.get(i);
			if (tmp[0].equals(newAttribute[0])&&tmp[1].equals(newAttribute[1])&&tmp[2].equals(newAttribute[2])&&tmp[3].equals(newAttribute[3])) {
				doubleAttribute = true;
				break;
			}
		}
		if (!doubleAttribute) {
			attributesToImport.add(newAttribute);
		}
	}
	public void removeDownloadedSourceData() throws ImportException {}
}
