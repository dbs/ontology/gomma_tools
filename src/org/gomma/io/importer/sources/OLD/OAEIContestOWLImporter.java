package org.gomma.io.importer.sources.OLD;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.io.importer.models.ImportObj.ImportObjAttribute;
import org.gomma.model.DataTypes;
import org.semanticweb.owl.apibinding.OWLManager;
import org.semanticweb.owl.model.OWLAnnotationAxiom;
import org.semanticweb.owl.model.OWLClass;
import org.semanticweb.owl.model.OWLDescription;
import org.semanticweb.owl.model.OWLObjectSomeRestriction;
import org.semanticweb.owl.model.OWLOntology;
import org.semanticweb.owl.model.OWLOntologyCreationException;
import org.semanticweb.owl.model.OWLOntologyManager;

import uk.ac.manchester.cs.owl.OWLTypedConstantImpl;
@Deprecated
public class OAEIContestOWLImporter extends PreSourceImporter {

	public OAEIContestOWLImporter() {
		super();
	}	
	/*
	public static void main(String[] args) {
		try {
			OAEIContestOWLImporter i = new OAEIContestOWLImporter();
			i.importIntoTmpTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	public void loadSourceData() throws ImportException {}
	
	public void importIntoTmpTables() throws ImportException {
	
		try {				
				OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
				// load an ontology from a physical URI
				String location = super.getMainImporter().getLocation();
				
				//test parsing with this class...DAZU MAIN METHODE AUSKOMMENTIEREN
				//String location = "http://webrum.uni-mannheim.de/math/lski/anatomy09/mouse_anatomy_2008.owl";
				//FUNKTIONIERT
				//http://webrum.uni-mannheim.de/math/lski/anatomy09/nci_anatomy_2008.owl
				//FUNKTIONIERT NICHT - andere Struktur des OWL files
				//http://purl.org/obo/owl/EHDAA
				
				URI physicalURI = URI.create(location);
				OWLOntology ontology = manager.loadOntologyFromPhysicalURI(physicalURI);

				HashMap<String, ImportObj>	objHashMap	= new HashMap<String, ImportObj>();				
				ArrayList<ImportObj>		objList		= new ArrayList<ImportObj>();
				ImportSourceStructure		objRelSet	= new ImportSourceStructure();
				
				for (OWLClass owlClass : ontology.getReferencedClasses()) {				
					/*if(owlClass.getURI().toString().contains("http://www.geneontology.org")){
							System.out.println(owlClass.toString());
					}*/
					
					if(!owlClass.getURI().toString().contains("http://www.geneontology.org")){

						String acc = owlClass.toString();
						ImportObj importObj = new ImportObj(acc);
						
						Set<OWLDescription> test = owlClass.getDisjointClasses(ontology);
						for(OWLDescription d:test){
							//System.out.println("descripition"+d.toString());
						}
						// get importObj (id & name)
						for (OWLAnnotationAxiom axiom: owlClass.getAnnotationAxioms(ontology)){
							
							String attributeName,attributeValue;								
							
							if(axiom.getAnnotation().getAnnotationValue() instanceof OWLTypedConstantImpl){
								attributeName = axiom.getAnnotation().getAnnotationURI().getRawFragment();
								attributeValue = ((OWLTypedConstantImpl)axiom.getAnnotation().getAnnotationValue()).getLiteral();												
								
								if (attributeValue.contains(".owl#")) {	//NORMALISIERUNG AN # IN STRING
									String [] attSplitted = attributeValue.split("#");
									attributeValue = attSplitted[1];									
								}
								//if (attributeValue.contains("\'")){	System.out.println(attributeValue);	}
								importObj.addAttribute(attributeName, "N/A", DataTypes.STRING, attributeValue);	
								//System.out.println(attributeName +  " - " + attributeValue);							
							}else{
								attributeName = axiom.getAnnotation().getAnnotationURI().getRawFragment().toString();
								attributeValue = axiom.getAnnotation().getAnnotationValue().toString();//.replace("&#39;", "'")
													
								if (attributeValue.contains("#")) {	//NORMALISIERUNG AN # IN STRING									
									String [] attSplitted = attributeValue.split("#");
									attributeValue = attSplitted[1];															
								}
								//if (attributeValue.contains("\'")){	System.out.println(attributeValue);	}
								//System.out.println(attributeName +  " - " + attributeValue);
								importObj.addAttribute(attributeName, "N/A", DataTypes.STRING, attributeValue);			
							}
						}
					
						// add ImportObj to the objHashMap 
						//OLD COMMENT (to get importObj directly by its id, for synonym&definition parsing) 
						objHashMap.put(acc,importObj);
						
						// get relationships
						for(OWLDescription relationship: owlClass.getSuperClasses(ontology)){
							String relName = null, relValue = null;
							
							if(relationship instanceof OWLClass){
								OWLClass rel = (OWLClass) relationship;
								relName = "is_a";
								relValue = rel.toString();
								//System.out.println(relName +  " - " + relValue);
							}else if(relationship instanceof OWLObjectSomeRestriction){
								OWLObjectSomeRestriction rel = (OWLObjectSomeRestriction) relationship;
								relName = rel.getProperty().toString();
								relValue = rel.getFiller().toString();
								//System.out.println(relName +  " - " + relValue);
							}
							if(!relValue.toLowerCase().equalsIgnoreCase("thing")){
								objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
							}
						}
					}					
				}

				// synonyms & definitions are parsed by the generic owl parser but return only a "genid"
				// replace synonyms & definitions by parsing the file
				// method returns the required objList instead of the objHashMap 
				objList = replaceGenidByAttributeValue(objHashMap,location);
				
				//System.out.println(objHashMap.size());
				//System.out.println(objList.size());
				
				
				super.importIntoTmpTables(objList,objRelSet);
	
		} catch (OWLOntologyCreationException e) {
			System.out.println("The ontology could not be created: " + e.getMessage());
		}
	}

	private ArrayList<ImportObj> replaceGenidByAttributeValue(HashMap<String, ImportObj> objHashMap,String location) {
		
		//parsing could be different for other owl-ontologies!!
		
		ArrayList<ImportObj> objList = new ArrayList<ImportObj>();
		boolean openTag = false;
		String genid="";
		String attValue="";	
		
		try {

			URL url = new URL(location);
			URLConnection con=url.openConnection();
			BufferedReader file = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			String line;	
			HashMap<String, String> genid2AttValue	= new HashMap<String, String>();
			
			while ((line=file.readLine())!=null)  {
				if(!openTag && line.startsWith("    <rdf:Description rdf:about=")){
					String[] lineArray = line.split("#");
					genid =lineArray[1].replace("\"", "").replace(">", "");
					openTag = true;						
				}
				if(openTag && line.startsWith("        <rdfs:label rdf:datatype=")){
					String[] lineArray = line.split(">");
					attValue =lineArray[1].replace("</rdfs:label", "");
				}
				if(openTag && line.startsWith("    </rdf:Description>")){
					openTag = false;
					genid2AttValue.put(genid.trim(),attValue.trim());	
				}			
			}
			//System.out.println(genid2AttValue.size());
			for (ImportObj obj : objHashMap.values()){
				for(ImportObjAttribute elem:obj.getAttributeList()){					
					String updateValue = genid2AttValue.get(elem.getValue());
					//System.out.println(elem.getValue()+" - "+updateValue);
					if(updateValue!=null){
						elem.setValue(updateValue);
					}					
				}
				objList.add(obj);
			}		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objList;
	}

	private List<String[]> processClass(String currentClass) {
		
		String attributeName	= null, attributeValue = null;
		List<String[]> results	= new Vector<String[]>();
		String accession= "";
		String pattern	= "<owl:Class rdf:";
		int start		= currentClass.indexOf(pattern)+pattern.length();
		int end			= currentClass.indexOf("\">");
		String sub_str	= currentClass.substring(start,end);
		
		if(sub_str.startsWith("about=")){
			accession = sub_str.substring(sub_str.indexOf("#")+1).replace("\">", "");
			
		}else if(currentClass.substring(start).startsWith("ID=")){
			accession = sub_str.replace("ID=\"", "").replace("\">", "");			
		}
		String[] lines = currentClass.split("\n");
		
		for(int i=0; i<lines.length;i++){
	
			if(lines[i].contains("<oboInOwl:hasRelatedSynonym>")){
				attributeName = "synonym";
				//System.out.println(lines[i]);
				attributeValue = lines[i+3].replace(">", "").replace("</rdfs:label", "").trim();				
				//System.out.println(accession + " - " + attributeName + " - " + attributeValue);				
				String[] triple =  {accession,attributeName,attributeValue};
				results.add(triple);				
			}
			
			if(lines[i].contains("<oboInOwl:hasDefinition>")){
				attributeName = "definition";
				attributeValue = lines[i+3].replace(">", "").replace("</rdfs:label", "").trim();			
				//System.out.println(accession + " - " + attributeName + " - " + attributeValue);			
				String[] triple =  {accession,attributeName,attributeValue};
				results.add(triple);
			}			
		}
		return results;
	}

	@Override
	protected void removeDownloadedSourceData() throws ImportException {
		// TODO Auto-generated method stub

	}
}
