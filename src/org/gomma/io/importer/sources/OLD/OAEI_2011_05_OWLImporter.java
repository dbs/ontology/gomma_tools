package org.gomma.io.importer.sources.OLD;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportObj.ImportObjAttribute;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.model.DataTypes;
import org.semanticweb.owl.apibinding.OWLManager;
import org.semanticweb.owl.model.OWLAnnotationAxiom;
import org.semanticweb.owl.model.OWLClass;
import org.semanticweb.owl.model.OWLDescription;
import org.semanticweb.owl.model.OWLObjectAllRestriction;
import org.semanticweb.owl.model.OWLObjectIntersectionOf;
import org.semanticweb.owl.model.OWLObjectSomeRestriction;
import org.semanticweb.owl.model.OWLObjectUnionOf;
import org.semanticweb.owl.model.OWLOntology;
import org.semanticweb.owl.model.OWLOntologyCreationException;
import org.semanticweb.owl.model.OWLOntologyManager;

import uk.ac.manchester.cs.owl.OWLTypedConstantImpl;

@Deprecated
public class OAEI_2011_05_OWLImporter extends PreSourceImporter {

	public OAEI_2011_05_OWLImporter() {
		super();
	}	
	/*
	public static void main(String[] args) {
		try {
			OAEI_2011_05_OWLImporter i = new OAEI_2011_05_OWLImporter();
			i.importIntoTmpTables();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	public void loadSourceData() throws ImportException {}
	
	public void importIntoTmpTables() throws ImportException {
	
		try {				
				OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
				//load an ontology from a physical URI - main Methode auskommentieren
				String location = super.getMainImporter().getLocation();
				String prefix	= super.getMainImporter().getAccessionPrefix();
				
				//oder
				//test parsing with this class...DAZU MAIN METHODE EINKOMMENTIEREN
				//FMA
					//String prefix	= "FMA:";
					//small
						//String location = "http://seals-test.sti2.at/tdrs-web/testdata/persistent/cf0378d9-da30-4b58-b937-192028ed4961/a9fe4a9b-54c0-4ecf-bdfe-f405cf72193d/suite/original-fma-a-nci-a/component/source";
					//big
						//String location = "http://seals-test.sti2.at/tdrs-web/testdata/persistent/cf0378d9-da30-4b58-b937-192028ed4961/a9fe4a9b-54c0-4ecf-bdfe-f405cf72193d/suite/original-fma-b-nci-b/component/source";
					//whole
						//String location = "http://seals-test.sti2.at/tdrs-web/testdata/persistent/cf0378d9-da30-4b58-b937-192028ed4961/a9fe4a9b-54c0-4ecf-bdfe-f405cf72193d/suite/original-fma-c-nci-c/component/source";					
				
				//NCI
					//String prefix	= "NCI:";
					//small
						//String location = "http://seals-test.sti2.at/tdrs-web/testdata/persistent/cf0378d9-da30-4b58-b937-192028ed4961/a9fe4a9b-54c0-4ecf-bdfe-f405cf72193d/suite/original-fma-a-nci-a/component/target";
					//big
						//String location = "http://seals-test.sti2.at/tdrs-web/testdata/persistent/cf0378d9-da30-4b58-b937-192028ed4961/a9fe4a9b-54c0-4ecf-bdfe-f405cf72193d/suite/original-fma-b-nci-b/component/target";					
					//whole
						//String location = "http://seals-test.sti2.at/tdrs-web/testdata/persistent/cf0378d9-da30-4b58-b937-192028ed4961/a9fe4a9b-54c0-4ecf-bdfe-f405cf72193d/suite/original-fma-c-nci-c/component/target";								
				URI physicalURI			= URI.create(location);
				OWLOntology ontology	= manager.loadOntologyFromPhysicalURI(physicalURI);

				HashMap<String, ImportObj>	objHashMap	= new HashMap<String, ImportObj>();				
				ArrayList<ImportObj>		objList		= new ArrayList<ImportObj>();
				ImportSourceStructure		objRelSet	= new ImportSourceStructure();
				for (OWLClass owlClass : ontology.getReferencedClasses()) {		
						
					String id = prefix+owlClass.toString();
					ImportObj importObj = new ImportObj(id);
					//System.out.println(id);
					
					importObj.addAttribute("name", "N/A", DataTypes.STRING, owlClass.toString());
					
					for (OWLAnnotationAxiom axiom: owlClass.getAnnotationAxioms(ontology)){
						
						String attributeName,attributeValue;								
						
						if(axiom.getAnnotation().getAnnotationValue() instanceof OWLTypedConstantImpl){
							attributeName = axiom.getAnnotation().getAnnotationURI().getRawFragment();
							attributeValue = ((OWLTypedConstantImpl)axiom.getAnnotation().getAnnotationValue()).getLiteral();												
							
							if (attributeValue.contains("@")) {	//hinter @ steht languagekuerzel, z.B. en, lat, ...									
								String [] attSplitted = attributeValue.split("@");
								attributeValue = attSplitted[0];															
							}
							importObj.addAttribute("synonym", "N/A", DataTypes.STRING, attributeValue);
						}else{
							attributeName = axiom.getAnnotation().getAnnotationURI().getRawFragment().toString();
							attributeValue = axiom.getAnnotation().getAnnotationValue().toString();//.replace("&#39;", "'")
							if (attributeValue.contains("@")) {										
								String [] attSplitted = attributeValue.split("@");
								attributeValue = attSplitted[0];															
							}
							importObj.addAttribute("synonym", "N/A", DataTypes.STRING, attributeValue);
						}
					}
					// add ImportObj to the objHashMap 
					// OLD COMMENT (to get importObj directly by its id, for synonym&definition parsing) 
					objHashMap.put(id,importObj);
					
					// get relationships
					for(OWLDescription relationship: owlClass.getSuperClasses(ontology)){
						String relName = null, relValue = null;
						
						if(relationship instanceof OWLClass){
							OWLClass rel = (OWLClass) relationship;
							relName = "is_a";
							relValue = prefix+rel.toString();
							//System.out.println(relName +  " - " + relValue);
							objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
						}else if(relationship instanceof OWLObjectSomeRestriction){
							OWLObjectSomeRestriction rel = (OWLObjectSomeRestriction) relationship;
							relName = rel.getProperty().toString();
							relValue = prefix+rel.getFiller().toString();
							objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
							//System.out.println(relName +  " - " + relValue);
						}else if(relationship instanceof OWLObjectAllRestriction){
							OWLObjectAllRestriction rel = (OWLObjectAllRestriction) relationship;
							relName = rel.getProperty().toString();
							relValue = prefix+rel.getFiller().toString();
							objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
							//System.out.println(relName +  " - " + relValue);
						}else{
							// andere Properties z.B. DataValue, DataMinCardinality
							//System.out.println(relationship.toString());
						}							
					}
//				if(id.equals("NCI:Thyroid_Gland_Spindle_Cell_Tumor_with_Thymus-Like_Differentiation")){
					
				
					for(OWLDescription equivClass:owlClass.getEquivalentClasses(ontology)){
						String relName = null, relValue = null;
						
						if(equivClass instanceof OWLObjectIntersectionOf){
							OWLObjectIntersectionOf intersectOfClass = (OWLObjectIntersectionOf) equivClass;
							
							for(OWLDescription i:intersectOfClass.getOperands()){
								
								if(i instanceof OWLObjectSomeRestriction){
									OWLObjectSomeRestriction p = (OWLObjectSomeRestriction) i;
									relName = "intersectOf"+"_"+p.getProperty().toString();
									relValue = prefix+p.getFiller().toString();
									//System.out.println(id+  " - " + relName +  " - " + relValue);
									objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
									
								}else if(i instanceof OWLObjectAllRestriction){
									OWLObjectAllRestriction p = (OWLObjectAllRestriction) i;
									relName = "intersectOf"+"_"+p.getProperty().toString();
									relValue = prefix+p.getFiller().toString();
									//System.out.println(id+  " - " + relName +  " - " + relValue);
									objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
									
								}else if(i instanceof OWLObjectIntersectionOf){ //verschachtelte Intersection
									
									OWLObjectIntersectionOf intersectOfintersectOfClass = (OWLObjectIntersectionOf) i;
									
									for(OWLDescription ii:intersectOfintersectOfClass.getOperands()){
										
										if(ii instanceof OWLObjectSomeRestriction){
											OWLObjectSomeRestriction p = (OWLObjectSomeRestriction) ii;
											relName = "intersectOfintersectOf"+"_"+p.getProperty().toString();
											relValue = prefix+p.getFiller().toString();
											//System.out.println(id+  " - " + relName +  " - " + relValue);
											objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
											
										}else if(ii instanceof OWLObjectAllRestriction){
											OWLObjectAllRestriction p = (OWLObjectAllRestriction) ii;
											relName = "intersectOfintersectOf"+"_"+p.getProperty().toString();
											relValue = prefix+p.getFiller().toString();
											//System.out.println(id+  " - " + relName +  " - " + relValue);
											objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
											
										}else{										
											relName = "intersectOfintersectOf";
											relValue = prefix+i.toString();
											//System.out.println(id+  " - " + relName +  " - " + relValue);	
											objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);		
										}
									}
								}else if(i instanceof OWLObjectUnionOf){		
									OWLObjectUnionOf unionOfClass = (OWLObjectUnionOf) i;
									
									for(OWLDescription ui:unionOfClass.getOperands()){
										
										if(ui instanceof OWLObjectSomeRestriction){
											OWLObjectSomeRestriction p = (OWLObjectSomeRestriction) ui;
											relName = "unionOfintersectOf"+"_"+p.getProperty().toString();
											relValue = prefix+p.getFiller().toString();
											//System.out.println(id+  " - " + relName +  " - " + relValue);
											objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
											
										}else if(ui instanceof OWLObjectAllRestriction){
											OWLObjectAllRestriction p = (OWLObjectAllRestriction) ui;
											relName = "unionOfintersectOf"+"_"+p.getProperty().toString();
											relValue = prefix+p.getFiller().toString();
											//System.out.println(id+  " - " + relName +  " - " + relValue);
											objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
											
										}else if(ui instanceof OWLObjectIntersectionOf){ //verschachtelte Intersection
											
											OWLObjectIntersectionOf intersectOfunionOfClass = (OWLObjectIntersectionOf) ui;
											
											for(OWLDescription iui:intersectOfunionOfClass.getOperands()){
												
												if(iui instanceof OWLObjectSomeRestriction){
													OWLObjectSomeRestriction p = (OWLObjectSomeRestriction) iui;
													relName = "intersectOfunionOfintersectOf"+"_"+p.getProperty().toString();
													relValue = prefix+p.getFiller().toString();
													//System.out.println(id+  " - " + relName +  " - " + relValue);
													objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
													
												}else if(iui instanceof OWLObjectAllRestriction){
													OWLObjectAllRestriction p = (OWLObjectAllRestriction) iui;
													relName = "intersectOfunionOfintersectOf"+"_"+p.getProperty().toString();
													relValue = prefix+p.getFiller().toString();
													//System.out.println(id+  " - " + relName +  " - " + relValue);
													objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);
													
												}else{										
													relName = "intersectOfunionOfintersectOf";
													relValue = prefix+ui.toString();
													//System.out.println(id+  " - " + relName +  " - " + relValue);	
													objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);		
												}
											}
										}else{									
											relName = "unionOfintersectOf";
											relValue = prefix+ui.toString();									
											//System.out.println(id+  " - " + relName +  " - " + relValue);	
											objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);	
										}				
									}
								}else{									
									relName = "intersectOf";
									relValue = prefix+i.toString();									
									//System.out.println(id+  " - " + relName +  " - " + relValue);	
									objRelSet.addRelationship(relValue, importObj.getAccessionNumber(), relName);	
								}				
							}
						}else{
							//System.out.println(equivClass.toString());
						}
					}
				}
			//}
				//objList = replaceGenidByAttributeValue(objHashMap,location);
				objList = new ArrayList<ImportObj>();	
				for (ImportObj obj : objHashMap.values()){
					objList.add(obj);
				}				
				//System.out.println(objHashMap.size());
				//System.out.println(objList.size());
				System.out.println("ids "+ ontology.getReferencedClasses().size());
				
				super.importIntoTmpTables(objList,objRelSet);
				
	
		} catch (OWLOntologyCreationException e) {
			System.out.println("The ontology could not be created: " + e.getMessage());
		}
	}

	private ArrayList<ImportObj> replaceGenidByAttributeValue(HashMap<String, ImportObj> objHashMap,String location) {
		
		//parsing could be different for other owl-ontologies!!
		
		ArrayList<ImportObj> objList = new ArrayList<ImportObj>();
		boolean openTag = false;
		String genid="";
		String attValue="";	
		
		try {

			URL url = new URL(location);
			URLConnection con=url.openConnection();
			BufferedReader file = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			String line;	
			HashMap<String, String> genid2AttValue	= new HashMap<String, String>();
			
			while ((line=file.readLine())!=null)  {
				if(!openTag && line.startsWith("    <rdf:Description rdf:about=")){
					String[] lineArray = line.split("#");
					genid =lineArray[1].replace("\"", "").replace(">", "");
					openTag = true;						
				}
				if(openTag && line.startsWith("        <rdfs:label rdf:datatype=")){
					String[] lineArray = line.split(">");
					attValue =lineArray[1].replace("</rdfs:label", "");
				}
				if(openTag && line.startsWith("    </rdf:Description>")){
					openTag = false;
					genid2AttValue.put(genid.trim(),attValue.trim());	
				}			
			}
			//System.out.println(genid2AttValue.size());
			for (ImportObj obj : objHashMap.values()){
				for(ImportObjAttribute elem:obj.getAttributeList()){					
					String updateValue = genid2AttValue.get(elem.getValue());
					//System.out.println(elem.getValue()+" - "+updateValue);
					if(updateValue!=null){
						elem.setValue(updateValue);
					}					
				}
				objList.add(obj);
			}		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objList;
	}

	private List<String[]> processClass(String currentClass) {
		
		String attributeName	= null, attributeValue = null;
		List<String[]> results	= new Vector<String[]>();
		String accession= "";
		String pattern	= "<owl:Class rdf:";
		int start		= currentClass.indexOf(pattern)+pattern.length();
		int end			= currentClass.indexOf("\">");
		String sub_str	= currentClass.substring(start,end);
		
		if(sub_str.startsWith("about=")){
			accession = sub_str.substring(sub_str.indexOf("#")+1).replace("\">", "");
			
		}else if(currentClass.substring(start).startsWith("ID=")){
			accession = sub_str.replace("ID=\"", "").replace("\">", "");			
		}
		String[] lines = currentClass.split("\n");
		
		for(int i=0; i<lines.length;i++){
	
			if(lines[i].contains("<oboInOwl:hasRelatedSynonym>")){
				attributeName = "synonym";
				//System.out.println(lines[i]);
				attributeValue = lines[i+3].replace(">", "").replace("</rdfs:label", "").trim();				
				//System.out.println(accession + " - " + attributeName + " - " + attributeValue);				
				String[] triple =  {accession,attributeName,attributeValue};
				results.add(triple);				
			}
			
			if(lines[i].contains("<oboInOwl:hasDefinition>")){
				attributeName = "definition";
				attributeValue = lines[i+3].replace(">", "").replace("</rdfs:label", "").trim();			
				//System.out.println(accession + " - " + attributeName + " - " + attributeValue);			
				String[] triple =  {accession,attributeName,attributeValue};
				results.add(triple);
			}			
		}
		return results;
	}

	@Override
	protected void removeDownloadedSourceData() throws ImportException {
		// TODO Auto-generated method stub

	}
}
