package org.gomma.io.importer.sources.OLD;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
@Deprecated
public class OAEIImportUpdate {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		// mouse=true	-->	Mouse-Anatomy;
		// mouse=false	-->	NCI-Anatomy
		boolean mouse = true;
			
		//parse owl-files for Annotations/Descriptions
		//Update abstract genid in database --> instead of genid "real" definition / synonym
		boolean openTag = false;
		String genid="";
		String attValue="";	
		RandomAccessFile file = null;
		
		String ontology;
		int lds;
		if(mouse){
			ontology = "mouse_anatomy_2010";
			lds = 1;
		}else{
			ontology = "nci_anatomy_2010";
			lds = 2;
		}
		
		try { file = new RandomAccessFile("E:/Ontologien und Datenbanken/Anatomy/2010/"+ontology+".owl","r");}
		catch (FileNotFoundException e) { e.printStackTrace();}
		
		String line;	
		HashMap<String, String> genid2AttValue	= new HashMap<String, String>();
		
		while ((line=file.readLine())!=null)  {
			if(!openTag && line.startsWith("    <rdf:Description rdf:about=")){
				String[] lineArray = line.split("#");
				genid =lineArray[1].replace("\"", "").replace(">", "");
				openTag = true;						
			}
			if(openTag && line.startsWith("        <rdfs:label rdf:datatype=")){
				String[] lineArray = line.split(">");
				attValue =lineArray[1].replace("</rdfs:label", "");
			}
			if(openTag && line.startsWith("    </rdf:Description>")){
				openTag = false;
				genid2AttValue.put(genid,attValue);
			}			
		}
	
		FileWriter pw = new FileWriter("E:/Ontologien und Datenbanken/Anatomy/2010/UPDATE_synonyms_definitions_"+ontology+".txt");
		pw.append("START\n");
		for(String key:genid2AttValue.keySet()){
			String query =	"UPDATE gomma_obj_versions o, gomma_obj_att_values v " +
							"SET v.att_value_s = '"+genid2AttValue.get(key)+"' " +
							"WHERE o.lds_id_fk = " + lds +
							" AND o.obj_id = v.obj_id_fk" +
							" AND v.att_value_s = '"+key+"';\n";
			pw.append(query);		
		}
		pw.append("END");
		pw.flush();
		pw.close();
		
	}
}
