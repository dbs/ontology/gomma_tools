package org.gomma.io.importer.sources.UMLS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailData {

	public static final String SYN = "synonym";


	public static final String PT = "preferredTerm";


	public static final String DEF = "definition";


	public static final String SEM_TYPE = "semType";


	public static final String SEM_TYPE_TREE = "semTypeTree";
	
	
	String cui;
	String name;
	String langName;
	 List<Attribute> synonym;
	 List <Attribute> definiton;
	List<Attribute> prefferedTerms;
List<Attribute> semTypes;
	 List<Attribute> semTreeNr;
	
	public DetailData (){
	}
	
	
	
	public void addAttribute (Attribute a, String attName) {
		if (attName.equals(SYN)){
			if (synonym ==null){
				synonym = new ArrayList<Attribute>();
			}
			synonym.add(a);
		}else if (attName.equals(DEF)){
			if (definiton ==null){
				definiton = new ArrayList<Attribute>();
			}
			definiton.add(a);
		}else if (attName.equals(SEM_TYPE)){
			if (semTypes ==null){
				semTypes = new ArrayList<Attribute>();
			}
			semTypes.add(a);
		}else if (attName.equals(SEM_TYPE_TREE)){
			if (semTreeNr ==null){
				semTreeNr = new ArrayList<Attribute>();
			}
			semTreeNr.add(a);
		}else if (attName.equals(PT)){
			if (prefferedTerms ==null){
				prefferedTerms = new ArrayList<Attribute>();
			}
			prefferedTerms.add(a);
		}
	}
	
	
}
