/**
 * Datencontainer f�r die einzelnen Eintr�ge aus der Datenbank
 * 
 * @author Michael Gassner
 * @version 1.0
 */

package org.gomma.io.importer.sources.UMLS;

import java.util.List;
import java.util.Vector;

public class Data{
	String cui= null;
	String aui= null;	
	String name= null;	
	boolean obsolete = false;
	
	List<String> prefTerms = null;
	List<String> restSynonyms = null;
	List<String> definitions = new Vector<String>();
	List<String> semTypTreeNumbers = new Vector<String>();
	List<String> semTypeNames = new Vector<String>();
	
}