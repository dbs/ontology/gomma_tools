/**
 * UMLS Metathesaurus Importer
 * Queries to UMLS MySQL DB and import into GOMMA Repository
 * 
 * Attention! So far - due to UMLS's size, not all concepts (CUIs) are imported - importIntoTermTables makes some restrictions
 * Currently, these restrictions are case specific and might be adapted in future ..
 *  
 * @author Anika Gross
 * @version 1.0
 * 
 */

package org.gomma.io.importer.sources.UMLS;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.model.DataTypes;

public class UmlsMetathesaurusImporter extends PreSourceImporter {

	public Connection con = null;

	public void loadSourceData() throws ImportException {
	}

	public void importIntoTmpTables() throws ImportException {

		String url = this.getMainImporter().getLocation();
		String user = "umlsUser";
		String pw = "umlsUser123";
		String driver = "com.mysql.jdbc.Driver";

		try {
			Class.forName(driver).newInstance();
			con = DriverManager.getConnection(url, user, pw);
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		HashMap<String, Data> dataset = null;
		try {
			dataset = getUMLSData(con);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		HashMap<String,ImportObj> addedObjs = new HashMap<String,ImportObj>();
		ArrayList<ImportObj> objList = new ArrayList<ImportObj>();
		ImportSourceStructure objRelSet = new ImportSourceStructure();

		for (String cui : dataset.keySet()) {

			ImportObj importObj = null;

			/*
			 * if(!objHashMap.containsKey(cui)){ importObj = new ImportObj(cui);
			 * }else{ importObj = objHashMap.get(cui); }
			 */

			if (dataset.get(cui).name != null) {// PN Name vorhanden
				//System.out.println("PN Name vorhanden");
				if(addedObjs.containsKey(cui)){
					importObj = addedObjs.get(cui);
				}else{
					importObj = new ImportObj(cui);
					addedObjs.put(cui,importObj);
				}
					
				importObj.addAttribute("name", "N/A", DataTypes.STRING,dataset.get(cui).name);
				//System.out.println("name" + " - " +cui + " - " +dataset.get(cui).name);
				
				// alle pref terms und alle synonyme als synonyme
				if (dataset.get(cui).prefTerms != null) {
					for (String pref : dataset.get(cui).prefTerms) {// alle prefTerms hinzufuegen
						importObj.addAttribute("synonym", "N/A",DataTypes.STRING, pref);
						//System.out.println("synonym (prefTerms)" + " - " +cui + " - " +pref);
					}

				}
				if (dataset.get(cui).restSynonyms != null) {
					for (String syn : dataset.get(cui).restSynonyms) {// alle Synonyme hinzufuegen
						importObj.addAttribute("synonym", "N/A",DataTypes.STRING, syn);
						//System.out.println("synonym (restSyn)" + " - " +cui + " - " +syn);
					}
				}
			}
			// falls nicht PN --> PT als name
			else if (dataset.get(cui).name == null && dataset.get(cui).prefTerms != null) {
				//System.out.println("falls nicht PN --> PT als name");
				if(addedObjs.containsKey(cui)){
					importObj = addedObjs.get(cui);
				}else{
					importObj = new ImportObj(cui);
					addedObjs.put(cui,importObj);
				}
				
				importObj.addAttribute("name", "N/A", DataTypes.STRING, dataset.get(cui).prefTerms.get(0));
				//System.out.println("name" + " - " +cui + " - " +dataset.get(cui).prefTerms.get(0));
				
				for (int i = 1; i < dataset.get(cui).prefTerms.size(); i++) {
					importObj.addAttribute("synonym", "N/A", DataTypes.STRING, dataset.get(cui).prefTerms.get(i));
					//System.out.println("synonym (prefTerms)" + " - " +cui + " - " +dataset.get(cui).prefTerms.get(i));
				}
				if (dataset.get(cui).restSynonyms != null) {
					for (String syn : dataset.get(cui).restSynonyms) {// alle Synonyme hinzufuegen
						importObj.addAttribute("synonym", "N/A", DataTypes.STRING, syn);
						//System.out.println("synonym (restSyn)" + " - " +cui + " - " +syn);
					}
				}
			}
			// falls nicht PN & nicht PT --> Syn als name
			else if (dataset.get(cui).name == null && dataset.get(cui).prefTerms == null && (cui.equals("C1848676") 
																						|| cui.equals("C2242529")
																						|| cui.equals("C0021430") 
																						|| cui.equals("C0011900")
																						|| cui.equals("C0023671") 
																						|| cui.equals("C0027646") 
																						|| cui.equals("C0025677"))) {
				//System.out.println("falls nicht PN & nicht PT --> Syn als name");
				// falls kein preferred name vorhanden, nimm Synonym als Name
				// ACHTUNG den 3. Fall!!!! //nur die fuer diesen im
				// Anwendungsfall n�tigen 7 weiteren betroffenen CUIs,
				// sonst 2,8 Mio UMLS CUIs importiert -> OutOFMem beim Matching

				if(addedObjs.containsKey(cui)){
					importObj = addedObjs.get(cui);
				}else{
					importObj = new ImportObj(cui);
					addedObjs.put(cui,importObj);
				}
				
				if (dataset.get(cui).restSynonyms != null) {
					importObj.addAttribute("name", "N/A", DataTypes.STRING,	dataset.get(cui).restSynonyms.get(0));
					//System.out.println("name" + " - " +cui + " - " +dataset.get(cui).restSynonyms.get(0));
					
					for (int i = 1; i < dataset.get(cui).restSynonyms.size(); i++) {
						importObj.addAttribute("synonym", "N/A", DataTypes.STRING,dataset.get(cui).restSynonyms.get(i));
						//System.out.println("synonym (restSyn)" + " - " +cui + " - " +dataset.get(cui).restSynonyms.get(i));
					}
				}
			}
		}
		for(String cui:addedObjs.keySet()){
			
			ImportObj importObj = addedObjs.get(cui);
			
			for (String def : dataset.get(cui).definitions) {
				importObj.addAttribute("definition", "N/A", DataTypes.STRING,def);
			}
			if (dataset.get(cui).obsolete) {
				importObj.addAttribute("isObsolete", "N/A", DataTypes.STRING,"true");
			} else {
				importObj.addAttribute("isObsolete", "N/A", DataTypes.STRING,"false");
			}

			for (String semTypeName : dataset.get(cui).semTypeNames) {
				importObj.addAttribute("semTypeName", "N/A", DataTypes.STRING,semTypeName);
			}
			for (String semTypeNr : dataset.get(cui).semTypTreeNumbers) {
				importObj.addAttribute("semTypeNr", "N/A", DataTypes.STRING,semTypeNr);
			}
			objList.add(addedObjs.get(cui));
		}
		
		System.out.println("Import " +objList.size() + " concepts to GOMMA ..");
		super.importIntoTmpTables(objList, objRelSet);
	}

	@Override
	protected void removeDownloadedSourceData() throws ImportException {
		// TODO Auto-generated method stub
	}

	private HashMap<String, Data> getUMLSData(Connection conn) {HashMap<String, Data> dataset = new HashMap<String, Data>();
		float start = System.currentTimeMillis();
		try {
			
			System.out.println("Get UMLS CUIs + concept information (name, synonyms..) ..");

			String query = "SELECT DISTINCT CUI, STR, SUPPRESS, TTY "
					+ "FROM MRCONSO "
					+ "WHERE (LAT = 'ENG' OR LAT = 'GER' OR LAT = 'eng' OR LAT = 'ger') "
					+ "AND isPref = 'Y' ";
				    /*+ "AND CUI IN ('C0001675','C0332157','C0441942','C1546899','C0011900','C0005893'," 
				    + "'C1305866','C0019046','C1281911','C0019016','C0313263','C1706180','C0023516','C1306620',"
					+ "'C0589120','C0013216','C0476658','C0232804','C1847014','C0010054','C1956346','C0795623',"
					+ "'C0019158','C0019159','C1305399','C1698960','C0220908','C1882087','C0201544','C1366489',"
					+ "'C2939420','C0035078','C0302148','C1417325','C1547219','C1305849','C2229679','C1705576',"
					+ "'C0428315','C1261155','C1261155','C0428315','C0373638','C0202054','C0149783')";*/
			// "AND CUI IN ('C1848676','C2242529','C0021430','C0011900','C0023671','C0027646','C0025677')";
			// AND TS = 'P'";

			// Objekt zum Ausfuehren von Queries
			PreparedStatement psmt = conn.prepareStatement(query);
			ResultSet rs = psmt.executeQuery(query);
			List<String> cuis = new Vector<String>();
			Data currentdata;
			while (rs.next()) {
				String cui = rs.getString(1);
				// System.out.println(cui);
				if (dataset.containsKey(cui)) { // falls schon in Gesamt Dataset enthalten, ergaenzen nur Preferred Name und Synonyme
					// System.out.println("dataset contained for "+cui);
					currentdata = dataset.get(cui);

					// NAME UND SYNONYME
					if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PN") && currentdata.name == null) { // PT = PREFERRED NAME = NAME
						currentdata.name = (rs.getString(2));
					} else if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PT")) { // PT = PREFERRED TERM = TERM
						if(currentdata.prefTerms==null){
							List<String> prefTerms = new Vector<String>();
							prefTerms.add(rs.getString(2));
							currentdata.prefTerms = prefTerms;
						}else{
							currentdata.prefTerms.add(rs.getString(2));
						}
						
					} else if (rs.getString(2) != null && !rs.getString(4).equalsIgnoreCase("PN") && !rs.getString(4).equalsIgnoreCase("PT")) {// !PT = SYNONYM						
						
						if(currentdata.restSynonyms==null){
							List<String> synonyms = new Vector<String>();
							synonyms.add(rs.getString(2));
							currentdata.restSynonyms = synonyms;
						}else{
							currentdata.restSynonyms.add(rs.getString(2));
						}
					}
					// OBSOLETE STATUS
					
					// PRUEFEN rs.getString(2) != null
					if (rs.getString(2) != null && !currentdata.obsolete && rs.getString(3).equalsIgnoreCase("O")) {
						currentdata.obsolete = true;
					}
				} else {// erstes Hinzufuegen des Codes + weitere Daten

					// System.out.println("add new dateset for "+cui);
					currentdata = new Data();
					// CUI
					if (rs.getString(1) != null) {
						currentdata.cui = (cui);
					}
					// cui Liste fuer spaeter
					cuis.add(cui);
					// NAME UND SYNONYME
					if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PN") && currentdata.name == null) { // PT = PREFERRED
															// NAME = NAME
						// System.out.println("add name first");
						currentdata.name = (rs.getString(2));
					} else if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PT")) { 
						// PT = PREFERRED NAME = NAME
						// System.out.println("add name first");
						List<String> prefTerms = new Vector<String>();
						prefTerms.add(rs.getString(2));
						currentdata.prefTerms = prefTerms;
					} else if (rs.getString(2) != null && !rs.getString(4).equalsIgnoreCase("PN") && !rs.getString(4).equalsIgnoreCase("PT")) {
						// !PT = SYNONYM
						// System.out.println("add syn first");
						List<String> synonyms = new Vector<String>();
						synonyms.add(rs.getString(2));
						currentdata.restSynonyms = synonyms;

					}
					// OBSOLETE STATUS
					if (rs.getString(3) != null && !currentdata.obsolete && rs.getString(3).equalsIgnoreCase("O")) {
						currentdata.obsolete = true;
					}
					// zum Gesamt Dataset hinzufuegen
					dataset.put(cui, currentdata);
				}
			}
			psmt.close();
			System.out.println(dataset.size() + " CUIs selected");
			String[] cuiArray = cuis.toArray(new String[cuis.size()]);

			// Definitions
			System.out.println("Get defintions ..");
			String query2 = "SELECT CUI, DEF FROM MRDEF WHERE CUI IN (" + preparePlaceHolders(cuiArray.length) + ");";
			PreparedStatement psmt2 = conn.prepareStatement(query2);
			setValues(psmt2, cuiArray);
			ResultSet rs2 = psmt2.executeQuery();

			while (rs2.next()) {
				String cui = rs2.getString(1);
				dataset.get(cui).definitions.add(rs2.getString(2));
			}
			psmt2.close();

			// Semantic Types
			System.out.println("Get semantic types ..");
			String query3 = "SELECT CUI, STN, STY FROM MRSTY WHERE CUI IN (" + preparePlaceHolders(cuiArray.length) + ");";
			PreparedStatement psmt3 = conn.prepareStatement(query3);
			setValues(psmt3, cuiArray);
			ResultSet rs3 = psmt3.executeQuery();

			while (rs3.next()) {
				String cui = rs3.getString(1);
				dataset.get(cui).semTypTreeNumbers.add(rs3.getString(2));
				dataset.get(cui).semTypeNames.add(rs3.getString(3));
			}
			psmt3.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		float end = (System.currentTimeMillis()-start)/(float)60000;
		System.out.println("Load data from UMLS repository done in "+end+" min.");
		return dataset;
	}

	public static String preparePlaceHolders(int length) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length;) {
			builder.append("?");
			if (++i < length) {
				builder.append(",");
			}
		}
		return builder.toString();
	}

	public static void setValues(PreparedStatement preparedStatement,
			String[] values) throws SQLException {
		for (int i = 0; i < values.length; i++) {
			preparedStatement.setObject(i + 1, values[i]);
		}
	}
}