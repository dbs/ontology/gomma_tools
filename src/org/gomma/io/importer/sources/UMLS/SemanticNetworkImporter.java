package org.gomma.io.importer.sources.UMLS;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.model.DataTypes;

public class SemanticNetworkImporter extends PreSourceImporter{
	public static final String SEM_TYPES = "SELECT UI,STY,DEF FROM SRDEF S where RT='STY'";
	public static final String RELS = "Select p.UI, c.UI, s.RL from SRDEF c,SRDEF p, SRSTR s"+
			 " where c.STY=s.STY2 and p.STY = s.STY1 and c.RT='STY' and p.RT='STY'" ;
	String user = "umlsUser";
	String pw = "umlsUser123";
	String driver = "com.mysql.jdbc.Driver";
	Connection con;
	@Override
	protected void loadSourceData() throws ImportException {
		
		
	}
	@Override
	protected void importIntoTmpTables() throws ImportException {
		try {
			String url = this.getMainImporter().getLocation();
			Class.forName(driver).newInstance();
			con = DriverManager.getConnection(url, user, pw);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SEM_TYPES);
			HashMap<String,ImportObj> semMap = new HashMap<String,ImportObj>();
			List<ImportObj> list = new ArrayList<ImportObj>();
			while (rs.next()){
				ImportObj o = new  ImportObj(rs.getString(1));
				String name = rs.getString(2);
				o.addAttribute("name_ENG", "N/A", DataTypes.STRING, name);
				o.addAttribute("definition_ENG", "N/A", DataTypes.STRING, rs.getString(3));
				semMap.put(rs.getString(1), o);
				list.add(o);
			}
			ResultSet rs2 =  stmt.executeQuery(RELS);
			ImportSourceStructure structure = new ImportSourceStructure();
			while (rs2.next()){
				String p = rs2.getString(1);
				String c = rs2.getString(2);
				if (semMap.containsKey(p)&&semMap.containsKey(c)){
					structure.addRelationship(p, c, rs2.getString(3));
				}
				
			}
			stmt.close();
			con.close();
			this.importIntoTmpTables(list, structure);
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	protected void removeDownloadedSourceData() throws ImportException {
		// TODO Auto-generated method stub
		
	}
	
}
