/**
 * UMLS Metathesaurus Importer
 * Queries to UMLS MySQL DB and import into GOMMA Repository
 * 
 * Attention! So far - due to UMLS's size, not all concepts (CUIs) are imported - importIntoTermTables makes some restrictions
 * Currently, these restrictions are case specific and might be adapted in future ..
 *  
 * @author Anika Gross
 * @version 1.0
 * 
 */

package org.gomma.io.importer.sources.UMLS;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;

import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.model.DataTypes;

public class UmlsMetathesaurusImporterWithRels extends PreSourceImporter {

	public Connection con = null;

	private HashSet<String> missingBlockedCodes ;
	 public static final String GET_ISA = "SELECT  Distinct CUI1 as child, CUI2 as par, RELA from MRREL"
			 +" where REL ='PAR' "
	    		+ "  LIMIT ? OFFSET ?";

	private String missingPath = "realMissing.txt";
	HashMap<String, DetailData> dataset = null;
	public void loadSourceData() throws ImportException {
	}

	public void importIntoTmpTables() throws ImportException {

		String url = this.getMainImporter().getLocation();
		String user = "umlsUser";
		String pw = "umlsUser123";
		String driver = "com.mysql.jdbc.Driver";
		
		try {
			Class.forName(driver).newInstance();
			con = DriverManager.getConnection(url, user, pw);
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.missingBlockedCodes = this.readMissingFile();
		
		HashMap<String,List<String>> relations = null;
		try {
			
			
			dataset = getUMLSData(con);
			System.out.println("get relations....");
			relations = this.getRelations();
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		HashMap<String,ImportObj> addedObjs = new HashMap<String,ImportObj>();
		ArrayList<ImportObj> objList = new ArrayList<ImportObj>();
		ImportSourceStructure objRelSet = new ImportSourceStructure();
		int size=0; 
		for (String cui : dataset.keySet()) {

			ImportObj importObj = null;

			/*
			 * if(!objHashMap.containsKey(cui)){ importObj = new ImportObj(cui);
			 * }else{ importObj = objHashMap.get(cui); }
			 */
			DetailData d = dataset.get(cui);
			if (d.name != null) {// PN Name vorhanden
				//System.out.println("PN Name vorhanden");
				if(addedObjs.containsKey(cui)){
					importObj = addedObjs.get(cui);
				}else{
					importObj = new ImportObj(cui);
					addedObjs.put(cui,importObj);
				}
				String langName = d.langName;
				importObj.addAttribute("name_"+langName, "N/A", DataTypes.STRING,d.name);
				//System.out.println("name" + " - " +cui + " - " +dataset.get(cui).name);
				if(d.synonym!=null){
					for (Attribute a : d.synonym){
						String lang = a.lang;
						String scope ="N/A";
						if (a.scope.equals("O"))
							scope = "obsolete";
						importObj.addAttribute("synonym_"+lang, scope ,DataTypes.STRING,a.value); 
					}
				}else if (d.prefferedTerms!=null){
					for (Attribute a :d.prefferedTerms){
						String lang = a.lang;
						String scope ="N/A";
						if (!a.scope.equals("O"))
							scope = "preferred";
						importObj.addAttribute("synonym_"+lang, scope ,DataTypes.STRING,a.value); 
					}
				}
			}
			// falls nicht PN --> PT als name
			
			else if (d.prefferedTerms != null){
				String langName = d.langName;
				List<Attribute> ptList = d.prefferedTerms;
				if (ptList != null) {
					//System.out.println("falls nicht PN --> PT als name");
					if(addedObjs.containsKey(cui)){
						importObj = addedObjs.get(cui);
					}else{
						importObj = new ImportObj(cui);
						addedObjs.put(cui,importObj);
					}
					
					importObj.addAttribute("name_"+ptList.get(0).lang, "N/A", DataTypes.STRING, ptList.get(0).value);
					//System.out.println("name" + " - " +cui + " - " +dataset.get(cui).prefTerms.get(0));
					d.langName = ptList.get(0).lang;
					for (int i =1;i<ptList.size(); i++){
						Attribute a = ptList.get(i);
						String lang = a.lang;
						String scope ="N/A";
						if (!a.scope.equals("O"))
							scope = "preferred";
						importObj.addAttribute("synonym_"+a.lang, scope ,DataTypes.STRING,a.value); 
					}
					List <Attribute> synAtts = d.synonym;
					if (synAtts != null) {
						for (Attribute a : synAtts) {// alle Synonyme hinzufuegen
							String lang = a.lang;
							String scope ="N/A";
							if (a.scope.equals("O"))
								scope = "obsolete";
							importObj.addAttribute("synonym_"+lang, scope ,DataTypes.STRING,a.value); 
						}
					}
				}	
			}else if (this.missingBlockedCodes.contains(cui)){
				size++;
				//System.out.println("falls nicht PN & nicht PT --> Syn als name");
				// falls kein preferred name vorhanden, nimm Synonym als Name
				// ACHTUNG den 3. Fall!!!! //nur die fuer diesen im
				// Anwendungsfall n�tigen 7 weiteren betroffenen CUIs,
				// sonst 2,8 Mio UMLS CUIs importiert -> OutOFMem beim Matching
				
				if(addedObjs.containsKey(cui)){
					importObj = addedObjs.get(cui);
				}else{
					importObj = new ImportObj(cui);
					addedObjs.put(cui,importObj);
				}
				List <Attribute> synAtts = d.synonym;
				if (synAtts != null) {
				String langName = synAtts.get(0).lang;
					importObj.addAttribute("name_"+langName, "N/A", DataTypes.STRING,synAtts.get(0).value);
					//System.out.println("name" + " - " +cui + " - " +dataset.get(cui).restSynonyms.get(0));
					d.langName = synAtts.get(0).lang;
					for (int i =1;i<synAtts.size(); i++){
						Attribute a = synAtts.get(i);
						String lang = a.lang;
						String scope ="N/A";
						if (a.scope.equals("O"))
							scope = "obsolete";
						importObj.addAttribute("synonym_"+lang, scope ,DataTypes.STRING,a.value); 
					}
				}
			}
		}
		for(String cui:addedObjs.keySet()){
			ImportObj importObj = addedObjs.get(cui);
			List<Attribute> defList = dataset.get(cui).definiton;
			if (defList!=null){
				for (Attribute def : defList) {
					String lang = def.lang;
					if (lang ==null){
						lang = dataset.get(cui).langName;
					}
					String scope ="N/A";
					importObj.addAttribute("definition_"+lang, scope ,DataTypes.STRING,def.value); 
				}
			}
			List<Attribute> sempTypes = dataset.get(cui).semTypes;
			if (sempTypes!=null){
				for (Attribute def : sempTypes) {
					
					String lang = def.lang;
					if (lang ==null){
						lang = dataset.get(cui).langName;
					}
					String scope ="N/A";
					importObj.addAttribute("semTypeName_"+lang, scope ,DataTypes.STRING,def.value); 
				}
			}
			List<Attribute> sempTypTree = dataset.get(cui).semTreeNr;
			if (sempTypes!=null){
				for (Attribute def : sempTypTree) {
					
					String lang = def.lang;
					if (lang ==null){
						lang = dataset.get(cui).langName;
					}
					String scope ="N/A";
					importObj.addAttribute("semTypeNr_"+lang, scope ,DataTypes.STRING,def.value); 
				}
			}
			objList.add(addedObjs.get(cui));
			
			List<String> children = relations.get(cui);
				if (children!=null){
				for (String c:children){
						objRelSet.addRelationship(cui, c, "is_a");
				}
			}	
		}
		
		System.out.println("Import " +objList.size() + " concepts to GOMMA ..");
		super.importIntoTmpTables(objList, objRelSet);
	}

	@Override
	protected void removeDownloadedSourceData() throws ImportException {
		// TODO Auto-generated method stub
	}

	private HashMap<String, DetailData> getUMLSData(Connection conn) {
		dataset = new HashMap<String, DetailData>();
		float start = System.currentTimeMillis();
		try {
			
			System.out.println("Get UMLS CUIs + concept information (name, synonyms..) ..");

			String query = "SELECT DISTINCT CUI, STR, SUPPRESS, TTY,LAT "
					+ "FROM MRCONSO "
					+ "WHERE (lcase(LAT) = 'eng' OR lcase(LAT) = 'ger')"
					+ "AND isPref = 'Y' ";
				    /*+ "AND CUI IN ('C0001675','C0332157','C0441942','C1546899','C0011900','C0005893'," 
				    + "'C1305866','C0019046','C1281911','C0019016','C0313263','C1706180','C0023516','C1306620',"
					+ "'C0589120','C0013216','C0476658','C0232804','C1847014','C0010054','C1956346','C0795623',"
					+ "'C0019158','C0019159','C1305399','C1698960','C0220908','C1882087','C0201544','C1366489',"
					+ "'C2939420','C0035078','C0302148','C1417325','C1547219','C1305849','C2229679','C1705576',"
					+ "'C0428315','C1261155','C1261155','C0428315','C0373638','C0202054','C0149783')";*/
			// "AND CUI IN ('C1848676','C2242529','C0021430','C0011900','C0023671','C0027646','C0025677')";
			// AND TS = 'P'";

			// Objekt zum Ausfuehren von Queries
			PreparedStatement psmt = conn.prepareStatement(query);
			ResultSet rs = psmt.executeQuery(query);
			DetailData currentdata;
			while (rs.next()) {
				String lang = rs.getString(5);
				String cui = rs.getString(1);
				// System.out.println(cui);
				if (dataset.containsKey(cui)) { // falls schon in Gesamt Dataset enthalten, ergaenzen nur Preferred Name und Synonyme
					// System.out.println("dataset contained for "+cui);
					currentdata = dataset.get(cui);
					// NAME UND SYNONYME
					if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PN") && currentdata.name == null) { // PT = PREFERRED NAME = NAME
						currentdata.name = (rs.getString(2));
						if (currentdata.langName==null)
							currentdata.langName=lang;
					} else if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PT")) { // PT = PREFERRED TERM = TERM
						Attribute a= new Attribute(rs.getString(2), lang, "preferred");
						currentdata.addAttribute(a,DetailData.PT);
					} else if (rs.getString(2) != null && !rs.getString(4).equalsIgnoreCase("PN") && !rs.getString(4).equalsIgnoreCase("PT")) {// !PT = SYNONYM							
						String scope= "N/A";
						if (rs.getString(3).equalsIgnoreCase("O")){
							scope= "O";
						}
						Attribute a = new Attribute(rs.getString(2), lang, scope);
						currentdata.addAttribute(a,DetailData.SYN);
						
					}
				} else {// erstes Hinzufuegen des Codes + weitere Daten

					// System.out.println("add new dateset for "+cui);
					currentdata = new DetailData();
					// CUI
					if (rs.getString(1) != null) {
						currentdata.cui = (cui);
					}
					
					// NAME UND SYNONYME
					if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PN") && currentdata.name == null) { // PT = PREFERRED NAME = NAME
						currentdata.name = (rs.getString(2));
						if (currentdata.langName==null)
							currentdata.langName=lang;
					} else if (rs.getString(2) != null && rs.getString(4).equalsIgnoreCase("PT")) { // PT = PREFERRED TERM = TERM
							Attribute a = new Attribute(rs.getString(2), lang, "preferred");
							currentdata.addAttribute(a,DetailData.PT);
					} else if (rs.getString(2) != null && !rs.getString(4).equalsIgnoreCase("PN") && !rs.getString(4).equalsIgnoreCase("PT")) {// !PT = SYNONYM						
							String scope= "N/A";
							if (rs.getString(3).equalsIgnoreCase("O")){
								scope= "O";
							}
							Attribute a = new Attribute(rs.getString(2), lang, scope);
							currentdata.addAttribute(a,DetailData.SYN);
						
					}
					// zum Gesamt Dataset hinzufuegen
					dataset.put(cui, currentdata);
				}
			}
			rs.close();
			psmt.close();
			System.out.println(dataset.size() + " CUIs selected");
			HashSet<String> removeKeys = new HashSet<String>();
			for (Entry <String,DetailData> e: this.dataset.entrySet()){
				if (e.getValue().name == null && e.getValue().prefferedTerms ==null&&
					!this.missingBlockedCodes.contains(e.getKey())){
						removeKeys.add(e.getKey());
					}
			}
			for (String k:removeKeys){
				this.dataset.remove(k);
			}
			System.out.println("reduced size: "+ dataset.size());
			String[] cuiArray =dataset.keySet().toArray(new String[]{});
			
			// Definitions
			System.out.println("Get defintions ..");
			String query2 = "SELECT CUI, DEF FROM MRDEF WHERE CUI IN (" + preparePlaceHolders(cuiArray.length) + ");";
			PreparedStatement psmt2 = conn.prepareStatement(query2);
			setValues(psmt2, cuiArray);
			ResultSet rs2 = psmt2.executeQuery();
			
			while (rs2.next()) {
				String cui = rs2.getString(1);
				Attribute a = new Attribute(rs2.getString(2), dataset.get(cui).langName, "N/A");
				dataset.get(cui).addAttribute(a,DetailData.DEF);
			}
			rs2.close();
			psmt2.close();

			// Semantic Types
			System.out.println("Get semantic types ..");
			String query3 = "SELECT CUI, STN, STY FROM MRSTY WHERE CUI IN (" + preparePlaceHolders(cuiArray.length) + ");";
			PreparedStatement psmt3 = conn.prepareStatement(query3);
			setValues(psmt3, cuiArray);
			ResultSet rs3 = psmt3.executeQuery();

			while (rs3.next()) {
				String cui = rs3.getString(1);
				Attribute a = new Attribute(rs3.getString(2), dataset.get(cui).langName, "N/A");
				dataset.get(cui).addAttribute(a,DetailData.SEM_TYPE_TREE);
				
				Attribute a2 = new Attribute(rs3.getString(3), dataset.get(cui).langName, "N/A");
				dataset.get(cui).addAttribute(a2,DetailData.SEM_TYPE);
			}
			rs3.close();
			psmt3.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		float end = (System.currentTimeMillis()-start)/(float)60000;
		System.out.println("Load data from UMLS repository done in "+end+" min.");
		return dataset;
	}
	
	
	private HashMap<String,List<String>> getRelations () throws SQLException{
		HashMap <String, List<String>> relations = new HashMap<String,List<String>>();
		PreparedStatement stmt = con.prepareStatement(GET_ISA);
		stmt.setQueryTimeout(3600*5);
		boolean found=true;
		int offset =0;
		int limit = 100000;
		while (found){
			found=false;
			stmt.setInt(1, limit);stmt.setInt(2, offset);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				found =true;
				String type = rs.getString(3);
				if (type ==null){
					type = "";
				}
				String parent = rs.getString(2);
				String child = rs.getString(1);
				/*if (type.equals("inverse_isa")){
					String tmp = parent;
					parent = child;
					child = tmp;
				}*/
				if (type.equals("inverse_isa")||type.equals("")||type.equals("isa")){
					List<String> relation = relations.get(parent);
					if (relation ==null && dataset.containsKey(parent)){
						relation = new ArrayList<String>();
						relations.put(parent, relation);
					}
					if (dataset.containsKey(child)&&relation!=null)
						relation.add(child);	
				}
			}
			offset = offset+limit;
			System.out.println("fetch relations:"+offset);
		}
		stmt.close();
		return relations;
	}

	public static String preparePlaceHolders(int length) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length;) {
			builder.append("?");
			if (++i < length) {
				builder.append(",");
			}
		}
		return builder.toString();
	}
	
	private HashSet<String> readMissingFile(){
		BufferedReader br;
		HashSet<String> codes = new HashSet<String>();
		try {
			br = new BufferedReader(new FileReader(this.missingPath));
			while(br.ready()){
				String line = br.readLine();
				String[]values =line.split(";");
				for (String v: values){
					codes.add(v);
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("missning codes:" +codes.size());
		return codes;
	}

	public static void setValues(PreparedStatement preparedStatement,
			String[] values) throws SQLException {
		for (int i = 0; i < values.length; i++) {
			preparedStatement.setObject(i + 1, values[i]);
		}
	}
}