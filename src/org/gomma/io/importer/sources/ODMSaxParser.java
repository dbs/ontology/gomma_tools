package org.gomma.io.importer.sources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;



public class ODMSaxParser extends DefaultHandler {

	private Stack<String> stack;
	
	private HashMap<String,Item> itemMap;
	
	private HashMap<String,Group> groupMap; 
	
	private Item currentItem ;
	
	private Document currentDocument;
	private String prefix;
	private Group currentGroup;
	private String currentLang;
	StringBuilder sb;
	public void startDocument() throws SAXException{
		itemMap = new HashMap<String,Item>();
		groupMap = new HashMap<String,Group>();
		stack = new Stack<String>();
	}
	
	public void endDocument() throws SAXException{
		
	}
	
	
	public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException{
		stack.push(qName);
		sb = new StringBuilder();
		if (attrs.getValue("xml:lang")!=null){
			if(attrs.getValue("xml:lang").equalsIgnoreCase("en")){
			currentLang = "_ENG";
			}else if (attrs.getValue("xml:lang").equalsIgnoreCase("de")){
				currentLang = "_GER";
			}else {
				currentLang ="";
			}
			
		}
		if (qName.equalsIgnoreCase("ODM")){
			currentDocument = new Document();
			currentDocument.identifier = attrs.getValue("Description");
		}else if (qName.equalsIgnoreCase("study")){
			prefix = attrs.getValue("OID");
		}else if (qName.equalsIgnoreCase("ItemGroupRef") && stack.get(stack.size()-2).equals("FormDef")){
			Group g = new Group();
			g.id = prefix+":"+attrs.getValue("ItemGroupOID");
			if (attrs.getValue("OrderNumber")!=null)
				g.values.put("order_number", attrs.getValue("OrderNumber"));
			groupMap.put(g.id, g);
		}else if (qName.equalsIgnoreCase("ItemGroupDef")){
			String id = prefix+":"+attrs.getValue("OID");
			Group g = groupMap.get(id);
			
			if (g ==null){
				g = new Group();
				g.id = id;
				groupMap.put(id, g);
			}
			currentGroup = g;
			g.values.put("name", attrs.getValue("Name"));
			//g.values.put("lang", attrs.getValue("xml:lang"));
			
		}else if (qName.equalsIgnoreCase("ItemRef") && 
				stack.get(stack.size()-2).equals("ItemGroupDef")){
			Item i = new Item();
			i.id = prefix+":"+attrs.getValue("ItemOID");
			if (attrs.getValue("OrderNumber")!=null)
				i.addValue("order_number", attrs.getValue("OrderNumber"));
			itemMap.put(i.id, i);
			if (currentGroup!=null){
				currentGroup.groupItems.add(i.id);
			}
		}else if (qName.equalsIgnoreCase("ItemDef")){
			String id = prefix+":"+attrs.getValue("OID");
			Item i = itemMap.get(id);
			
			if (i ==null){
				i = new Item();
				i.id = id;
				itemMap.put(id, i);
			}
			currentItem = i;
			i.addValue("name", attrs.getValue("Name"));
			//i.values.put("lang", attrs.getValue("xml:lang"));
			if ( attrs.getValue("DataType")!=null)
			i.addValue("datatype", attrs.getValue("DataType"));
		}else if (qName.equalsIgnoreCase("TranslatedText")){
			
		}
	}
	
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException{
		String elem = stack.pop();
		if (currentGroup!=null){
			if (stack.get(stack.size()-2).equals("ItemGroupDef")&&
					elem.equals("TranslatedText")){
				this.groupMap.get(currentGroup.id).values.put("description"+currentLang,sb.toString());
			}
		}else if (currentItem != null){
			String esc = sb.toString();
			if ( stack.get(stack.size()-1).equalsIgnoreCase("question")&&
				elem.equalsIgnoreCase("TranslatedText")){
				this.itemMap.get(currentItem.id).addValue("question"+currentLang,esc);
			}else if ( stack.get(stack.size()-1).equalsIgnoreCase("Description")&&
					elem.equalsIgnoreCase("TranslatedText")){
					this.itemMap.get(currentItem.id).addValue("description"+currentLang,esc);
			}
		}
		
		if(qName.equals("ItemGroupDef")){
			currentGroup =null;
		}else if (qName.equals("ItemDef")){
			currentItem = null;
		}else if (qName.equals("TranslatedText")){
			sb.delete(0, sb.length());
		}
	}
	
	public void characters(char[] buf, int offset, int len) throws SAXException{
		String v =  new String(buf,offset,len).trim();
		
		if (v.length()!=0){
			sb.append(v);
		}
	}
	
	public HashMap<String,Item> getItemMap() {
		return itemMap;
	}

	public void setItemMap(HashMap<String,Item> itemMap) {
		this.itemMap = itemMap;
	}

	public Document getCurrentDocument() {
		return currentDocument;
	}

	public HashMap<String, Group> getGroupMap() {
		return groupMap;
	}

	class Item {
		 HashMap<String, HashSet<String>> values;
		 String id;
		
		Item (){
			values = new HashMap<String,HashSet<String>>();
		}
		
		Item (String id){
			this.id = id;
			values = new HashMap<String,HashSet<String>>();
		}
		
		void addValue(String key , String value){
			HashSet<String> list = this.values.get(key);
			if (list ==null){
				list = new HashSet<String>();
				values.put(key, list);
			}
			list.add(value);
		}
	}
	
	 class Group {

		 HashMap<String,String> values;
 
		HashSet<String> groupItems;
		/**
		 * concatenation of study id and group id 
		 */
		 String id;
		 
		
		
		Group(){
			values = new HashMap<String,String>();
			groupItems = new HashSet<String>();
		}
	
	}
	
	
	class Document {
		String identifier;
		String fileName;
		String creationDateTime;
		 String getIdentifier() {
			return identifier;
		 }
	}
	
}
