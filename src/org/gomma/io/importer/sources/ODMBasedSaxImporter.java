package org.gomma.io.importer.sources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.gomma.Gomma;
import org.gomma.GommaImpl;
import org.gomma.api.cache.CacheManager;
import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.SourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.model.DataTypes;
import org.gomma.model.attribute.Attribute;
import org.gomma.util.SystemPropertyPool;
import org.xml.sax.SAXException;

public class ODMBasedSaxImporter extends PreSourceImporter{

	public void importIntoTmpTables() throws ImportException {

		long start, duration;
		ODMSaxParser parser;
		start = System.currentTimeMillis();
		
		System.out.print("Starting to load resources into temp area ...");
		try {
			//Exception in thread "main" org.gomma.exceptions.ImportException: insertTemporaryObjectSet(): Duplicate entry 'http://who.int/icf#ICFCategory' for key 'PRIMARY'
			SourceVersionImportAPI api = super.getImportAPI();
			HashMap<String, ImportObj>	objHashMap	= new HashMap<String, ImportObj>();	
			ImportSourceStructure		objRelSet	= new ImportSourceStructure();
			ArrayList<ImportObj>		objList		= new ArrayList<ImportObj>();
			String location 		= super.getMainImporter().getLocation(); //System.out.println(location);
			InputStream inputStream	= new FileInputStream(location);
			parser = new ODMSaxParser();
			SAXParser p =SAXParserFactory.newInstance().newSAXParser();
			p.parse(inputStream,parser);
			for (Entry<String, ODMSaxParser.Group> entry : parser.getGroupMap().entrySet())
			{
				String currentAcc = entry.getValue().id;
				ImportObj importObj = null; 
				if(!objHashMap.containsKey(currentAcc)){
					importObj = new ImportObj(currentAcc);
				}else{
					importObj = objHashMap.get(currentAcc);
				}
								
				for (Entry<String, String> entryAttributes : entry.getValue().values.entrySet()){
					
					importObj.addAttribute(entryAttributes.getKey(), "N/A", DataTypes.STRING, entryAttributes.getValue());
				}
				importObj.addAttribute("type", "N/A", DataTypes.STRING, "group");
				objHashMap.put(currentAcc,importObj);
			}
			
			
			
			for (Entry<String, ODMSaxParser.Item> entry : parser.getItemMap().entrySet())
			{
				String currentAcc = entry.getValue().id;
				ImportObj importObj = null; 
				if(!objHashMap.containsKey(currentAcc)){
					importObj = new ImportObj(currentAcc);
				}else{
					importObj = objHashMap.get(currentAcc);
				}
								
				for (Entry<String, HashSet<String>> entryAttributes : entry.getValue().values.entrySet()){
					for (String s :entryAttributes.getValue()){
						importObj.addAttribute(entryAttributes.getKey(), "N/A", DataTypes.STRING, s);
					}
					
				}
				importObj.addAttribute("type", "N/A", DataTypes.STRING, "item");
				objHashMap.put(currentAcc,importObj);
			}
			
			for(Entry<String, ODMSaxParser.Group> entry : parser.getGroupMap().entrySet()){
				for (String iid: entry.getValue().groupItems){
					objRelSet.addRelationship(iid, entry.getValue().id, "part_of");
				}
			}
			for (ImportObj obj : objHashMap.values()){
				objList.add(obj);
			}
			super.importIntoTmpTables(objList,objRelSet);
			
		} catch (RepositoryException e){
			throw new ImportException(e.getMessage());
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void loadSourceData() throws ImportException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void removeDownloadedSourceData() throws ImportException {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	public static void main (String[]args){
		Gomma g = new GommaImpl();
		try {
		g.initialize(args[0]);
		HashMap<String,String>properties = new HashMap<String,String>();
		properties.put(SourceImporter.ACCESSIONSPREFIX, "");
		properties.put(SourceImporter.IMPORTER, "org.gomma.io.importer.sources.ODMBasedSaxImporter");
		properties.put(SourceImporter.LOCATION, "//informatik.informatik.uni-leipzig.de/ifi/"
				+ "home/christen/eigene_Dateien/MDM_form_44v1.xml");
		properties.put(SourceImporter.OBJECTTYPE, "MedicalEntity");
		properties.put(SourceImporter.SOURCE, "MDM_form_44v1");
		properties.put(SourceImporter.VERSION, "v1");
		properties.put(SourceImporter.TIMESTAMP, "2001-04-03");
		
			g.getSourceManager().importSource(properties);
		} catch (ImportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GommaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
