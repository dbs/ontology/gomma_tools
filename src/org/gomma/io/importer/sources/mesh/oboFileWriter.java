package org.gomma.io.importer.sources.mesh;

import java.io.*;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class oboFileWriter {
	
	private Hashtable<String, String> alternativeIDs = new Hashtable<String, String>();

	public void write(Hashtable<String, MeSH> meshs) throws IOException{
		
		// 0. Preprocessing (Hashmap f�r alternative IDs erstellen)
		Iterator i01 = meshs.keySet().iterator();
		while (i01.hasNext()){
		  Object prekey = i01.next();
		  Vector<String> temptTree = meshs.get(prekey).treenumbers;
		  Iterator i02 = temptTree.iterator();
		  while (i02.hasNext()){
			  alternativeIDs.put(i02.next().toString(), meshs.get(prekey).name);
		  }
		}
		
		// 1. Datei anlegen ohne R�cksicht auf Verluste
	    File datei = new File("D:/test.obo");
	    // 2. PIPE:
	    // 2.1. �ffnen
	    FileWriter schreiber = new FileWriter(datei);
		// 2.2. schreiben
	    // 2.2.1. Vorverarbeitung
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MeSH:root");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:A");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Anatomy");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:B");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Organisms");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:C");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Diseases");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:D");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Chemicals and Drugs");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:E");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Analytical, Diagnostic and Therapeutic Techniques and Equipment");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:F");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Psychatry and Psychology");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:G");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Biological Sciences");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:H");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Physical Sciences");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:I");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Anthropology, Education, Sociology and Social Phenomena");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:J");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Technology and Food and Beverages");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:K");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Humanities");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:L");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Information Science");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:M");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Persons");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:N");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Health Care");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:V");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Publication Characteristics");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("[Term]");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("id: MESH:Z");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("name: Geographic Locations");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write("is_a: MeSH:root ! MESH");
	    schreiber.write(Character.LINE_SEPARATOR);
	    schreiber.write(Character.LINE_SEPARATOR);
	    
	    // 2.2.2. Ausgelesene Daten schreiben
	    Iterator i1 = meshs.keySet().iterator();
		while (i1.hasNext()){
		  Object key = i1.next();
		  schreiber.write("[Term]");
		  schreiber.write(Character.LINE_SEPARATOR);
		  Vector<String> treenumbers = meshs.get(key).treenumbers;
		  Iterator treeIt = treenumbers.iterator();
		  schreiber.write("id: MESH:" + insertDot(treeIt.next().toString()));
		  schreiber.write(Character.LINE_SEPARATOR);
		  while (treeIt.hasNext()){
			  schreiber.write("alt_id: MESH:" + insertDot(treeIt.next().toString()));
			  schreiber.write(Character.LINE_SEPARATOR);
		  }
		  schreiber.write("name: " + meshs.get(key).name);
		  schreiber.write(Character.LINE_SEPARATOR);
		  schreiber.write("def: \"" + meshs.get(key).definition + "\"");
		  schreiber.write(Character.LINE_SEPARATOR);
		  Vector<String> tempSyn = meshs.get(key).synonym;
		  Iterator i2 = tempSyn.iterator();
		  while (i2.hasNext()){
			  schreiber.write("synonym: \"" + i2.next().toString() + "\" []");
			  schreiber.write(Character.LINE_SEPARATOR);
		  }
		  Vector<String> temptXref = meshs.get(key).xref;
		  Iterator i3 = temptXref.iterator();
		  while (i3.hasNext()){
			  schreiber.write("xref: UMLS:" + i3.next().toString());
			  schreiber.write(Character.LINE_SEPARATOR);
		  }
		  Vector<String> temptrees = meshs.get(key).treenumbers;
		  Iterator i4 = temptrees.iterator();
		  while (i4.hasNext()){
			  String temptree = i4.next().toString();
			  if (5 < temptree.length()){
				  String tempIsa = temptree.substring(0, temptree.length()-4);
				  schreiber.write("is_a: MESH:" + insertDot(tempIsa) + " ! " + alternativeIDs.get(tempIsa));
				  schreiber.write(Character.LINE_SEPARATOR);
			  }	else{
				  if (temptree.contains("A")){
					  schreiber.write("is_a: MESH:A ! Anatomy");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("B")){
					  schreiber.write("is_a: MESH:B ! Organisms");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("C")){
					  schreiber.write("is_a: MESH:C ! Diseases");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("D")){
					  schreiber.write("is_a: MESH:D ! Chemicals and Drugs");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("E")){
					  schreiber.write("is_a: MESH:E ! Analytical, Diagnostic and Therapeutic Techniques and Equipment");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("F")){
					  schreiber.write("is_a: MESH:F ! Psychiatry and Psychology");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("G")){
					  schreiber.write("is_a: MESH:G ! Biological Sciences");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("H")){
					  schreiber.write("is_a: MESH:H ! Physical Sciences");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("I")){
					  schreiber.write("is_a: MESH:I ! Anthropology, Education, Sociology and Social Phenomena");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("J")){
					  schreiber.write("is_a: MESH:J ! Technology and Food and Beverages");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("K")){
					  schreiber.write("is_a: MESH:K ! Humanities");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("L")){
					  schreiber.write("is_a: MESH:L ! Information Science");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("M")){
					  schreiber.write("is_a: MESH:M ! Persons");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("N")){
					  schreiber.write("is_a: MESH:N ! Health Care");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("V")){
					  schreiber.write("is_a: MESH:V ! Publication Characteristics");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
				  if (temptree.contains("Z")){
					  schreiber.write("is_a: MESH:Z ! Geographic Locations");
					  schreiber.write(Character.LINE_SEPARATOR);
				  }
			  }
		  }
		  schreiber.write(Character.LINE_SEPARATOR);
		}
		schreiber.write("[Typedef]");
		schreiber.write(Character.LINE_SEPARATOR);
		schreiber.write("id: is_a");
		schreiber.write(Character.LINE_SEPARATOR);
		schreiber.write("name: is a");
		schreiber.write(Character.LINE_SEPARATOR);
	    // 2.3. schliessen 
	    schreiber.close();
	}
	
	//Punkt einf�gen in Baumpfad
	private String insertDot(String temp){
		String newDot = "dotplaceholder";
		if (temp.contains("A")){
			newDot = temp.replace("A", "A.");
		  }
		  if (temp.contains("B")){
			  newDot = temp.replace("B", "B.");
		  }
		  if (temp.contains("C")){
			  newDot = temp.replace("C", "C.");
		  }
		  if (temp.contains("D")){
			  newDot = temp.replace("D", "D.");
		  }
		  if (temp.contains("E")){
			  newDot = temp.replace("E", "E.");
		  }
		  if (temp.contains("F")){
			  newDot = temp.replace("F", "F.");
		  }
		  if (temp.contains("G")){
			  newDot = temp.replace("G", "G.");
		  }
		  if (temp.contains("H")){
			  newDot = temp.replace("H", "H.");
		  }
		  if (temp.contains("I")){
			  newDot = temp.replace("I", "I.");
		  }
		  if (temp.contains("J")){
			  newDot = temp.replace("J", "J.");
		  }
		  if (temp.contains("K")){
			  newDot = temp.replace("K", "K.");
		  }
		  if (temp.contains("L")){
			  newDot = temp.replace("L", "L.");
		  }
		  if (temp.contains("M")){
			  newDot = temp.replace("M", "M.");
		  }
		  if (temp.contains("N")){
			  newDot = temp.replace("N", "N.");
		  }
		  if (temp.contains("V")){
			  newDot = temp.replace("V", "V.");
		  }
		  if (temp.contains("Z")){
			  newDot = temp.replace("Z", "Z.");
		  }
		return newDot;
	}

	
}