package org.gomma.io.importer.sources.mesh;

import javax.xml.parsers.*;

import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;
import java.util.Hashtable;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;


public class MeSHXMLReader extends DefaultHandler {
	
	public final static String parserClass = "org.apache.xerces.parsers.SAXParser";
	private StringBuffer elementTextBuf = new StringBuffer();
	private Vector<String> xmlPath = new Vector<String>();
	private static String filename;
	private MeSH currMesh;
	private Hashtable<String, MeSH> meshs = new Hashtable<String, MeSH>();
	
	private String getParent() {
	    return (xmlPath.size() > 1) ? (String) xmlPath.elementAt(xmlPath.size()-2) : "";
	  }
	
	private String getParentOfParent() {
		return (xmlPath.size() > 1) ? (String) xmlPath.elementAt(xmlPath.size()-3) : "";
	}
	
	private static void doIt (String filename)throws IOException, SAXException, ParserConfigurationException{
		SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler(new MeSHXMLReader());
        xmlReader.parse(filename);
	}

	
	static public void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
    	
		long startTime = System.currentTimeMillis();
		filename = "D:/desc2011.xml";
		doIt(filename);
    	long endTime = System.currentTimeMillis();
    	long elapsedTime = endTime - startTime;
    	System.out.println("ben�tigte Zeit: "+elapsedTime+"ms");    	
    	
    }
	
	//Wird vom Parser am Anfang des Dokuments aufgerufen
	public void startDocument() throws SAXException {
        System.out.println("Starte Dokument " + filename);
    }
	
	//Wird vom Parser beim Start eines Elements aufgerufen
	public void startElement(String namespaceURI, String localName, String rawName, Attributes atts) throws SAXException {
		//neues Element -> Textinhalt zur�ck setzen
		elementTextBuf.setLength(0);
		//aktuellen Elemennamen an Pfad anf�gen
		xmlPath.addElement(rawName);
	}
	
	//Wird vom Parser am Ende eines Elements aufgerufen
	public void endElement(String namespaceURI, String localName, String rawName) {
		
		//entferne Whitespace an Zeichendatengrenzen
		String elementText = elementTextBuf.toString().trim();
		
		//MeSH-Objekt zwischenspeichern
		try {
			if (rawName.equals("DescriptorUI") && getParent().equals("DescriptorRecord")){
				if (currMesh != null && !currMesh.treenumbers.isEmpty()){
					meshs.put(currMesh.treenumbers.get(0), currMesh);
					}
				if (currMesh != null && currMesh.treenumbers.isEmpty()){
					System.out.println(currMesh.name);
					}
				currMesh = new MeSH();
				currMesh.id = elementText;
				}
			//Name auslesen
			if (rawName.equals("String") && getParent().equals("DescriptorName") && getParentOfParent().equals("DescriptorRecord")){
				currMesh.name = elementText;
				}
			//Synonyme auslesen
			if (rawName.equals("String") && getParent().equals("ConceptName")){
				if (!elementText.equals(currMesh.name) && !currMesh.synonym.contains(elementText)){
					currMesh.synonym.add(elementText);
				}
			}
			if (rawName.equals("String") && getParent().equals("Term")){
				if (!elementText.equals(currMesh.name) && !currMesh.synonym.contains(elementText)){
					currMesh.synonym.add(elementText);
				}
			}
			//treenumber auslesen
			if (rawName.equals("TreeNumber")){
				currMesh.treenumbers.add(elementText);
				}
			//Definition auslesen
			if (rawName.equals("ScopeNote")){
				currMesh.definition = elementText;
				}
			//xref(ConceptUMLSUI) auslesen
			if (rawName.equals("ConceptUMLSUI")){
				currMesh.xref.add(elementText);
				}
		
		} finally {
			// Element zu ende -> Textinhalt zuruecksetzen
		    // notwendig bei mixed content
		    elementTextBuf.setLength(0);
		    // Element vom Pfad entfernen
		    xmlPath.setSize(xmlPath.size()-1);
		}
	}
	
	//Wird vom Parser mit Textinhalt des aktuellen Elements aufgerufen
	public void characters(char[] ch, int start, int length) {
	    elementTextBuf.append(ch, start, length);
	}
	
	//Wird vom Parser am Ende des Dokuments aufgerufen
	public void endDocument() throws SAXException {
		if (currMesh != null){
			meshs.put(currMesh.treenumbers.firstElement(), currMesh);
			}
		//schreiben in obo-File
		oboFileWriter writer = new oboFileWriter();
		try {
			writer.write(meshs);
		} catch (IOException e) {
			e.printStackTrace();
		}
        }

}
