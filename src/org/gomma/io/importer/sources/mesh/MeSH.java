package org.gomma.io.importer.sources.mesh;

import java.util.Vector;

public class MeSH{
	String id = "";
	String name = "";
	String definition = "";
	Vector<String> treenumbers = new Vector<String>();
	Vector<String> synonym = new Vector<String>();
	Vector<String> xref = new Vector<String>();
}