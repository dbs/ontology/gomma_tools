package org.gomma.io.importer.sources;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.ImportException;
import org.gomma.io.importer.PreSourceImporter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SourceFromMappingXMLImporter extends PreSourceImporter {
	
	private SourceFromMappingXMLParser xmlParser;
	
	public void loadSourceData() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from file system ...");
		
		try {
			xmlParser = new SourceFromMappingXMLParser(this);
			xmlParser.readSourceObjectsFromXML();
		} catch (GommaException e) {
			throw new ImportException(e.getMessage());
		}
		
		duration = (System.currentTimeMillis()-start);
		System.out.println("  Done ! ("+duration+" ms)");
	}
	
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		
		try {
			SourceVersionImportAPI api = super.getImportAPI();
			List<String[]> accessionsToImport = new Vector<String[]>();
			for (int i=0;i<xmlParser.accessions.size();i++) {
				accessionsToImport.add(new String[]{xmlParser.accessions.get(i)});
			}
			api.insertIntoTmpTables(accessionsToImport,"insert into tmp_objects (accession) values (?)");
		} catch (GommaException e) {
			throw new ImportException(e.getMessage());
		}
		duration = (System.currentTimeMillis()-start);
		System.out.println("  Done ! ("+duration+" ms)");
	}
	
	public void removeDownloadedSourceData() throws ImportException {
	}
	
	public class SourceFromMappingXMLParser extends DefaultHandler {
		public SourceFromMappingXMLImporter importer;
		public List<String> accessions;
		
		public SourceFromMappingXMLParser(SourceFromMappingXMLImporter importer) {
			this.importer = importer;
			this.accessions = new Vector<String>();
		}
		
		public void readSourceObjectsFromXML() throws GommaException {
			  try {
				SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
				saxParser.parse( new File( this.importer.getMainImporter().getLocation() ), this );
			} catch (ParserConfigurationException e) {
				throw new GommaException(e.getMessage());
			} catch (SAXException e) {
				throw new GommaException(e.getMessage());
			} catch (IOException e) {
				throw new GommaException(e.getMessage());
			}
		}
		public void startDocument() throws SAXException {   
		}
		public void endDocument() throws SAXException {
		}
		public void startElement( String namespaceURI,String localName,String qName,Attributes attrs ) throws SAXException {
			if (qName.equals("object")) {
				String currentAccession = null;
				String currentSource = null;
				String currentObjectType = null;
				if (attrs!=null) {
		    		for (int i=0;i<attrs.getLength();i++) {
		    			String attName = attrs.getLocalName(i);
		    			String attValue = attrs.getValue(i).trim();
		    			if (attName.equalsIgnoreCase("accession")) {
		    				currentAccession = attValue;
		    			} else if (attName.equalsIgnoreCase("source_name")) {
		    				currentSource = attValue;
		    			} else if (attName.equalsIgnoreCase("objecttype")) {
		    				currentObjectType = attValue;
		    			} else if (attName.equalsIgnoreCase("source")) {
		    				String[] tmpSplit = attValue.split("@");
		    				currentObjectType = tmpSplit[0];
		    				currentSource = tmpSplit[1];
		    			}
		    		}
		    	}
				if (currentAccession!=null&&currentObjectType!=null&&currentSource!=null) {
					if (currentObjectType.equalsIgnoreCase(importer.getMainImporter().getObjectType())&&
							currentSource.equalsIgnoreCase(importer.getMainImporter().getSourceName())) {
						if (!this.accessions.contains(currentAccession)) {
							this.accessions.add(currentAccession);
						}
					}
				}
			}
		}
		public void endElement(String namespaceURI,String localName,String qName ) throws SAXException {
		}
		public void characters( char[] buf, int offset, int len ) throws SAXException {
		}
	}
}
