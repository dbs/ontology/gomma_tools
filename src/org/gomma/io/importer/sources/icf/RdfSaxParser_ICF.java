package org.gomma.io.importer.sources.icf;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RdfSaxParser_ICF extends DefaultHandler
{

	private Stack<String>				stack				= null;
	public Map<String, ParserConcept>	parsedConcepts		= null;
	//private String						_caching			= "";
	//private String						_cachBuffer			= "";
	private boolean						_include			= false;
	//private boolean						_getCharacters		= false;
	private ParserConcept				_actualTopConcept	= null;
	private String[]					_actualProperty     = null;
	public int							tagCounter			= 0;
	public boolean 						genIDs_found		= false;
	public Set<String>					languages_found		= new HashSet<String>();
	private String						namespace			= "";
	public Map<String, String>			inverseIndex		= null;
	public int numberOfRetries								= 0;
	private StringBuffer elementTextBuf = new StringBuffer();
	
	//
	// SourceVersionStructure
	//

	public RdfSaxParser_ICF(String uri)
	{
		doParsing(uri);
		/*try
		{
			long timeStart = System.currentTimeMillis();
			// Use an instance of ourselves as the SAX event handler
			DefaultHandler handler = this;
			// Parse the input with the default (non-validating) parser
			SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
			URLConnection con = new URL(uri).openConnection();
			InputStream inputStream = con.getInputStream();
			Reader reader = new InputStreamReader(inputStream,"UTF-8");
  	      	InputSource is = new InputSource(reader);
  	      	is.setEncoding("UTF-8");
			saxParser.parse(is, handler);
			//System.out.println("Runtime: " + (System.currentTimeMillis() - timeStart) + " ms");
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			//Make retry?
			
			System.exit(2);
		}*/
	}
	
	public static void main(String[] args) {
		
		RdfSaxParser_ICF pa = new RdfSaxParser_ICF("http://rest.bioontology.org/bioportal/ontologies/download/47404?apikey=4ea81d74-8960-4525-810b-fa1baab576ff&userapikey=");
	}
	
	public void doParsing(String uri) {
		try
		{
			long timeStart = System.currentTimeMillis();
			// Use an instance of ourselves as the SAX event handler
			DefaultHandler handler = this;
			// Parse the input with the default (non-validating) parser
			SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
			URLConnection con = new URL(uri).openConnection();
			InputStream inputStream = con.getInputStream();
			Reader reader = new InputStreamReader(inputStream,"UTF-8");
  	      	InputSource is = new InputSource(reader);
  	      	is.setEncoding("UTF-8");
			saxParser.parse(is, handler);
			//System.out.println("Runtime: " + (System.currentTimeMillis() - timeStart) + " ms");
		}
		catch (Throwable t)
		{
			System.err.println(t.getMessage());
			//Do Retry
			numberOfRetries++;
			if (numberOfRetries<3) {
				resetVariables();
				doParsing(uri);
			} else {
				System.out.println("\007\007");
				System.out.println("Tried to parse ontology "+numberOfRetries+" times without success, giving up now !!");
			}
		}
	}
	
	public void resetVariables() {
		stack 				= null;
		parsedConcepts 		= null;
		_include 			= false;
		_actualTopConcept	= null;
		_actualProperty     = null;
		tagCounter			= 0;
		genIDs_found		= false;
		languages_found		= new HashSet<String>();
		namespace			= "";
		inverseIndex		= null;
	}

	public void startDocument() throws SAXException
	{
		parsedConcepts = new HashMap<String, RdfSaxParser_ICF.ParserConcept>();
		inverseIndex = new HashMap<String, String>();
		stack = new Stack<String>();
	}

	public void endDocument() throws SAXException
	{
		//System.out.println("concepts:" + parsedConcepts.size());
		int numberOfEntries = 0;
		for (ParserConcept c : this.parsedConcepts.values())
		{
			// System.out.println(c);
			numberOfEntries += c.attributes.size();
		}
		System.out.println("subentries in concepts: " + numberOfEntries);
		
	}

	public void startElement(String namespaceURI, String localName, String qName, Attributes attrs) throws SAXException
	{
		elementTextBuf.setLength(0);
		stack.push(qName+"$"+(++tagCounter));

		if (qName.equals("rdf:RDF")) {
			this.namespace = attrs.getValue("xmlns");
		}
		
		if(qName.equals("owl:Class")){
			//System.out.println("id: "+attrs.getValue("rdf:ID"));
		}
		// System.out.println(stack);System.out.println(stack.size());
		// System.out.println(qName);

		// get data in depth search collecting mode e.g. subclass of : owl:restriction...
		/*if (!_caching.equals(""))
		{
			cachingData(qName, attrs);
			return;
		}*/
		
		
		// node is CLASS node, creating ParserConcept
		// stack size to get sure that the class node is at the correct depth (not a subnode)
		if ((qName.equals("owl:Class")||qName.equals("rdf:Description")) && stack.size() == 2)
		{
			_include = true;

			//classCounter++;
			for (int i = 0; i < attrs.getLength(); i++)
			{
				String attName = attrs.getQName(i);
				if (attName.equalsIgnoreCase("rdf:about") || attName.equalsIgnoreCase("rdf:ID"))
				{
					String accession = attrs.getValue(i).trim();
					accession = fixNamespace(accession);
					ParserConcept c = new ParserConcept(accession);
					this.parsedConcepts.put(qName + "$" + tagCounter, c);
					this.inverseIndex.put(accession, qName + "$" + tagCounter);
					c.addAttribute("type", "class");
					c.type = "class";
					_actualTopConcept = c;
					//Take last part of the URI as name
					int nameIndex = accession.indexOf("#");
					if (nameIndex>=0) {
						c.addAttribute("name"+"$"+tagCounter, accession.substring(nameIndex+1));
					} else {
						c.addAttribute("name"+"$"+tagCounter, accession);
					}
					// System.out.println("Accession: " + attrs.getValue(i).trim());
				}
				else
					System.out.println("Error in owl:class, no name found");
			}
		}

		if ((qName.equals("owl:ObjectProperty")||qName.equals("owl:DatatypeProperty")
				||qName.equalsIgnoreCase("owl:FunctionalProperty")||qName.equalsIgnoreCase("owl:InverseFunctionalProperty")) && stack.size() == 2)
		{
			_include = true;
			for (int i = 0; i < attrs.getLength(); i++)
			{
				String attName = attrs.getQName(i);
				if (attName.equalsIgnoreCase("rdf:about") || attName.equalsIgnoreCase("rdf:ID"))
				{
					String accession = attrs.getValue(i).trim();
					accession = fixNamespace(accession);
					ParserConcept c = new ParserConcept(accession);
					c.addAttribute("type", "property");
					c.type = "property";
					this.parsedConcepts.put(qName + "$" + tagCounter, c);
					this.inverseIndex.put(accession, qName + "$" + tagCounter);
					_actualTopConcept = c;
					//Take last part of the URI as name
					int nameIndex = accession.indexOf("#");
					if (nameIndex>=0) {
						c.addAttribute("name"+"$"+tagCounter, accession.substring(nameIndex+1));
					} else {
						c.addAttribute("name"+"$"+tagCounter, accession);
					}
					// System.out.println("Accession: " + attrs.getValue(i).trim());
				}
				else
					System.out.println("Error in owl:Property, no name found");
			}
		}
		// do not collect data from other unwanted nodes which do not have a ParseConcept
		if (_include)
		{
			ParserConcept c = _actualTopConcept;

			if (qName.equals("rdfs:label"))
			{
				if (attrs.getLength() > 0)
				{
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("xml:lang"))
						{
							languages_found.add(attrs.getValue(i).trim());
						}
					}
				}
				//_getCharacters = true;
			}
			else if (qName.equalsIgnoreCase("oboInOwl:hasRelatedSynonym")) 
			{
				if (attrs.getLength() > 0)
				{
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("rdf:resource"))
						{
							c.addRelation("hasGenID"+"$"+tagCounter, attrs.getValue(i).trim());
							genIDs_found = true;
						}
					}
				}
			}
			else if (qName.equalsIgnoreCase("rdfs:subClassOf"))
			{
				// this subClassOf is created inline
				if (attrs.getLength() == 0)
				{
					// _caching = qName;
					// _cachBuffer = "";
					// _getCharacters = true;
				}
				else
				// subClassof is linked to a known node
				{
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("rdf:resource"))
						{
							String target = attrs.getValue(i).trim();
							target = fixNamespace(target);
							c.addRelation("rdfs:subClassOf"+"$"+tagCounter, target);
							// System.out.println(attrs.getValue(i).trim());
						}
					}
				}
			}
			else if (qName.equalsIgnoreCase("owl:onProperty"))
			{
				if(stack.get(stack.size()-2).startsWith("owl:Restriction")&&stack.get(stack.size()-3).startsWith("rdfs:subClassOf")) {
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("rdf:resource"))
						{
							if (_actualProperty!=null&&_actualProperty[1]!=null) {
								_actualProperty[0] = attrs.getValue(i).trim()+"$"+tagCounter;
								c.addRelation(_actualProperty[0], _actualProperty[1]);
								_actualProperty = null;
							} else {
								_actualProperty = new String[2];
								_actualProperty[0] = attrs.getValue(i).trim()+"$"+tagCounter;
							}
							
							//_actualProperty = new String[2];
							//_actualProperty[0] = attrs.getValue(i).trim()+"$"+tagCounter;
						}
					}
				}
			}
			else if (qName.equalsIgnoreCase("owl:someValuesFrom")||qName.equalsIgnoreCase("owl:allValuesFrom"))
			{
				if(stack.get(stack.size()-2).startsWith("owl:Restriction")&&stack.get(stack.size()-3).startsWith("rdfs:subClassOf")) {
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("rdf:resource"))
						{
							if (_actualProperty!=null&&_actualProperty[0]!=null) {
								_actualProperty[1] = attrs.getValue(i).trim();//+"$"+tagCounter;
								c.addRelation(_actualProperty[0], _actualProperty[1]);
								_actualProperty = null;
							} else {
								_actualProperty = new String[2];
								_actualProperty[1] = attrs.getValue(i).trim();//+"$"+tagCounter;
							}
							//_actualProperty[1] = attrs.getValue(i).trim();
							//c.addRelation(_actualProperty[0], _actualProperty[1]);
						}
					}
				}
			}
			/*else if (qName.equalsIgnoreCase("owl:disjointWith"))
			{
				if (attrs.getLength() > 0)
				{
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("rdf:resource"))
						{
							c.addRelation("owl:disjointWith", attrs.getValue(i).trim());
							// System.out.println(attrs.getValue(i).trim());
						}
					}
				}
			}*/
			else if (qName.equals("owl:ObjectProperty"))
			{
				if(stack.get(stack.size()-2).startsWith("owl:onProperty")&&stack.get(stack.size()-3).startsWith("owl:Restriction")&&stack.get(stack.size()-4).startsWith("rdfs:subClassOf")) {
					for (int i = 0; i < attrs.getLength(); i++)
					{
						String attName = attrs.getQName(i);
						if (attName.equalsIgnoreCase("rdf:about")||attName.equalsIgnoreCase("rdf:ID"))
						{
							if (_actualProperty!=null&&_actualProperty[1]!=null) {
								_actualProperty[0] = attrs.getValue(i).trim()+"$"+tagCounter;
								c.addRelation(_actualProperty[0], _actualProperty[1]);
								_actualProperty = null;
							} else {
								_actualProperty = new String[2];
								_actualProperty[0] = attrs.getValue(i).trim()+"$"+tagCounter;
							}
						}
					}
				}
			}
			else if ((qName.equals("rdfs:domain")||qName.equals("rdfs:range"))&&stack.get(stack.size()-2).startsWith("owl:ObjectProperty")) 
			{
				String attribute;
				if (qName.equals("rdfs:domain")) {
					attribute = "domain";
				} else {
					attribute = "range";
				}
				for (int i = 0; i < attrs.getLength(); i++)
				{
					String attName = attrs.getQName(i);
					if (attName.equalsIgnoreCase("rdf:resource"))
					{
						String accession = attrs.getValue(i).trim();
						accession = fixNamespace(accession);						
						//_actualTopConcept = c;
						//Take last part of the URI as name
						//Store URIs for domain and range to later resolve labels, synonyms of the concepts
						//int nameIndex = accession.indexOf("#");
						//if (nameIndex>=0) {
						//	c.addAttribute(attribute+"$"+tagCounter, accession.substring(nameIndex+1));
						//} else {
						c.addAttribute(attribute+"$"+tagCounter, accession);
						//}
					}
				}
			}
			else
			{
				if (stack.size() == 3) {
					//System.out.println(qName);
				}
			}
		}
	}

	// depth search collecting of node and attributes
	/*private void cachingData(String qName, Attributes attrs)
	{
		_cachBuffer += " / " + qName;

		if (attrs.getLength() > 0)
		{
			_cachBuffer += "[";
			for (int i = 0; i < attrs.getLength(); i++)
			{
				_cachBuffer += attrs.getQName(i) + ":" + attrs.getValue(i).trim();
			}
			_cachBuffer += "]";
		}
	}*/

	public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	{
		if(qName.equals("icd:label") && stack.get(stack.size()-2).contains("icd:DefinitionTerm")){
		
			ParserConcept c = _actualTopConcept;
			String elementText = elementTextBuf.toString().trim();
			c.addAttribute("icd:DefinitionTerm", elementText);
			//System.out.println("definition: "+elementText);
			
		}
		
		if(qName.equals("skos:altLabel") && stack.get(stack.size()-2).contains("owl:Class")){
			String elementText = elementTextBuf.toString().trim();
			//System.out.println("rdfs:label: " + elementText);
			}
			
		elementTextBuf.setLength(0);
		stack.pop();

		// no need to look up in out ranged areas
		if (!_include)
			return;

		// when owl:class is closed, do not collect data until new ParseConcept is created
		if ((qName.equalsIgnoreCase("owl:Class")||qName.equalsIgnoreCase("rdf:Description")||qName.equalsIgnoreCase("owl:ObjectProperty")) && stack.size() < 2)
			_include = false;

		// the closing tag of the node was found which subdata should be collected
		/*if (qName.equalsIgnoreCase(_caching))
		{
			ParserConcept c = _actualTopConcept;
			c.addAttribute(qName, _cachBuffer);

			_caching = "";
			_getCharacters = false;
		}*/
	}
	
	public String fixNamespace(String accession) {
		if (!accession.startsWith("http")) {
			if (accession.startsWith("#")) {
				accession = accession.substring(1,accession.length());
			}
			accession = this.namespace + accession;
		}
		return accession;
	}

	public void characters(char[] buf, int offset, int len) throws SAXException
	{
		elementTextBuf.append(buf, offset, len);
		if (!stack.isEmpty() && _include)
		{
			String s = new String(buf, offset, len);
			if (stack.size()>2) {
				ParserConcept c = parsedConcepts.get(stack.get(stack.size()-2));
				String attributeName = stack.peek();
				//Block relationships which do not belong to attributes
				if (attributeName.startsWith("rdfs:subClassOf")||attributeName.startsWith("owl:equivalentClass")) {
					return;
				}
				if (c!=null&&s.length()>0) {
					c.addAttribute(attributeName, s);
				}
			}
		}
	}

	public class ParserConcept
	{
		public String				accession;
		public Map<String, String>	attributes;
		public Map<String, String>	relations;
		public String  				type;

		public ParserConcept(String accession)
		{
			this.accession = accession;
			this.attributes = new HashMap<String, String>();
			this.relations = new HashMap<String, String>();
			this.type = "class";
		}

		public void addAttribute(String key, String value)
		{
			if (attributes.containsKey(key))
			{
				String currentValue = attributes.get(key);
				currentValue += value;
				attributes.put(key, currentValue);
			}
			else
			{
				attributes.put(key, value);
			}
		}

		public void addRelation(String key, String value)
		{
			
				relations.put(key, value);
			
		}
		
		public boolean hasGenIDRelation() {
			for (String key : relations.keySet()) {
				if (key.contains("hasGenID")) {
					return true;
				}
			}
			return false;
		}

		public String toString()
		{
			StringBuffer result = new StringBuffer(accession + "\n");
			for (String attName : this.attributes.keySet())
			{
				result.append(attName + " >> " + this.attributes.get(attName) + "\n");
			}
			for (String relName : this.relations.keySet())
			{
				result.append(relName + " >R> " + this.relations.get(relName) + "\n");
			}

			return result.toString();
		}
	}
}
