package org.gomma.io.importer.sources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Vector;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;

public class SimpleCSVImporter extends PreSourceImporter {

	private List<String[]> accessions;
	
	public void loadSourceData() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from file system ...");
		if (accessions==null) {
			accessions = new Vector<String[]>();
		}
		try {
			String location = this.getMainImporter().getLocation();
			RandomAccessFile csvFile = new RandomAccessFile(location,"r");
			String currentLine;
			while ((currentLine=csvFile.readLine())!=null) {
				accessions.add(new String[]{currentLine.trim()});
			}
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		
		try {
			SourceVersionImportAPI api = super.getImportAPI();
			api.insertIntoTmpTables(accessions,"insert into tmp_objects (accession) values (?)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
			
		duration = (System.currentTimeMillis()-start);
		System.out.println("  Done ! ("+duration+" ms)");
	}
	
	public void removeDownloadedSourceData() throws ImportException {
	}

}
