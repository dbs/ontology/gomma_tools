package org.gomma.io.importer.sources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.gomma.api.cache.CacheManager;
import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.io.importer.sources.RdfSaxParser.ParserConcept;
import org.gomma.model.DataTypes;
import org.gomma.model.attribute.Attribute;

public class SAXBasedOWLImporterURI extends PreSourceImporter {
	
	private RdfSaxParser	_pars;
	
	public SAXBasedOWLImporterURI() {
		super();
	}	
	
	public void loadSourceData() throws ImportException {}
	
	public void importIntoTmpTables() throws ImportException {

		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources into temp area ...");
		try {
			//Exception in thread "main" org.gomma.exceptions.ImportException: insertTemporaryObjectSet(): Duplicate entry 'http://who.int/icf#ICFCategory' for key 'PRIMARY'
			SourceVersionImportAPI api = super.getImportAPI();
			
			String location			= super.getMainImporter().getLocation();
			URL physicalURL			= new URL(location);
			URLConnection conn		= physicalURL.openConnection();
			InputStream inputStream	= conn.getInputStream();
			_pars 					= new RdfSaxParser(inputStream);
			
			HashMap<String, ImportObj>	objHashMap	= new HashMap<String, ImportObj>();				
			ArrayList<ImportObj>		objList		= new ArrayList<ImportObj>();
			ImportSourceStructure		objRelSet	= new ImportSourceStructure();
//			Problem languages used kann nicht genutzt werden, GOMMA Schema ändern? 
//			List<String> languagesUsed = new Vector<String>(_pars.languages_found);			
//			Source s = new Source.Builder(srcID).objectType(objType).physicalSourceName(pds).isOntology(true).languagesUsed(languagesUsed).build();

			//resolve GenID mappings
			if (_pars.genIDs_found) {
				this.resolveGenIDs(_pars);
			}
			// loop through parsed concepts
			for (Map.Entry<String, RdfSaxParser.ParserConcept> entry : _pars.parsedConcepts.entrySet())
			{
				String currentAcc = entry.getValue().accession;
				ImportObj importObj = null; 
				if(!objHashMap.containsKey(currentAcc)){
					importObj = new ImportObj(currentAcc);
				}else{
					importObj = objHashMap.get(currentAcc);
				}
								
				for (Map.Entry<String, String> entryAttributes : entry.getValue().attributes.entrySet())
				{
					Attribute att = getAttribute(entryAttributes.getKey());
					//was ist mit synonymen?
					//resolveGenIDs(RdfSaxParser parser)
					if(att!=null) {

						if (entry.getValue().type.equalsIgnoreCase("property")&&(att.getName().equalsIgnoreCase("domain")||att.getName().equalsIgnoreCase("range"))) {
							//Umsetzung der URIs auf Originalnamen
							List<String> originalLabels = this.getOriginalLabels(entryAttributes.getValue());
							for (String value : originalLabels) {
								importObj.addAttribute(att.getName(), "N/A", DataTypes.STRING, value);
							}
						} else {
							// Attribute zu Obj hinzufuegen
							importObj.addAttribute(att.getName(), "N/A", DataTypes.STRING, entryAttributes.getValue());
						}
					}
				}
				objHashMap.put(currentAcc,importObj);
			}
			
			// ReLoop for relationships after adding of all objects to SVS
			for (Map.Entry<String, RdfSaxParser.ParserConcept> entry : _pars.parsedConcepts.entrySet())
			{
				String currentAcc = entry.getValue().accession;
				// Relationships
				for (Map.Entry<String, String> entryRelations : entry.getValue().relations.entrySet())
				{
					String type = getRelationship(entryRelations.getKey()); // --> relation e.g. subclassOf ??
					if (type!=null) {
						String parentAcc = entryRelations.getValue();
						if (parentAcc.contains("owl#Thing")) {
							//Block Thing Object
						} else {
							parentAcc = _pars.fixNamespace(parentAcc);
							if (!objHashMap.containsKey(parentAcc))
							{	ImportObj importObj = new ImportObj(parentAcc);
								
							
								int nameIndex = parentAcc.indexOf("#");
								String value;
								if (nameIndex>=0) {
									value = parentAcc.substring(nameIndex+1);
								} else {
									value = parentAcc;
								}
								Attribute att = getAttribute("name");
								importObj.addAttribute(att.getName(), "N/A", DataTypes.STRING, value);
								att = getAttribute("type");
								importObj.addAttribute(att.getName(), "N/A", DataTypes.STRING, "class");
								objHashMap.put(parentAcc,importObj);
							}
							objRelSet.addRelationship(parentAcc, currentAcc, type); //oder andersrum
						}
						//System.out.println(toAcc+" "+type+" "+currentAcc);
						//relSet.add(objRel);
					}
				}
			}			
			for (ImportObj obj : objHashMap.values()){
				objList.add(obj);
			}
			super.importIntoTmpTables(objList,objRelSet);
			
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	protected void addAttributeToTmpList(String[] newAttribute, List<String[]> attributesToImport) {
		boolean doubleAttribute = false;
		for (int i=0;i<attributesToImport.size();i++) {
			String[] tmp = attributesToImport.get(i);
			if (tmp[0].equals(newAttribute[0])&&tmp[1].equals(newAttribute[1])&&tmp[2].equals(newAttribute[2])&&tmp[3].equals(newAttribute[3])) {
				doubleAttribute = true;
				break;
			}
		}
		if (!doubleAttribute) {
			attributesToImport.add(newAttribute);
		}
	}
	protected void removeDownloadedSourceData() throws ImportException {
	}
	private Attribute getAttribute(String name)
	{
		if (name.contains("label")) {
			return CacheManager.getInstance().getAttributeCache().getAttribute(3, "synonym", "N/A", DataTypes.STRING);
		}
		else if (name.contains("comment")) {
			return CacheManager.getInstance().getAttributeCache().getAttribute(2, "comment", "N/A", DataTypes.STRING);
		}
		else if (name.contains("name")) {
			return CacheManager.getInstance().getAttributeCache().getAttribute(1, "name", "N/A", DataTypes.STRING);
		}
		else if (name.contains("synonym")) {
			return CacheManager.getInstance().getAttributeCache().getAttribute(3, "synonym", "N/A", DataTypes.STRING);
		}
		else if (name.contains("type")) {
			return CacheManager.getInstance().getAttributeCache().getAttribute(4, "type", "N/A", DataTypes.STRING);
		}
		else if (name.contains("domain"))
			return CacheManager.getInstance().getAttributeCache().getAttribute(5, "domain", "N/A", DataTypes.STRING);
		else if (name.contains("range"))
			return CacheManager.getInstance().getAttributeCache().getAttribute(6, "range", "N/A", DataTypes.STRING);
		//System.out.println("Unhandled Attribute: " + name);
		return null;
	}

	private String getRelationship(String name)
	{
		if (name.contains("rdfs:subClassOf"))
			return "is_a";
		//else if (name.contains("owl:disjointWith"))
		//	return "owl:disjointWith";	
		String[] nameSplit = name.split("\\$");
		//System.out.println("Unhandled Relation: " + name);
		return nameSplit[0];
	}
	
	public boolean isThreadSafe() {
		return true;
	}
	
	private void resolveGenIDs(RdfSaxParser parser) {
		List<ParserConcept> conceptsToResolve = new Vector<RdfSaxParser.ParserConcept>();
		Map<String,ParserConcept> genIDConcepts = new HashMap<String,RdfSaxParser.ParserConcept>();
		List<String> genIDConceptsKeys = new Vector<String>();
		for (String key : parser.parsedConcepts.keySet()) {
			ParserConcept concept = parser.parsedConcepts.get(key);
			if (concept.hasGenIDRelation()) {
				conceptsToResolve.add(concept);
			}
			if (concept.accession.contains("genid")) {
				genIDConcepts.put(concept.accession,concept);
				genIDConceptsKeys.add(key);
			}
		}
		
		//Konzepte abarbeiten und GenID Konzepte l�schen !!
		for (ParserConcept concept : conceptsToResolve) {
			Set<String> relationsToDelete = new HashSet<String>();
			for (Map.Entry<String, String> entry : concept.relations.entrySet()) {
				String key = entry.getKey();
				if (key.contains("hasGenID")) {
					String genIDAccession = entry.getValue();
					ParserConcept genIDConcept = genIDConcepts.get(genIDAccession);
					
					for (Map.Entry<String, String> attribute : genIDConcept.attributes.entrySet()) {
						String key2 = attribute.getKey();
						if (key2.contains("rdfs:label")) {
							concept.addAttribute("synonym"+"$"+(++parser.tagCounter), attribute.getValue());
						}
					}
					relationsToDelete.add(key);
				}
			}
			for (String key : relationsToDelete) {
				concept.relations.remove(key);
			}
		}
		
		for (String key : genIDConceptsKeys) {
			parser.parsedConcepts.remove(key);
		}
	}
	
	public Set<String> getLanguages() {
		return _pars.languages_found;
	}
	
	private List<String> getOriginalLabels(String accession) {
		List<String> result = new Vector<String>();
		String internalAccession = _pars.inverseIndex.get(accession);
		ParserConcept c = _pars.parsedConcepts.get(internalAccession);
		if (c!=null) {
			for (Map.Entry<String, String> entryAttribute : c.attributes.entrySet())
			{
				Attribute att = getAttribute(entryAttribute.getKey());
				if(att!=null&&(att.getName().equalsIgnoreCase("synonym")||att.getName().equalsIgnoreCase("name"))) {
					result.add(entryAttribute.getValue());
				}
			}
		}
		return result;
	}
}
