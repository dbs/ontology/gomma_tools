package org.gomma.io.importer.sources.ncit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.PreSourceImporter;

public class NCIThesaurusImporterDirectURL extends PreSourceImporter {
	
	public void loadSourceData() throws ImportException {
	}
	
	public void importIntoTmpTables() throws ImportException {
		long start, duration;
		start = System.currentTimeMillis();
		System.out.print("Starting to load resources from web into temp area ...");
		try {
			//Temp. Tabellen fuer NCI Thesaurus erzeugen (Relationen ueber Namen !)
			String nciRelationship = "CREATE TABLE nci_relationship ( "+
									"source_id varchar(20) NOT NULL, "+
									"target_name varchar(700) NOT NULL, "+
									"rel_type varchar(50) NOT NULL, "+
									"KEY source_id (source_id), "+
									"KEY target_name (target_name(250)), "+
									"KEY rel_type (rel_type) "+
									") TYPE=MyISAM ;";
			SourceVersionImportAPI api = super.getImportAPI();
			api.insertIntoTmpTables(new String[]{nciRelationship});
			
			URL url = new URL(this.getMainImporter().getLocation());
			ZipInputStream zs = new ZipInputStream(url.openStream());
			
			ZipEntry entry;
			while ((entry=zs.getNextEntry())!=null) {
				if (entry.getName().startsWith("Thesaurus")) {
					BufferedReader reader = new BufferedReader(new InputStreamReader(zs,"UTF-8"));
					String line;
					String id, name, definition = null;
					String is_Obsolete = "false";
					String[] targets, synonyms = null;
					List<String[]> objectsImport = new Vector<String[]>();
					List<String[]> attributesImport = new Vector<String[]>();
					List<String[]> relationshipsImport = new Vector<String[]>();
					int parsed = 0;
					while (zs.available()>0) {
						line = reader.readLine();
						String[] lineItems = line.split("\t");
						if (lineItems.length>1) {
							id = lineItems[0].trim();
							name = lineItems[1].trim();
							if (lineItems.length>2) {
								targets = lineItems[2].trim().split("\\|");
							} else {
								targets = new String[]{};
							}
							if (lineItems.length>3) {
								synonyms = lineItems[3].trim().split("\\|");
							}
							if (lineItems.length>4) {
								definition = lineItems[4].trim();
							}
							for (int i = 0; i < targets.length; i++) {
								if (targets[i].startsWith("Retired_Concept")) {
									is_Obsolete = "true";
								}
							}
							if (targets.length==1&&targets[0].equalsIgnoreCase("root_node")) {
								targets = null;
							}
							objectsImport.add(new String[]{id});
							attributesImport.add(new String[]{id,"name",latin1Converter(name)});
							if (definition!=null)
								attributesImport.add(new String[]{id,"definition",latin1Converter(definition)});
							attributesImport.add(new String[]{id,"isObsolete",is_Obsolete});
							if (synonyms!=null) {
								for (String synonym : synonyms) {
									attributesImport.add(new String[]{id,"synonym",latin1Converter(synonym)});
								}
							}
							if (targets!=null) {
								for (String target : targets) {
									if (target.length()>600) {
										target = target.substring(0,600);
									}
									relationshipsImport.add(new String[]{id,target,"is_a"});
								}
							}
							parsed++;
							if (parsed>2000) {
								api.insertIntoTmpTables(objectsImport,"replace into tmp_objects (accession) values (?)");
								api.insertIntoTmpTables(attributesImport,"replace into tmp_obj_attribute (accession,attribute,value) values (?,?,?)");
								api.insertIntoTmpTables(relationshipsImport,"replace into nci_relationship (source_id,target_name,rel_type) values (?,?,?)");
								objectsImport.clear();
								attributesImport.clear();
								relationshipsImport.clear();
								parsed = 0;
							}
							id = null;
							name = null;
							definition = null;
							is_Obsolete = "false";
							targets = null;
							synonyms = null;
						}
					}
					api.insertIntoTmpTables(objectsImport,"replace into tmp_objects (accession) values (?)");
					api.insertIntoTmpTables(attributesImport,"replace into tmp_obj_attribute (accession,attribute,value) values (?,?,?)");
					api.insertIntoTmpTables(relationshipsImport,"replace into nci_relationship (source_id,target_name,rel_type) values (?,?,?)");
					objectsImport.clear();
					attributesImport.clear();
					relationshipsImport.clear();
					String[] statements = new String[2]; 
			
					statements[0] = "replace into tmp_structure (parent_acc,child_acc,rel_type) "+
											"select a.accession, r.source_id, r.rel_type "+
											"from tmp_obj_attribute a, nci_relationship r "+
											"where a.attribute = 'name' and r.target_name = a.value;";
					statements[1] = "drop table if exists nci_relationship";
					api.insertIntoTmpTables(statements);
					
					break;
				}
			}
			zs.close();
			duration = (System.currentTimeMillis()-start);
			System.out.println("  Done ! ("+duration+" ms)");
			
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	public void removeDownloadedSourceData() throws ImportException {
	}
	
	private String latin1Converter(String s) {
		for (int i=0;i<s.length();i++) {
			char c = s.charAt(i);
			if (Character.UnicodeBlock.of(c) != Character.UnicodeBlock.BASIC_LATIN) {
				if (c==945||c==946||c==1030||c==8242||c==8203||c==8209) {
					//System.out.println(s);
					//System.out.println(((int)c));
					s = s.replace(c+"", "?");
					//System.out.println(s);
				}
			}
		}
		//s = s.replace("\\xCE", "?");
		//s = s.replace("\\xB2", "?");
		return s;
	}
}
