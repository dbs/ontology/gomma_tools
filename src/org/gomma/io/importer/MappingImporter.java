/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the OMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * @author:     Toralf Kirsten
 * e-mail:      tkirsten@izbi.uni-leipzig.de
 * institution: Interdisciplinary Centre for Bioinformatics,
 *              University of Leipzig
 * @version:    1.0
 * date:        2007/11/25
 *
 * changes:     --
 * 
 **/

package org.gomma.io.importer;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Iterator;

import org.gomma.Gomma;
import org.gomma.GommaImpl;
import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;

public class MappingImporter {

	private Gomma gomma;
	
	public MappingImporter(String confFileNameLoc) throws GommaException, RemoteException {
		this.gomma = new GommaImpl();
		this.gomma.initialize(confFileNameLoc);
	}
	
	public void importMapping(String mapFileNameLoc) throws GommaException, RemoteException {
		this.gomma.getMappingManager().importMapping(mapFileNameLoc);
	}
	
	public void deleteMapping(String mappingName) throws GommaException, RemoteException {
		this.gomma.getMappingManager().deleteMapping(this.getMapping(mappingName));
	}
	
	public void renameMapping(String oldMappingName, String newMappingName) throws GommaException, RemoteException {
		MappingSet set = this.gomma.getMappingManager().getMappingSet();
		
		for (Mapping map : set.getCollection()) {
			if (map.getName().equals(oldMappingName)) {
				Mapping m = new Mapping.Builder(map.getID(),map.getDomainSource(),map.getRangeSource())
							.isDerived(map.isDerived())
							.isInstanceMapping(map.isInstanceMapping())
							.mappingClassName(map.getMappingClassName())
							.mappingMethodName(map.getMappingMethodName())
							.mappingToolName(map.getMappingToolName())
							.mappingTypeName(map.getMappingTypeName()).build();
				this.gomma.getMappingManager().updateMapping(m);
				break;
			}
		}
	}
	
	public void finish() throws GommaException, RemoteException {
		this.gomma.shutdown();
	}
	
	private Mapping getMapping(String mapName) throws GommaException, RemoteException {
		MappingSet maps = this.gomma.getMappingManager().getMappingSet();
		MappingSet set = new MappingSet();
		
		for (Mapping m : maps.getCollection())
			if (m.getName().equals(mapName)) set.addMapping(m);
		
		if (set.size()<1) throw new GommaException("Could not resolve the corresponding mapping object.");
		if (set.size()>1) throw new GommaException("Could not resolve a single mapping object for mapping name: '"+mapName+"'");
		Iterator<Integer> keyIterator = set.getIDSet().iterator();
		return set.getMapping(keyIterator.next());
	}
	
	
	public static String usage() {
		return "\nusage: MappingImporter <configFileNameLocation> (-i=<mappingFileNameLocation> | -d=<mappingName> )\n\n";
	}
	
	public static void main(String[] args) {
		MappingImporter importer;
		if (args != null && args.length==2) {
			try {
				String cfg = args[0].startsWith("-")?args[1]:args[0];
				String opt = args[1].startsWith("-")?args[1]:args[0];
				importer = new MappingImporter(cfg);
				if (opt.startsWith("-i")) {
					File f = new File (opt.substring(3));
					if (f.isDirectory()) {
						File[] importFiles = f.listFiles();
						for (File file : importFiles) {
							importer.importMapping(file.getAbsolutePath());
						}
					} else {
						importer.importMapping(f.getAbsolutePath());
					}
					importer.finish();
					return;
				} else 
				if (opt.startsWith("-d")) {
					importer.deleteMapping(opt.substring(3));
					importer.finish();
					return;
				} else 
				if (opt.startsWith("-r")) {
					String[] names = opt.substring(3).split(",");
					importer.renameMapping(names[0],names[1]);
					importer.finish();
					return;
				} else throw new GommaException("Could not resolve a possible option: "+opt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else System.out.println(MappingImporter.usage());
	}
}
