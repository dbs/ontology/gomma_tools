package org.gomma.io.importer;

import java.io.File;

import org.gomma.GommaImpl;


public class ExternalSourceImporter {

	public static String usage() {
		return "usage: ExternalSourceImporter <gomma.ini> <import-source.ini>";
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		if (args==null||!(args.length==2)) {
			System.out.println("Error: Wrong number of arguments.\n"+ExternalSourceImporter.usage());
		} else {
			GommaImpl rep = new GommaImpl();
			rep.initialize(args[0]);
			File importLocation = new File(args[1]);
		
			if (importLocation.isDirectory()) {
				
				File[] importFiles = importLocation.listFiles();
				for (int i=0;i<importFiles.length;i++) 
					rep.getSourceManager().importSource(importFiles[i].getAbsolutePath());
				
			} else {
				rep.getSourceManager().importSource(args[1]);
			}
			rep.shutdown();
		}
	}

}
