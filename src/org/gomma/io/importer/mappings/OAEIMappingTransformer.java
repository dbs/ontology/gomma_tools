package org.gomma.io.importer.mappings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * transform a oaei mapping file to a gomma mapping xml file.
 * The metadata of the xml file might be specified in a *.ini file, which has the following format as example:
 * <table  border="2" cellspacing="1" cellpadding="1">
 * <tbody>
 * <tr><th>property</th><th>value</th></tr>
 * <tr><td>baseName</td><td>oaei2013_SNOMED2NCI_repaired_UMLS_mappings</td></tr>
 * <tr><td>versionName</td><td>oaei2013_SNOMED2NCI_repaired_UMLS_mappings_2013</td></tr>
 *  <tr><td>timestampMap</td><td>2013-07-01</td></tr>
 *   <tr><td>mapping_class</td><td> ontology_mapping</td></tr>
 *    <tr><td>mapping_type</td><td> is_equivalent_to</td></tr>
 *     <tr><td>is_instance_type</td><td>false</td></tr>
 *      <tr><td>is_derived</td><td>false</td></tr>
 *    <tr><td>mapping_method</td><td>manual</td></tr>
 *    <tr><td>minSupport</td><td>1</td></tr>
 *    <tr><td>minConfidence</td><td>0</td></tr>
 *     <tr><td>sourceName</td><td>SNOMED_extended_overlapping_fma_nci</td></tr>
 *     <tr><td>targetName</td><td>NCIThesaurus_whole</td></tr>
 *     <tr><td>sourceType</td><td>AnatomicalEntity</td></tr>
 *     <tr><td>targetType</td><td>AnatomicalEntity</td></tr> 
 *      <tr><td>sourceTimestamp</td><td> 2013-07-01</td></tr> 
 *       <tr><td>sourceVersion</td><td> 2013-07</td></tr> 
 *        <tr><td>targetTimestamp</td><td> 2013-07-01</td></tr> 
 *         <tr><td>targetVersion</td><td> 2013-07</td></tr> 
 * </tbody>
 * </table>
 * @author Victor Christen
 *
 */
public class OAEIMappingTransformer {

	public static final int TXT =0;
	public static final int RDF =1;
	public static final int OWL =2;
	private String baseName;
	private String versionName;
	private String timestampMap;
	private String objType;
	
	private String mapping_class; 
	private String mappingType;
	private boolean is_instance_type;
	private boolean is_derived;
	private String mappingMethod;
	
	private String sourceName;
	private String targetName;
	private String sourceType;
	private String targetType;
	int type ;
	private String minSupport;
	private String minConfidence;
	private String sourceTimestamp;
	private String targetTimestamp;
	
	private DocumentBuilder builder ;
	private Document document;
	private Element mapElement;
	private String mapping_type;
	private String sourceVersion;
	private String targetVersion;
	private String mapping_method; 
	public void transformOAEIMapping (String oaeiFileName, String metadataFile){
		builder =null;
		
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		document  = builder.newDocument();
		try {
			this.readMetaDataFile(metadataFile);
			File f = new File (oaeiFileName);
			
			if (f.getName().endsWith("txt")){
				type = TXT;
			}else if (f.getName().endsWith("rdf")){
				type = RDF;
			}else{
				type = OWL;
			}
			switch (type){
			case TXT:
				this.transformCSV(f);	
				break;
			case RDF:
				break;
			case OWL:
				break;
			default:
				System.out.println("Format not supported!");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void readMetaDataFile(String file) throws FileNotFoundException, IOException{
		Properties prop = new Properties();
		prop.load(new FileReader(file));
		baseName = prop.getProperty("baseName");
		versionName = prop.getProperty("versionName");
		timestampMap = prop.getProperty("timestampMap");
		mapping_class = prop.getProperty("mapping_class");
		mapping_type = prop.getProperty("mapping_type");
		mapping_method = prop.getProperty("mapping_method");
		is_instance_type = Boolean.parseBoolean(prop.getProperty("is_instance_type"));
		is_derived = Boolean.parseBoolean(prop.getProperty("is_derived"));
		minSupport = prop.getProperty("minSupport");
		minConfidence = prop.getProperty("minConfidence");
		sourceName = prop.getProperty("sourceName");
		targetName = prop.getProperty("targetName");
		sourceType = prop.getProperty("sourceType");
		targetType = prop.getProperty("targetType");
		sourceTimestamp = prop.getProperty("sourceTimestamp");
		sourceVersion = prop.getProperty("sourceVersion");
		targetTimestamp = prop.getProperty("targetTimestamp");
		targetVersion= prop.getProperty("targetVersion");
		
		
		mapElement = document.createElement("mapping");
		document.appendChild(mapElement);
		Attr baseAttr = document.createAttribute("baseName");
		baseAttr.setNodeValue(baseName);
		mapElement.setAttributeNode(baseAttr);
		
		Attr versionAttr = document.createAttribute("versionName");
		versionAttr.setNodeValue(versionName);
		mapElement.setAttributeNode(versionAttr);
		
		Attr timeAttr = document.createAttribute("timestamp");
		timeAttr.setNodeValue(timestampMap);
		mapElement.setAttributeNode(timeAttr);
		
		Attr mapClassAttr = document.createAttribute("mapping_class");
		mapClassAttr.setNodeValue(mapping_class);
		mapElement.setAttributeNode(mapClassAttr);
		
		Attr mapTypeAttr = document.createAttribute("mapping_type");
		mapTypeAttr.setNodeValue(mapping_type);
		mapElement.setAttributeNode(mapTypeAttr);
		
		Attr isInstanceAttr = document.createAttribute("is_instance_map");
		isInstanceAttr.setNodeValue(Boolean.toString(is_instance_type));
		mapElement.setAttributeNode(isInstanceAttr);
		
		Attr isDerivedAttr = document.createAttribute("is_derived_map");
		isDerivedAttr.setNodeValue(Boolean.toString(is_derived));
		mapElement.setAttributeNode(isDerivedAttr);
		
		mapElement.setAttribute("mapping_method", mapping_method);
		
		Element metaData = document.createElement("metadata");
		metaData.setAttribute("minSupport", minSupport);
		metaData.setAttribute("minConfidence", minConfidence);
		Element domain = document.createElement("domain_sources");
		Element ds = document.createElement("source");
		ds.setAttribute("identifier", sourceType+"@"+sourceName);
		ds.setAttribute("objectType", sourceType);
		ds.setAttribute("name", sourceName);
		ds.setAttribute("timestamp", sourceTimestamp);
		ds.setAttribute("version", sourceVersion);
		domain.appendChild(ds);		
		Element target = document.createElement("range_sources");
		Element ts = document.createElement("source");
		ts.setAttribute("identifier", targetType+"@"+targetName);
		ts.setAttribute("objectType", targetType);
		ts.setAttribute("name", targetName);
		ts.setAttribute("timestamp", targetTimestamp);
		ts.setAttribute("version", targetVersion);
		target.appendChild(ts);
		
		metaData.appendChild(domain);
		metaData.appendChild(target);
		
		mapElement.appendChild(metaData);		
		
	}
	
	private Element transformOAEIMap(String line){
		Element cor = null;
		String[] values = line.split("\\|");
		cor = document.createElement("correspondence");
		cor.setAttribute("confidence", values[3]);
		cor.setAttribute("support", "1");
		
		Element dos = document.createElement("domain_objects");
		Element o = document.createElement("object");
		o.setAttribute("accession", values[0]);
		o.setAttribute("objecttype", sourceType);
		o.setAttribute("source_name", sourceName);
		dos.appendChild(o);
		Element ros = document.createElement("range_objects");
		Element o2 = document.createElement("object");
		o2.setAttribute("accession", values[1]);
		o2.setAttribute("objecttype", targetType);
		o2.setAttribute("source_name", targetName);
		ros.appendChild(o2);
		cor.appendChild(dos);cor.appendChild(ros);
		if (values[2].equals("=")){
			cor.setAttribute("corr_type", "is_equivalent_to");
		}else if (values[2].equals("<")){
			cor.setAttribute("corr_type", "is_less_general_than");
		}else if (values[2].equals(">")){
			cor.setAttribute("corr_type", "is_more_general_than");
		}
		
		return cor;
	}
	
	private void transformCSV(File f) throws IOException, TransformerException{
		FileReader fr = new FileReader (f);
		BufferedReader br = new BufferedReader(fr);
		Element correspondences= document.createElement("correspondences");
		
		while(br.ready()){
			Element corElement = this.transformOAEIMap(br.readLine());
			if (corElement!=null)
				correspondences.appendChild(corElement);
		}
		br.close();
		this.mapElement.appendChild(correspondences);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(new File("gomma_"+f.getName().replace(".txt", ".xml")+""));
		transformer.transform(source, result);
	}
	
	public static void usage(){
		System.out.println("usage:<oaei_mapping_file> <metadata_gomma_mapping_file>");
	}
	public static void main(String[] args){
		if (args==null){
			usage();
		}else if (args.length!=2){
			usage();
		}else{
			OAEIMappingTransformer transformer = new OAEIMappingTransformer();
			transformer.transformOAEIMapping(args[0], args[1]);
		}
	}
}
