package org.gomma.io.importer.updater;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Vector;


public class WebPageParserGo implements WebPageParser {

	public Vector<Ontology> ontologieNew = null;
	
	public Vector<Ontology> loadOntolodyNew(String goURLString) {
		ontologieNew = new Vector<Ontology>();

		try {
			URL goURL = new URL(goURLString);
			// URL yahoo = new
			// URL("http://obo.cvs.sourceforge.net/obo/obo/ontology/vocabularies/flybase_controlled_vocabulary.obo?view=log");
			URLConnection goConnection = goURL.openConnection();
			BufferedReader dis = new BufferedReader(new InputStreamReader(goConnection
					.getInputStream()));
			String inputLine;
			Vector<String> versionen = new Vector<String>();
//			String version = "";
			boolean versionGefunden = false;

			while ((inputLine = dis.readLine()) != null) {
				
				//ende der versionen Liste in der Webseite
				if (inputLine.contains("<a href=\"DATESTAMP\">")) {
					versionGefunden = false;
				}
				
				//versions Link hinzufuegen
				if (versionGefunden) {
					versionen.add(inputLine);
					//System.out.println(inputLine);
				}
				
				//anfang der version Liste in der Webseite
				if (inputLine.contains("<a href=\"/\">Parent Directory</a>")) {
					versionGefunden = true;
				}

			}
			
			dis.close();
			// System.out.println(versionen.get(1));
			
			for (int i = 0; i < versionen.size(); i++) {
				//version Webseite laden
				String versionlink = versionen.get(i);
				
				//<a href="2005-03-21/">
				versionlink=versionlink.substring(9, 19);
				//System.out.println("link:"+versionlink);
				goURL = new URL(goURLString+versionlink);
				goConnection = goURL.openConnection();
				dis = new BufferedReader(new InputStreamReader(goConnection.getInputStream()));
				String month = "";
				String year = "";
				int day = 0;
				String location="";
				
				while ((inputLine = dis.readLine()) != null) {
					// Details herausfiltern
					//inputline enth�lt version der ontologie mit detail Informationen
					if (inputLine.matches(".*go_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-termdb-tables.tar.gz.*")) {
							
						String details = inputLine.substring(9, 41);
						
						location=inputLine.substring(9,41);
						
						if(location.matches("go_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-termdb-tables.tar.gz")){
							location = goURLString+versionlink+"/"+location;
						} else {
							System.out.println("WebPageParserGo parse failure maybe update.");
							System.exit(0);
						}
						
						
						//<a href="go_20090222-termdb-tables.tar.gz">
						//System.out.println("raw:"+details);
						year = details.substring(3, 7);
						month = details.substring(7, 9);
						
						//tag validieren
						try {
							int dayTmp = new Integer(details.substring(9, 11)).intValue();
							if (dayTmp > 0 && dayTmp < 32) {
								day = dayTmp;
							}else{
								System.out.println("day: " +day);
								System.out.println("maybe failed parse go-website!");
								System.exit(0);
							}
							
						} catch (NumberFormatException nfe) {
							System.out.println("maybe failed parse go-website!");
							System.exit(0);
						}

						//monat validieren
						try {
							int monthTmp = new Integer(month).intValue();
							if (monthTmp < 0 || monthTmp > 12) {
								System.out.println("month: " +month);
								System.out.println("maybe failed parse go-website!");
								System.exit(0);
							}
						} catch (NumberFormatException nfe) {
							System.out.println("maybe failed parse go-website!");
							System.exit(0);
						}

						//Jahr validieren
						try {
							int yearTmp = new Integer(year).intValue();
							//System.out.println("Jahr:"+jahrtmp);
							if (yearTmp < 1900 || yearTmp > 2100) {
								System.out.println("year: " +year);
								System.out.println("maybe failed parse go-website!");
								System.exit(0);
							}
						} catch (NumberFormatException nfe) {
							System.out.println("maybe failed parse go-website!");
							System.exit(0);
						}
						//System.out.println("version:"+(jahr+"."+monat));
						ontologieNew.add(new Ontology("", (year+"."+month), year,month, String.valueOf(day), location));
						
					}
						
				}
				
				dis.close();
				
			}
			
		} catch (MalformedURLException me) {
			System.out.println("MalformedURLException: " + me);
		} catch (IOException ioe) {
			System.out.println("IOException: " + ioe);
		}
		return ontologieNew;

	}
	
	
	public static void main(String[] args){
		WebPageParserGo go = new WebPageParserGo();
		Vector<Ontology> v =  go.loadOntolodyNew("http://archive.geneontology.org/lite/");
		Collections.sort(v);
		for(Ontology o : v){
			System.out.println(o.version );
			System.out.println(o.location);
		}
	}

}
