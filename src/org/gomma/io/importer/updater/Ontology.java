package org.gomma.io.importer.updater;

/**
 * Ontology data object
 * @author: Tom Weiland 
 * @version: 1.0
 * 
 */

public class Ontology implements Comparable<Ontology>{
	public String fileName;
	public String month;
	public String day;
	public String year;
	public String version;
	public String source = null;
	public String location = null;
	public String isontology=null;
	public String objecttype=null;
	public String importer = null;
	
	
	Ontology(String fileName, String version){
		this.fileName=fileName;
		this.version=version;
	}
	
	Ontology(String fileName, String version, String year, String month, String day, String location){
		this.fileName=fileName;
		this.version=version;
		this.year=year;
		this.month=month;
		this.day=day;
		this.location = location;
	}
	
	Ontology(String fileName, String version, String year, String month, String day, String source, String location, String isontology, String objecttype, String importer){
		this.fileName=fileName;
		this.version=version;
		this.year=year;
		this.month=month;
		this.day=day;
		this.source=source;
		this.location=location;
		this.isontology=isontology;
		this.objecttype=objecttype;
		this.importer=importer;
	}
	
	
	public int compareTo(Ontology o) {
			//System.out.println("v1:"+version);
			int version1 = Integer.parseInt(version.substring(0,version.indexOf(".")));
			int version1n = Integer.parseInt(version.substring(version.indexOf(".")+1));
			//System.out.println("v2:"+o.version);
			int version2 = Integer.parseInt(o.version.substring(0,o.version.indexOf(".")));
			int version2n = Integer.parseInt(o.version.substring(o.version.indexOf(".")+1));
			if( version1 > version2 && version1n > version2n ){
				return 1;
			
			}else if( version1 < version2 && version1n < version2n ){
				return -1;
			} else if( version1 == version2 && version1n > version2n ){
				return 1;
			} else if( version1 == version2 && version1n < version2n ){
				return -1;
			}else
			return 0;
	
	}

}
