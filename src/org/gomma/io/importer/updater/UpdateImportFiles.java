package org.gomma.io.importer.updater;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Properties;
import java.util.Vector;

/**
 * Update import files. to start correct two parameters needed first path and
 * name to master file second path to output folder Example: "..\masterfile"
 * "..\\"
 * 
 * @author: Tom Weiland
 * @version: 1.0
 * 
 */

public class UpdateImportFiles {

	public Vector<Ontology> ontologyOld = null;
	public Vector<Ontology> ontologyNew = null;
	public Vector<Ontology> ontologyVersionFound = null;
	public String source = null;
	public String location = null;
	public String isontology = null;
	public String objecttype = null;
	public String importer = null;

	public String masterfile = "";
	public String outputFolder = "";

	public String webSiteType = "";

	public boolean newOntologyFile = false;

	public static void main(String[] args) {

		// command line option
		/*
		 * for(String option: args){ System.out.println(option); }
		 */
		String masterfile = "";
		String outputFolder = "";
		try {
			masterfile = args[0];
			outputFolder = args[1];
			// System.out.println(masterfile);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("no value for master file, please set value");
			System.exit(0);
		}

		UpdateImportFiles a = new UpdateImportFiles(masterfile, outputFolder);
		a.readMasterFile();

	}

	UpdateImportFiles(String masterfile, String outputFolder) {
		this.masterfile = masterfile;
		this.outputFolder = outputFolder;
	}

	public void readMasterFile() {
		// loc = System.getProperty("user.dir")+loc;
		try {
			BufferedReader in = new BufferedReader(new FileReader(masterfile));
			String zeile = null;
			while ((zeile = in.readLine()) != null) {
				newOntologyFile = false;
				String[] splitArray = zeile.split("\t");
				// Daten fuer Datei laden
				source = splitArray[0];
				location = splitArray[1];
				isontology = splitArray[2];
				objecttype = splitArray[3];
				importer = splitArray[4];
				String fileName = splitArray[5];
				webSiteType = splitArray[6];
				// System.out.println(source+" "+location+" "+isontology+"
				// "+objecttype+" "+importer+" "+fileName);

				// 1. alte Ontologie laden
				// ------------------------------------------------
				ontologyOld=loadOntologyOld(fileName);
				// 2. neue Ontologie laden
				// ------------------------------------------------
				System.out.println("check ontology: " + objecttype+"@"+source);
				if (webSiteType.equals("GO")) {
					WebPageParserGo wgo = new WebPageParserGo();
					ontologyNew = wgo.loadOntolodyNew(location);
				} else if (webSiteType.equals("CVS")) {
					WebPageParserCVS wcvs = new WebPageParserCVS();
					ontologyNew = wcvs.loadOntolodyNew(location);
				}

				// loadOntolodyNew(location);

				// break;
				// 3. vergleich der Versionen
				ontologyVersionFound = versionCompare(ontologyOld,
						ontologyNew);
				// erster Monat Onotlgie neue Funktion

				// 4. in Datei schreiben
				FilesWriting d = new FilesWriting(outputFolder, newOntologyFile);
				d.writeFile(ontologyVersionFound, fileName);

			}
		} catch (IOException e) {
			System.out.println("could not find master file: " + masterfile);
			// e.printStackTrace();
		}
		System.out.println("done.");
	}

	public Vector<Ontology> loadOntologyOld(String file) {
		String text = "";
		Vector<Ontology> ontologieOld = new Vector<Ontology>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(outputFolder
					+ file));
			String zeile = null;
			boolean neueOn = false;
			Ontology o = null;
			Properties properties = new Properties();
			while ((zeile = in.readLine()) != null) {
				text += "\n" + zeile;
				String[] zeileSplit = zeile.split("\t");
				if (zeileSplit.length==2) {
					properties.setProperty(zeileSplit[0].trim(), zeileSplit[1].trim());
				}
				if (zeile.indexOf("#") == 0) {
					//properties.load(new StringReader(text));
					String date = (String) properties.get("timestamp");
					String[] dateValues = date.split("-");
					// todo [0] entfernen und property loader verwenden

					o = new Ontology(file, ((String) properties.get("version"))
							.replace("-", "."), dateValues[0], dateValues[1],
							dateValues[2], "");
					
					ontologieOld.add(o);
					neueOn = true;
				}

				if (neueOn) {
					text = "";
					neueOn = false;
				}

			}
		} catch (IOException e) {
			// ontology Datei noch nicht vorhanden
			newOntologyFile = true;
			ontologieOld = new Vector<Ontology>();
			// e.printStackTrace();
		}
		return ontologieOld;
	}

	public Vector<Ontology> versionCompare(Vector<Ontology> ontologyOld,
			Vector<Ontology> ontologyNew) {
		Vector<Ontology> ontologyVersionFound = new Vector<Ontology>();
		int indexVersion = -1;
		if (ontologyOld.size() > 0) {
			// System.out.println("ontologie gemeinsame version suchen");
			// aus den alten Ontologien die neuste Version suchen
			// --------------------------------
			Collections.sort(ontologyOld);// absteigend sortieren
			Ontology actuelVersion = ontologyOld
					.get((ontologyOld.size() - 1));
			// System.out.println("actuelle version "+actuelVersion.version+"
			// jahr"+actuelVersion.year);
			Collections.sort(ontologyNew);
			/*
			 * for(Ontology on : ontologieNew){ System.out.println("neue
			 * version:"+on.version); }
			 */
			// erste Veroeffentlichung der Monate suchen
			for (int i = 0; i < ontologyNew.size(); i++) {
				// Ontologie o = ontologieOld.get(i);
				Ontology o2 = ontologyNew.get(i);
				// System.out.println("old " + o.fileName+" "+ o.version +" "+
				// o.year);
				if (actuelVersion.version.equals(o2.version)) {
					//System.out.println("old " + actuelVersion.fileName + " "
					//		+ actuelVersion.version + " " + actuelVersion.year);
					//System.out.println("new " + o2.fileName + " " + o2.version
					//		+ " " + o2.year + " " + o2.month);
					indexVersion = i;
					break;
				}

			}
		} else {
			// keine alten ontologies
			// System.out.println("keine gemeinsame Version -> alle neu");
			indexVersion = 0;
			Collections.sort(ontologyNew);
			Ontology oNew = ontologyNew.get(0);
			// erste version hinzuf�gen
			ontologyVersionFound.add(new Ontology(oNew.fileName,
					oNew.version, oNew.year, oNew.month, oNew.day, source,
					oNew.location, isontology, objecttype, importer));
			System.out.println("new version:" + oNew.version + " " + oNew.year
					+ "-" + oNew.month + "-" + oNew.day);
		}
		// version gefunden
		// System.out.println("indexVersion:"+indexVersion+"
		// size:"+ontologieNew.size());
		if (indexVersion > -1) {
			/*
			 * for(int i = (indexVersion+1); i < ontologieNew.size(); i++){
			 * System.out.println("neue version:"+ontologieNew.get(i).version); }
			 */
			String month = ontologyNew.get(indexVersion).month;
			String year = ontologyNew.get(indexVersion).year;
			// System.out.println("aktuelle
			// version"+ontologieNew.get(indexVersion).version);
			// System.out.println("aktuelle jahr"+year);

			for (int i = (indexVersion + 1); i < ontologyNew.size(); i++) {
				// for (int i = (indexVersion-1); i >= 0; i--) {
				Ontology oNew = ontologyNew.get(i);
				if (year.equals(oNew.year)
						&& Integer.parseInt(month) < Integer
								.parseInt(oNew.month)) {
					// String fileName, String version, String year, String
					// month, int day
					if (webSiteType.equals("GO")) {
						oNew.version = oNew.version.replace(".", "-");
					}
					ontologyVersionFound
							.add(new Ontology(oNew.fileName, oNew.version,
									oNew.year, oNew.month, oNew.day, source,
									oNew.location, isontology, objecttype, importer));
					System.out.println("new version:" + oNew.version + " "
							+ oNew.year + "-" + oNew.month + "-" + oNew.day);
					//System.out.println(oNew.location);
					month = oNew.month;
				}
				// jahreswechsel
				if (Integer.parseInt(year) < Integer.parseInt(oNew.year)) {
					// Jahreswechsel
					year = oNew.year;
					month = oNew.month;
					if (webSiteType.equals("GO")) {
						oNew.version = oNew.version.replace(".", "-");
					}
					ontologyVersionFound
							.add(new Ontology(oNew.fileName, oNew.version,
									oNew.year, oNew.month, oNew.day, source,
									oNew.location, isontology, objecttype, importer));
					System.out.println("new version:" + oNew.version + " "
							+ oNew.year + "-" + oNew.month + "-" + oNew.day);
					//System.out.println(oNew.location);
				}
			}
		}

		return ontologyVersionFound;

	}
}
