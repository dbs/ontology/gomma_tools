package org.gomma.io.importer.updater;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.Vector;


public class WebPageParserCVS implements WebPageParser{

	public Vector<Ontology> ontologieNew = null;
	
	public Vector<Ontology> loadOntolodyNew(String cvsURLString) {
		ontologieNew = new Vector<Ontology>();

		try {
			URL cvsURL = new URL(cvsURLString);
			
			URLConnection cvsConnection = cvsURL.openConnection();
			BufferedReader dis = new BufferedReader(new InputStreamReader(cvsConnection
					.getInputStream()));
			String inputLine;
			Vector<String> versionen = new Vector<String>();
			String version = ""; //Version mit Details
			boolean versionFound = false;

			while ((inputLine = dis.readLine()) != null) {
				if (inputLine.contains("<a name=\"rev")) {
					versionen.add(version);
					version = "";
					version += inputLine;
					versionFound = true;
				}

				if (versionFound) {
					version += inputLine;

				}
				/*
				if(inputLine.contains(">download<")){
					System.out.println(inputLine);
				}
				*/
			}
			versionen.add(version);
			dis.close();
			// System.out.println(versionen.get(1));

			for (int i = 0; i < versionen.size(); i++) {

				// Details herausfiltern
				String version1 = versionen.get(i);
				if (version1.length() > 1) {

					// Revision
					int start = version1.indexOf("<a name=\"rev") + 12;
					int end = version1.indexOf("\"></a>");
					String versionsNumber = version1.substring(start, end);
					// System.out.println("version rev: "+version1.substring(
					// start, end));

					// Zeitstempel
					// <em>Fri Sep 12 07:04:24 2008 UTC</em>
					start = version1.indexOf("<em>") + 4;
					end = version1.indexOf("</em>");
					StringTokenizer st = new StringTokenizer(version1
							.substring(start, end), " "); // trenne den String
															// durch das
															// Trennzeichen ;
					String month = "";
					String year = "";
					int day = 0;
					while (st.hasMoreTokens()) {
						String s = st.nextToken();
						// Jahr ermitteln
						if (s.matches("[2][0-9][0-9][0-9]")) {
							year = s;
						}

						// Monat ermitteln
						if (s.equals("Jan"))
							month = "01";
						if (s.equals("Feb"))
							month = "02";
						if (s.equals("Mar"))
							month = "03";
						if (s.equals("Apr"))
							month = "04";
						if (s.equals("May"))
							month = "05";
						if (s.equals("Jun"))
							month = "06";
						if (s.equals("Jul"))
							month = "07";
						if (s.equals("Aug"))
							month = "08";
						if (s.equals("Sep"))
							month = "09";
						if (s.equals("Oct"))
							month = "10";
						if (s.equals("Nov"))
							month = "11";
						if (s.equals("Dec"))
							month = "12";

						// tag ermitteln
						try {
							int dayTmp = new Integer(s).intValue();
							if (dayTmp > 0 && dayTmp < 32) {
								day = dayTmp;
							}
						} catch (NumberFormatException nfe) {
							// System.err.println("Kann Zahl nicht umwandeln.")
						}

					}
					// System.out.println("Jahr: " + jahr +" Monat: "+ monat +"
					// Tag: "+ tag);
					// System.out.println("version time: "+version1.substring(
					// start, end));
					
					start = version1.indexOf("*checkout");
					end = version1.indexOf("download");
					//System.out.println("http://obo.cvs.sourceforge.net/"+version1.substring(start, (end-2)));				
					
					ontologieNew.add(new Ontology("", versionsNumber, year,	month, String.valueOf(day), ("http://obo.cvs.sourceforge.net/"+version1.substring(start, (end-2)))));
				}
			}
		} catch (MalformedURLException me) {
			System.out.println("MalformedURLException: " + me);
		} catch (IOException ioe) {
			System.out.println("IOException: " + ioe);
		}
		return ontologieNew;

	}
	
	public static void main(String[] args){
		WebPageParserCVS go = new WebPageParserCVS();
		Vector<Ontology> v =  go.loadOntolodyNew("http://obo.cvs.sourceforge.net/obo/obo/ontology/anatomy/cell_type/cell.obo?view=log");
		Collections.sort(v);
		for(Ontology o : v){
			System.out.println(o.version);
		}
	}
	

}
