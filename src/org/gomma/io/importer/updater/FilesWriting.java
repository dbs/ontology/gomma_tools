package org.gomma.io.importer.updater;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Vector;

/**
 * Class to write result 
 * @author: Tom Weiland 
 * @version: 1.0
 * 
 */

public class FilesWriting {
	
	private String loc;
	private boolean newOntologyFile;
	FilesWriting(String loc, boolean newOntologyFile){
		this.loc=loc;
		this.newOntologyFile=newOntologyFile;
	}
	
	
	public void writeFile(Vector<Ontology> ontologies, String fileName){
		try{
			FileWriter fstream = new FileWriter(loc+fileName, true);
		    BufferedWriter out = new BufferedWriter(fstream);
		    
		    for(int i=0;i<ontologies.size();i++){
		    	Ontology o = ontologies.get(i);
		    	String text = "\nobjectType\t"+o.objecttype.trim()+"\nsource\t"+o.source.trim()+"\n"+"isontology\t"+o.isontology.trim()+"\n"+"location\t"+o.location.trim()+"\n"+"version\t"+o.version.trim()+"\ntimestamp\t"+o.year.trim()+"-"+o.month.trim()+"-"+o.day+"\n"+"importer\t"+o.importer.trim()+"\n#";
		    	//String text = "\nsource\t"+o.source+"\n"+"isontology\t"+o.isontology+"\n"+"location\t"+o.location+"="+o.version+"\n"+"version\t"+o.version+"\ntimestamp\t"+o.year+"-"+o.month+"-"+o.day+"\n"+"importer\t"+o.importer+"\n#";
		    	//ersten Eintrag erzeugen, wenn neue Datei erzeugt wurde
		    	if(newOntologyFile){
					   System.out.println("create file: "+fileName);
					   
					   text = "objecttype\t"+o.objecttype.trim()+"\nsource\t"+o.source.trim()+"\n"+"isontology\t"+o.isontology.trim()+"\n"+"location\t"+o.location.trim()+"\n"+"version\t"+o.version.trim()+"\ntimestamp\t"+o.year.trim()+"-"+o.month.trim()+"-"+o.day+"\n"+"importer\t"+o.importer.trim()+"\n#";
					   //text = "source\t"+o.source+"\n"+"isontology\t"+o.isontology+"\n"+"location\t"+o.location+"="+o.version+"\n"+"version\t"+o.version+"\ntimestamp\t"+o.year+"-"+o.month+"-"+o.day+"\n"+"importer\t"+o.importer+"\n#";
					   newOntologyFile=false;	
				  }
				out.write(text);
		    }
		    out.close();
		    }catch (Exception e){// Catch exception if any
		      System.out.println("could not find file: "+loc+fileName);
		    	//System.err.println("Error: " + e.getMessage());
		    }
	}
		
}