package org.gomma.io.importer;

import java.rmi.RemoteException;
import java.util.HashMap;

import org.gomma.GommaImpl;
import org.gomma.exceptions.GommaException;

public class Example_SourceImporter_WithOut_ImportScript {

	public static void main(String[] args) throws RemoteException, GommaException {
		
		//enter website user login and project name
		String userLogin	= "";
		String projectName	= "";
		
		String dbUrl	= "jdbc:mysql://wdiserv2.informatik.uni-leipzig.de:3306/eSystems_gomma_"+userLogin+"_"+projectName;
		String dbUser	= "eSystems";
		String pw		= "esys123";
		String driver	= "com.mysql.jdbc.Driver";
		
		GommaImpl gi = new GommaImpl();
		gi.initialize(driver,dbUrl,dbUser,pw);
		
		//enter source data (get data via user interface--values from text field and/or by selection of a source in existing repository)
		String source 			= "";
		String objectType		= "";
		String isOntology		= "";
		String location			= ""; //URI
		String dataString		= ""; //local file
		String version			= "";
		String timestamp		= "";
		String structuretype	= "";
		String importer			= ""; //org.gomma.io.importer.sources.OBOImporterURI, SAXBasedImporterURI, ...
		String accessionPrefix	= "";		
		
		//create map containing necessary source metadata for import
		HashMap<String,String> importDescriptionObj = new HashMap<String,String>();
		importDescriptionObj.put(SourceImporter.SOURCE, source);
		importDescriptionObj.put(SourceImporter.OBJECTTYPE, objectType);
		importDescriptionObj.put(SourceImporter.ISONTOLOGY, isOntology);
		importDescriptionObj.put(SourceImporter.VERSION,version);
		importDescriptionObj.put(SourceImporter.TIMESTAMP, timestamp);
		importDescriptionObj.put(SourceImporter.STRUCTURETYPE, structuretype);
		importDescriptionObj.put(SourceImporter.IMPORTER, importer);
		importDescriptionObj.put(SourceImporter.ACCESSIONSPREFIX, accessionPrefix);
		//entweder oder ..
		importDescriptionObj.put(SourceImporter.LOCATION, location);
		importDescriptionObj.put(SourceImporter.DATASTRING, dataString);
		
		//import
		gi.getSourceManager().importSource(importDescriptionObj);
	}
}
